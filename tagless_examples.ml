open Tagless_types

module EX(S: C_lang) = struct
  open S

  let test_var_create i =
    let f = func (fun n ->
        new_var n (fun acc -> return_ (deref acc))) in
    app f (int_ i)

  let test_fact n =
    let one = (int_ 1) in
    let fact = func (fun n ->
        new_var (int_ 1) (fun acc -> seq
                             (for_ one (n %+ one) one
                                (fun i -> asgn acc (i %* (deref acc)))
                             )
                             (return_ (deref acc))
                         )) in
    app fact (int_ n)

  let test_nested_loop n m =
    let one = (int_ 1) in
    let nested_loop =
      func (fun n ->
          return_ (
            func (fun m ->
                new_var (int_ 0)
                  (fun sum ->
                     seq
                       (for_ one n one
                          (fun i ->
                             (for_ one m one
                                (fun j ->
                                   (asgn sum ((deref sum) %+ (i %* j)))
                                )
                             )
                          )
                       )
                       (return_ (deref sum))
                  )
              )
          )
        )
    in
    app (app nested_loop (int_ n)) (int_ m)

end
