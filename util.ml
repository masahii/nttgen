open Fft_types
open Codelib

let rec nat_to_int: type n. n nat -> int = function
  | Z -> 0
  | S(n) -> 1 + (nat_to_int n)

let rec pow a = function
  | 0 -> 1
  | 1 -> a
  | n ->
    let b = pow a (n / 2) in
    b * b * (if n mod 2 = 0 then 1 else a)

let convert_array: 'a code array -> 'a array code =
  let rec convert_list: 'a code list -> 'a list code = function
    | hd :: tl ->
      let tl_code = convert_list tl in
      .<.~hd :: .~tl_code>.
    | [] -> .<[]>. in
  fun arr ->
    let lst = List.init (Array.length arr) (fun i -> arr.(i)) in
    let lst_code = convert_list lst in
    .<Array.of_list .~lst_code >.
