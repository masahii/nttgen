N = 1024
q = 12289


def find_primitive_root(n):
    # find omega s.t omega^n mod q == 1
    for w in range(q):
        if (w ** n) % q == 1:
            reject = False
            for i in range(1, n):
                if (w ** i) % q == 1:
                    reject = True
                    break
            if reject is False:
                return w


omega = find_primitive_root(1024)
omegas = [(1024, omega)]
n = 1024

for i in range(10):
    print(n, omega)
    omega = (omega ** 2) % q
    n //= 2
    omegas.append((n, omega))

print(";".join(["(%d, %d)" % (n, o) for (n, o) in omegas]))
# for n in [1, 2, 4, 8, 16, 1024]:
#     print(n, )
