open Ctypes
open Foreign
open Tagless_types
open Tagless_fft
open Tagless_impl_c
open Tagless_unroll
open Tagless_vectorize

module Simple = struct
  open C_codegen
  let test_fact () =
    let one = (int_ 1) in
    let fact =
      func "fact" CInt (fun n ->
          new_var (int_ 1) (fun acc ->
              seq
                (for_ one (n %+ one) one (fun i ->
                     asgn acc (i %* (deref acc)))
                )
                (return_ (deref acc)))) in
    func "fact_call" CInt (fun n ->
        return_ (app fact n))

  let vector_add n =
    let arg_ty = CArr CInt in
    func2 "vector_add" arg_ty arg_ty (fun arr1 arr2->
        let open Unroll(C_codegen)(struct let factor = 8 end) in
        for_unroll (int_ 0) (int_ n) (int_ 1) (fun i ->
            arr_set arr1 i ((arr_get arr1 i) %+ (arr_get arr2 i))))

end

module Bit_rev_test = struct
  module Bit_rev = Tagless_fft.Bit_rev(C_codegen)
  let bit_rev n =
    Bit_rev.get_bit_reverse n (CArr CInt) ()
end

module SIMD_test = struct
  let vector_add n =
    let size = n in
    let q = 12289 in
    let qinv = 12287 in
    let omega = 49 in
    let module Param = struct let q = q let qinv = qinv let omega = omega let n = size end in
    let open C_codegen in
    let module D = UIntegerModulo(C_codegen)(Param) in
    let arg_ty = CArr CUInt16 in
    func2 "vector_add" arg_ty arg_ty (fun arr1 arr2->
        let module SIMD = AVX2_UInt16(D) in
        let open Vectorize(SIMD) in
        for_ (int_ 0) (int_ n) (int_ 1) (fun i ->
            arr_set arr1 i (D.add (arr_get arr1 i) (arr_get arr2 i))))
end

let get_prim_root n q =
  assert (n = 1024 && q = 12289);
  49

let get_ntt (module Param: Ntt_param) =
  let module C_Int_mod = IntegerModulo(C_codegen)(Param)  in
  let module C_NTT = FFT_gen(C_codegen)(C_Int_mod) in
  C_NTT.fft Param.n

let get_wrapper_common out_so func_name signature =
  let dl_path = Printf.sprintf "/home/masa/projects/dev/nttgen/%s" out_so in
  let dl = Dl.dlopen ~flags:[Dl.RTLD_LAZY] ~filename:dl_path in
  foreign func_name ~from:dl signature

let get_wrapper out_so func_name arg_ty ret_ty =
  get_wrapper_common out_so func_name (arg_ty @-> returning ret_ty)

let get_wrapper2 out_so func_name arg_ty1 arg_ty2 ret_ty =
  get_wrapper_common out_so func_name (arg_ty1 @-> arg_ty2 @-> returning ret_ty)

let compile_c decls out_so =
  let tmp_c = "generated.c" in
  let oc = open_out tmp_c in
  Printf.fprintf oc "%s" decls;
  close_out oc;
  let command = Printf.sprintf "gcc -mavx2 -shared %s -fPIC -o %s" tmp_c out_so in
  (* let command = Printf.sprintf "gcc -mavx2 -shared %s -fPIC -o %s" "vector_add_u16_avx2.c" out_so in *)
  ignore(Sys.command command)

let test_vector_add =
  let size = 1024 in
  let (_, _, _, fn_name, decls) = SIMD_test.vector_add size in
  Printf.printf "%s\n" decls;
  let out_so = "generated.so" in
  compile_c decls out_so;
  let vector_add = get_wrapper2 out_so fn_name (ptr uint16_t) (ptr uint16_t) void in
  let arr1 = CArray.of_list uint16_t (List.init size (fun i -> Unsigned.UInt16.of_int i)) in
  let arr2 = CArray.of_list uint16_t (List.init size (fun i -> Unsigned.UInt16.of_int i)) in
  vector_add (CArray.start arr1) (CArray.start arr2);
  CArray.iter (fun i -> Printf.printf "%d\n" (Unsigned.UInt16.to_int i)) arr1
