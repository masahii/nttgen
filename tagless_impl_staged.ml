module C = struct
  open Codelib
  type 'a expr = 'a code
  type 'a stmt = 'a code
  type ('a, 'b) func_rep = ('a -> 'b) expr
  type ('a1, 'a2, 'b) func_rep2 = ('a1 -> 'a2 -> 'b) expr

  let int_ (n: int) = .<n>.

  let (%+) x y = .<.~x + .~y>.
  let (%-) x y = .<.~x - .~y>.
  let (%*) x y = .<.~x * .~y>.
  let (%/) x y = .<.~x / .~y>.
  let (%<) x y = .<.~x < .~y>.
  let (%<<) a b = .<Int.shift_left .~a .~b>.
  let (%>>) a b = .<Int.shift_right .~a .~b>.

  let (%&) x y = .<.~x land .~y>.

  let func _ _ f = genlet .<fun a -> .~(f .<a>.)>.
  let func2 _ _ _ f = genlet .<fun a1 a2 -> .~(f .<a1>. .<a2>.)>.

  let app f arg =
    .<.~f .~arg>.

  let app2 f arg1 arg2 =
    .<.~f .~arg1 .~arg2>.

  let new_var v k =
    .<let var = ref .~v in
      .~(k .<var>.)
      >.

  let asgn var v =
    .<
      .~var := .~v
    >.

  let deref var = .<!(.~var)>.

  let return_ v = v

  let if_ cond tru fls =
    .<if .~cond then .~tru
    else begin
      .~(match fls with
          | Some stmt -> stmt
          | _ -> .<()>.
        )
       end
    >.

  let let_ rhs body = .<let v = .~rhs in .~(body .<v>.)>.

  let for_ low high step body =
    .<let num_iter = (.~high - .~low) / .~step in
      for i = 0 to num_iter - 1 do
         let index = .~low + .~step * i in
         .~(body .<index>.);
      done
     >.

  let for_step = 1

  let seq s1 s2 = .<begin ignore(.~s1); .~s2 end>.

  let ignore_ v = .<let _ = .~v in ()>.
end

module C_complex = struct
  include C
  open Complex
  let complex_ v = .<v>.

  let from_real r = .<{re=(.~r); im=0.0}>.

  let cadd x y = .<Complex.add .~x .~y>.
  let csub x y = .<Complex.sub .~x .~y>.
  let cmul x y = .<Complex.mul .~x .~y>.
  let cdiv x y = .<Complex.div .~x .~y>.
end

module C_Array = struct
  include C
  type 'a arr = 'a array

  let arr_init n f =
    let a = Array.init n f in
    Codelib.genlet (Util.convert_array a)

  let arr_length arr = .<Array.length .~arr>.

  let arr_copy arr = .<Array.copy .~arr>.

  let arr_get arr i = Codelib.genlet .<(.~arr).(.~i)>.

  let arr_set arr i v = .<(.~arr).(.~i) <- .~v>.

end

module C_modulo = struct
  include C
  let mod_ a b = .<.~a mod b>.
end
