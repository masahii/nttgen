open Tagless_impl_staged
open Tagless_fft


(* TODO *)
let get_prim_root n q =
  assert (n = 1024 && q = 12289);
  49

let get_ntt (module Param: Ntt_param) =
  let module C_Int_mod = IntegerModulo(C_modulo)(Param)  in
  (* let module C_NTT = FFT_unrolled_gen(C_Array)(C_Int_mod) in *)
  (* let module C_NTT = FFT_vectorized_unrolled_gen(C_Array)(C_Int_mod) in *)
  (* let module C_NTT = FFT_vectorized_gen(C_Array)(C_Int_mod) in *)
  let module C_NTT = FFT_gen(C_Array)(C_Int_mod) in
  let fn_code = C_NTT.fft Param.n in
  (* Codelib.print_code Format.std_formatter fn_code; print_newline (); *)
  (* Codelib.print_code_as_ast (Codelib.close_code fn_code); *)
  Runnative.run fn_code

let test_ntt_C () =
  let size = 1024 in
  let q = 12289 in
  let qinv = 12287 in
  let omega = get_prim_root size q in
  let module Param = struct let q = q let qinv = qinv let omega = omega let n = size end in
  let open Dft in
  let open DFT(Fft_types.IntegerModulo(Param)) in
  let arr = Array.init size (fun _ -> Random.int Param.q) in
  let ref_res = dft arr in
  let fn = get_ntt (module Param) in
  fn arr;
  ignore(Array.init 1024 (fun i -> Printf.printf "ref %d, tagless %d\n" ref_res.(i)  arr.(i)))
