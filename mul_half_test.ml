let csub x =
  let q = 12289 in
  let tmp_sub = x - q in
  let tmp_sra = Int.shift_right tmp_sub 63 in
  let tmp_and = tmp_sra land q in
  tmp_sub + tmp_and

let mulhi x y =
  let prod = x * y in
  Int.shift_right_logical prod 16

let mullo x y =
  let prod = x * y in
  prod mod (Int.shift_left 1 16)
  (* Printf.printf "prod: %d\n" prod;
   * let shl = (Int.shift_left prod 48) in
   * Printf.printf "shl: %d\n" shl;
   * let shr = Int.shift_right_logical shl 48 in
   * Printf.printf "shr: %d\n" shr;
   * shr *)

let barret_reduce x =
  let q = 12289 in
  let mul_5 = mulhi x 5 in
  csub (x - (mullo mul_5 q))

let not_zero x =
  Bool.to_int (x != 0)

let mul x y =
  (* Printf.printf "input: %d %d\n" x y; *)
  let mlo = mullo x y in (* 0 *)
  (* Printf.printf "mlo: %d\n" mlo; *)
  let mhi = mulhi x y in (* 910 *)
  (* Printf.printf "mhi: %d\n" mhi; *)
  let mlo_qinv = mullo mlo 12287 in (* 0 *)
  (* Printf.printf "mlo_qinv: %d\n" mlo_qinv; *)
  let t = mulhi mlo_qinv 12289 in (* 0 *)
  (* Printf.printf "t: %d\n" t; *)
  let carry = not_zero mlo in (* 0 *)
  (* Printf.printf "carry: %d\n" carry; *)
  let res = mhi + t + carry in
  (* Printf.printf "res: %d\n" res; *)
  let res_csub = csub res in
  (* Printf.printf "res_csub: %d\n" res_csub; *)
  res_csub

(* let _ =
 *   let size = 1024 in
 *   let arr1 = Array.init size (fun _ -> Random.int (12289 * 2)) in
 *   ignore(Array.init size (fun i ->
 *       let ref_res = arr1.(i) mod 12289 in
 *       let res = barret_reduce arr1.(i) in
 *       if ref_res != res then begin
 *         Printf.printf "inp %d\n" arr1.(i);
 *         Printf.printf "ref %d, res %d\n" ref_res res
 *       end)) *)

let _ =
  let size = 1024 in
  let rlog = 16 in
  let r = (Int.shift_left 1 rlog) in
  let arr1 = Array.init size (fun _ -> Random.int 12289) in
  let arr2 = Array.init size (fun _ -> Random.int 12289) in
  (* let arr1 = Array.init size (fun _ -> 8192) in
   * let arr2 = Array.init size (fun _ -> 8192) in *)
  ignore(Array.init size (fun i ->
      let ref_res = (arr1.(i) * arr2.(i)) mod 12289 in
      let res = mul arr1.(i) (arr2.(i) * r mod 12289) in
      if ref_res != res then begin
      Printf.printf "inp %d, %d\n" arr1.(i) arr2.(i);
      Printf.printf "ref %d, res %d\n" ref_res res
    end))
