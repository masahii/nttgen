open Tagless_fft
open Tagless_impl_staged

module C_Complex = ComplexDomain(C_complex)

let test_complex =
  let open Complex in
  let omega_1024_cde = C_Complex.primitive_root_power (C_Complex.int_ 1024) 10 in
  Codelib.print_code Format.std_formatter omega_1024_cde; print_newline ();
  let omega_1024_C = Runnative.run omega_1024_cde in
  Printf.printf "tagless C: %f %f\n" omega_1024_C.re omega_1024_C.im
