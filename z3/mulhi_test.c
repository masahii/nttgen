#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

uint16_t naive(uint16_t x, uint16_t y) {
  uint32_t prod = (uint32_t)x * (uint32_t)y;
  return (uint16_t)(prod >> 16);
}

uint16_t fast(uint16_t u, uint16_t v) {
  uint16_t u0, v0, w0, u1, v1, w1, w2, t;
  u0 = u & 0xFF; u1 = u >> 8;
  v0 = v & 0xFF; v1 = v >> 8;
  w0 = u0 * v0;
  t = u1 * v0 + (w0 >> 8);
  w1 = t & 0xFF;
  w2 = t >> 8;
  w1 = u0 * v1 + w1;
  return u1 * v1 + w2 + (w1 >> 8);
}

int main() {
  for (int i = 0; i < 100; ++i) {
    uint16_t x = (uint16_t)(((rand() / (double)RAND_MAX) * USHRT_MAX));
    uint16_t y = (uint16_t)(((rand() / (double)RAND_MAX) * USHRT_MAX));
    uint16_t r1 = naive(x, y);
    uint16_t r2 = fast(x, y);
    printf("%d, %d, %d, %d\n", x, y, r1, r2);

  }
  return 0;
}
