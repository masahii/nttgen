import numpy as np
import time
from z3 import *
from consts import *


def to_smt2(f, status="unknown", name="benchmark", logic="QF_BV"):
  v = (Ast * 0)()
  return Z3_benchmark_to_smtlib_string(f.ctx_ref(), name, logic, status, "", 0, v, f.as_ast())


Q = 12289
Q_inv = 12287
OMEGA = 49
N = 1024


def csub(x):
    # x: BitVec('x', 16)
    v = x - Q
    return v + ((v >> 15) & Q)


def csub_ref(x):
    return If(x >= Q, x - Q, x)


def to_u32(x):
    zeros = BitVecVal(0, 16)
    return Concat(zeros, x)


def mullo(x, y):
    return x * y


def mulhi_naive(x, y):
    mul = to_u32(x) * to_u32(y)
    return Extract(31, 16, mul)


def mulhi(u, v):
    u0 = u & 0xFF
    u1 = LShR(u, 8)
    v0 = v & 0xFF
    v1 = LShR(v, 8)
    w0 = u0 * v0
    t = u1 * v0 + LShR(w0, 8)
    w1 = t & 0xFF
    w2 = LShR(t, 8)
    w1 = u0 * v1 + w1
    return u1 * v1 + w2 + LShR(w1, 8)


def barrett_reduce(x):
    v1 = mulhi(x, BitVecVal(5, 16))
    rhs = mullo(v1, BitVecVal(Q, 16))
    return x - rhs


def barrett_reduce_ref(x):
    return URem(x, BitVecVal(Q, 16))


def mul_red_ref(x, y):
    mul = to_u32(x) * to_u32(y)
    return Extract(15, 0, URem(mul, BitVecVal(Q, 32)))


def mul_red(x, y):
    mlo = mullo(x, y)
    mhi = mulhi(x, y)
    mlo_qinv = mullo(mlo, BitVecVal(Q_inv, 16))
    t = mulhi(mlo_qinv, BitVecVal(Q, 16))
    carry = If(UGT(mlo, BitVecVal(0, 16)), BitVecVal(1, 16), BitVecVal(0, 16))
    return csub(mhi + t + carry)


def mul_red2(x, y):
    y_mont = Extract(
        15, 0, URem(to_u32(y) * BitVecVal(65536, 32), BitVecVal(Q, 32))
    )
    return mul_red(x, y_mont)


def test_csub():
    x = BitVec("x", 16)
    ref = csub_ref(x)
    opt = csub(x)

    s = Solver()
    s.add(Not(ref == opt))
    print(s.check(And(x > 0, x < 2 * Q)))
    # m = s.model()
    # print(m)
    # print(m.evaluate(ref))
    # print(m.evaluate(opt))


def test_mulhi():
    x = BitVec("x", 16)
    y = BitVec("y", 16)
    ref = mulhi_naive(x, y)
    opt = mulhi(x, y)
    formula = Not(ref == opt)

    with open("mulhi_test.smt2", "w") as f:
        f.write(to_smt2(formula))

    s = Solver()
    s.add(formula)
    # s.add(Not(ref == opt))
    print(s.check())


def test_barrett_reduce():
    x = BitVec("x", 16)
    ref = barrett_reduce_ref(x)
    opt = barrett_reduce(x)

    s = Solver()
    # s.set("produce-proofs", True)
    s.add(Not(ref == csub(opt)))
    s.set("threads", 4)
    print(s.check())
    print(s.solver)

    # print(s.statistics())


def prim_root_powers():
    powers = [1]
    for i in range(1, N // 2):
        powers.append((powers[-1] * OMEGA) % Q)
    return np.unique(powers).astype(np.int)


def test_mul_red():
    x = BitVec("x", 16)
    for i, omega in enumerate(prim_root_powers()):
        # y = BitVec('y', 16)
        y = BitVecVal(int(omega), 16)
        y_mont = Extract(
            15, 0, URem(to_u32(y) * BitVecVal(65536, 32), BitVecVal(Q, 32))
        )
        ref = mul_red_ref(x, y)
        opt = mul_red(x, y_mont)

        formula = Not(ref == opt)
        with open("mul_red_%d.smt2" % int(omega), "w") as f:
            f.write(to_smt2(formula))

        s = Solver()
        s.add(formula)
        # s.set("threads", 6)
        t1 = time.time()
        res = s.check()
        print(i, res, time.time() - t1)
        break

        # s = Solver()
        # s.add(formula)
        # print(i, s.check())

        # m = s.model()
        # print(m)
        # print(m.evaluate(ref))
        # print(m.evaluate(opt))
        # break


def test_mul_red2():
    x = BitVec("x", 16)
    for i, omega in enumerate(prim_root_powers()):
        # y = BitVec('y', 16)
        y = BitVecVal(int(omega), 16)
        ref = mul_red_ref(x, y)
        opt = mul_red2(x, y)


        s = Solver()
        s.set("threads", 4)
        s.add(Not(ref == opt))
        # s.set("threads", 6)
        t1 = time.time()
        res = s.check()
        print(i, res, time.time() - t1)
        break


def bit_reverse(arr):
    for i in range(N):
        if i < bit_rev_table[i]:
            tmp = arr[i]
            arr[i] = arr[bit_rev_table[i]]
            arr[bit_rev_table[i]] = tmp


def sub(x, y, barrett):
    return barrett((x + BitVecVal(2 * Q, 16)) - y)


omega_powers_bitvec = [BitVecVal(v, 16) for v in omega_powers]
omega_powers_mont_bitvec = [BitVecVal(v, 16) for v in omega_powers_mont]


def fft_(arr, barrett, mul, omegas):
    bit_reverse(arr)

    for s in range(1, 11):
        m = 1 << s
        coeff_begin = (1 << (s - 1)) - 1
        m_half = m // 2
        for k in range(0, N, m):
            for j in range(m_half):
                index = k + j
                omega = omegas[coeff_begin + j]
                u = arr[index]
                t = simplify(mul(arr[index + m_half], omega))
                sm = u + t

                if s % 2 == 0:
                    arr[index] = simplify(barrett(sm))
                else:
                    arr[index] = sm

                arr[index + m_half] = simplify(sub(u, t, barrett))


def fft(arr):
    return fft_(arr, barrett_reduce, mul_red, omega_powers_mont_bitvec)


def fft2(arr):
    return fft_(arr, barrett_reduce_ref, mul_red_ref, omega_powers_bitvec)


def test_fft():
    # arr = [BitVec("x%d" % i, 16) for i in range(N)]
    inp = np.loadtxt("input.txt").astype(np.int)
    arr = [BitVecVal(int(x), 16) for x in inp]
    fft(arr)

    output = [simplify(URem(x, BitVecVal(Q, 16))) for x in arr]
    # output = arr

    for i in range(N):
        print(output[i])


def dft(arr, omega):
    def mul_const(x, y):
        return (x * y) % Q

    def mul(x, y):
        return mul_red_ref(x, y)

    def add(x, y):
        return URem(x + y, BitVecVal(Q, 16))

    def primitive_root_power(j):
        ret = 1
        for i in range(j):
            ret = mul_const(ret, omega)
        return ret

    ret = []
    n = len(arr)
    for k in range(n):
        powers_k = primitive_root_power(k)
        powers_kj = 1
        dot = arr[0]
        for j in range(1, n):
            powers_kj = mul_const(powers_kj, powers_k)
            dot = add(dot, mul(BitVecVal(powers_kj, 16), arr[j]))
        ret.append(simplify(dot))

    return ret


def test_dft():
    # arr = [BitVec("x%d" % i, 16) for i in range(N)]
    inp = np.loadtxt("input.txt").astype(np.int)
    arr = [BitVecVal(int(x), 16) for x in inp]
    out = dft(arr, OMEGA)

    for i in range(N):
        print(out[i])


def fft_rec(arr):
    dft1 = dft(arr[0:N:2], (OMEGA ** 2) % Q)
    dft2 = dft(arr[1:N:2], (OMEGA ** 2) % Q)

    omegas = omega_powers_bitvec
    mul = mul_red_ref

    s = 10
    m = 1 << s
    coeff_begin = (1 << (s - 1)) - 1
    m_half = m // 2
    out = [0] * N
    for j in range(m_half):
        omega = omegas[coeff_begin + j]
        u = dft1[j]
        t = simplify(mul(dft2[j], omega))
        sm = u + t

        if s % 2 == 0:
            out[j] = simplify(barrett_reduce_ref(sm))
        else:
            out[j] = sm

        out[j + m_half] = simplify(sub(u, t, barrett_reduce_ref))

    return out


def test_fft_dft_equiv():
    Q_bv = BitVecVal(Q, 16)
    arr = [BitVec("x%d" % i, 16) for i in range(N)]
    input_constraint = And([x < Q_bv for x in arr])

    dft_out = dft(arr, OMEGA)
    print("dft done")

    # fft2(arr)
    # fft_out = [simplify(URem(x, Q_bv)) for x in arr]
    fft_out = fft_rec(arr)
    print("fft done")

    for i, (o1, o2) in enumerate(zip(dft_out, fft_out)):
        formula = Not(o1 == o2)
        with open("fft_rec/dft_vs_fft%d.smt2" % i, "w") as f:
            f.write(to_smt2(formula))

        s = Solver()
        s.add(formula)
        print(i, s.check())


def fft_rec_test():
    inp = np.loadtxt("input.txt").astype(np.int)
    arr = [BitVecVal(int(x), 16) for x in inp]

    out = fft_rec(arr)
    output = [simplify(URem(x, BitVecVal(Q, 16))) for x in out]

    for i in range(N):
        print(output[i])


# test_mulhi()
# test_csub()
# test_barrett_reduce()
# test_dft()
# test_fft_dft_equiv()
# test_fft()
# fft_rec_test()
x = BitVec("x", 16)
ref = barrett_reduce_ref(x)
opt = barrett_reduce(x)

s = Solver()
# s.set("produce-proofs", True)
s.add(Not(ref == csub(opt)))
s.set("threads", 4)
print(s.check())
print(s.solver)
