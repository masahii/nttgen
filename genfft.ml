open Fft_types
open Fft_domain


let split arr =
  let n = Array.length arr in
  let evens = Array.init (n/2) (fun i -> arr.(2 * i)) in
  let odds = Array.init (n/2) (fun i -> arr.(2 * i + 1)) in
  evens, odds


(* module MakeFFT_topdown(D: Domain) = struct
 *   module Prim_roots = Primitive_roots(D)
 *
 *   let fft n =
 *     let merge evens odds roots =
 *       let half = Array.length evens in
 *       let current_size = half * 2 in
 *       let ratio = n / current_size in
 *       let multiplied = Array.init half (fun i -> D.mul odds.(i) roots.(i * ratio)) in
 *       Array.init current_size (fun i -> if i < half then D.add evens.(i)  multiplied.(i)
 *                      else D.sub evens.(i - half) multiplied.(i - half))
 *     in
 *     let rec fft_rec input roots =
 *       let n = Array.length input in
 *       if n = 1 then input
 *       else
 *         let evens, odds = split input in
 *         merge (fft_rec evens roots) (fft_rec odds roots) roots
 *     in
 *     let prim_roots = Prim_roots.powers n (n / 2) 1 in
 *     fun input -> fft_rec input prim_roots
 *
 * end *)


module MakeFFT_topdown_staged(Staged_D: StagedDomain) = struct
  module D = Staged_D.Sta_D
  module FFT_staged = Fft_staged.MakeFFT(Staged_D)
  module Prim_roots = Primitive_roots(D)

  let fft_unrolled_16 =
    let exponent = S(S(S(S(Z)))) in
    let cde = FFT_staged.mk exponent in
    (* Codelib.print_code Format.std_formatter cde; print_newline (); *)
    Runnative.run cde

  let fft n =
    let merge evens odds roots =
      let half = Array.length evens in
      let current_size = half * 2 in
      let s = Base.Int.floor_log2 half in
      let coeff_begin = (Int.shift_left 1 (s - 1)) - 1 in
      let multiplied = Array.init half (fun i -> D.mul odds.(i) roots.(coeff_begin + i)) in
      Array.init current_size (fun i -> if i < half then D.add evens.(i)  multiplied.(i)
                     else D.sub evens.(i - half) multiplied.(i - half))
    in
    let rec fft_rec input roots =
      let n = Array.length input in
      if n = 16 then fft_unrolled_16 input
      else
        let evens, odds = split input in
        merge (fft_rec evens roots) (fft_rec odds roots) roots
    in
    let prim_roots = Prim_roots.powers_memory_efficient n in
    fun input -> fft_rec input prim_roots

end


module MakeFFT_bottomup(D: Domain)(Bit_rev: sig val bit_reverse_table: int array end) = struct
  module Prim_roots = Primitive_roots(D)

  let bit_reverse_table = Bit_rev.bit_reverse_table
  let bit_reverse arr =
    let length =  Array.length arr in
    assert (length = (Array.length bit_reverse_table));
    for i = 0 to length - 1 do
      let r = bit_reverse_table.(i) in
      if i < r then begin
        let tmp = arr.(i) in
        arr.(i) <- arr.(r);
        arr.(r) <- tmp
      end
    done

  let fft n =
    let prim_roots = Prim_roots.powers_memory_efficient n in
    fun arr ->
      bit_reverse arr;
      let n = Array.length arr in
      let num_stage = Base.Int.floor_log2 n in
      for s = 1 to num_stage do
        let m = Int.shift_left 1 s in
        let coeff_begin = (Int.shift_left 1 (s - 1)) - 1 in
        let num_butterfly = Int.shift_right n s in
        for i = 0 to num_butterfly - 1 do
          let k = i * m in
          for j = 0 to (m / 2 - 1) do
            let omega = prim_roots.(coeff_begin + j) in
            let t = D.mul omega arr.(k + j + m/2) in
            let u = arr.(k + j) in
            arr.(k + j) <- D.add u t;
            arr.(k + j + m/2) <- D.sub u t;
          done;
        done;
      done
end

open Bit_reverse
open Dft

let test_complex () =
  let size = 1024 in
  let open DFT(ComplexDomain) in
  let open Complex in
  let input = Array.init size (fun _ -> {re=Random.float 1.0; im=Random.float 1.0}) in
  let ref_res = dft input in
  let open MakeFFT_topdown_staged(ComplexStagedDomain) in
  let res = fft size input in
  Array.iter2 (fun {re=re1;im=im1} {re=re2;im=im2} -> Printf.printf "Staged: (%f, %f), Ref: (%f, %f)\n" re1 im1 re2 im2) res ref_res;

  let open MakeFFT_bottomup(ComplexDomain)(Bit_rev_1024) in
  fft size input;
  Array.iter2 (fun {re=re1;im=im1} {re=re2;im=im2} -> Printf.printf "Staged: (%f, %f), Ref: (%f, %f)\n" re1 im1 re2 im2) input ref_res


module NewHope_param = struct
  let n = 1024
  let q = 12289
  let qinv = 12287
  let omega = 49
end

(* let test_ntt () =
 *   let size = 1024 in
 *   let input = Array.init size (fun _ -> Random.int NewHope_param.q) in
 *   let open DFT(IntegerModulo(NewHope_param)) in
 *   let ref_res = dft input in
 *
 *   let open MakeFFT_topdown(IntegerModulo(NewHope_param)) in
 *   let res = fft size input in
 *   Array.iter2 (fun x y -> Printf.printf "Staged: %d, Ref: %d\n" x y) res ref_res;
 *
 *   let open MakeFFT_bottomup(IntegerModulo(NewHope_param))(Bit_rev_1024) in
 *   fft size input;
 *   Array.iter2 (fun x y -> Printf.printf "Staged: %d, Ref: %d\n" x y) input ref_res *)

let benchmark () =
  let size = 1024 in
  let input = Array.init size (fun _ -> Random.int NewHope_param.q) in
  (* let module TopDown = MakeFFT_topdown(IntegerModulo(NewHope_param)) in *)
  let module TopDown_staged = MakeFFT_topdown_staged(IntegerModuloStaged(NewHope_param)) in
  let module BottomUp = MakeFFT_bottomup(IntegerModulo(NewHope_param))(Bit_rev_1024) in
  (* let ntt_top_down = TopDown.fft size in *)
  let ntt_top_down_staged = TopDown_staged.fft size in
  let ntt_bottom_up = BottomUp.fft size in
  let ntt_bottom_up_tagless = Tagless_ntt_staged.get_ntt (module NewHope_param) in
  let ntt_bottom_up_tagless_int32 = Tagless_ntt_c.get_ntt_int32 (module NewHope_param) in
  let ntt_bottom_up_tagless_uint16 = Tagless_ntt_c.get_ntt_uint16 (module NewHope_param) in
  let open Ctypes in
  let c_arr_u16 = CArray.of_list uint16_t (List.init size (fun i -> Unsigned.UInt16.of_int input.(i))) in
  let c_arr_u16_ptr = CArray.start c_arr_u16 in
  let c_arr_i32 = CArray.of_list int (List.init size (fun i -> input.(i))) in
  let c_arr_i32_ptr = CArray.start c_arr_i32 in
  (* ignore(Benchmark.latency1 10000L ntt_top_down input ~name:"top down"); *)
  ignore(Benchmark.latency1 10000L ntt_top_down_staged input ~name:"top down staged base case");
  ignore(Benchmark.latency1 10000L ntt_bottom_up input ~name:"bottom up");
  ignore(Benchmark.latency1 20000L ntt_bottom_up_tagless input ~name:"tagless bottom up");
  ignore(Benchmark.latency1 50000L ntt_bottom_up_tagless_uint16 c_arr_u16_ptr ~name:"tagless c u16");
  ignore(Benchmark.latency1 50000L ntt_bottom_up_tagless_int32 c_arr_i32_ptr ~name:"tagless c i32")

let _ =
  benchmark ()
