open Tagless_impl_c
open Tagless_types
open Printf

module type Elem_ty = sig
  type t
  val of_int: int -> t
  val to_int: t -> int
  val rep_c_type: t c_type
end


module type SIMD_Instr = sig
  type 'a expr

  module T: Elem_ty

  type t = T.t
  type n

  val vec_len_exponent: n nat

  val broadcast: t -> (t, n) vec expr

  val add: (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr
  val sub: (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr
  val mullo: (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr

  val bitwise_and: (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr

  val shift_right_l: (t, n) vec expr -> int -> (t, n) vec expr
  val shift_right_a: (t, n) vec expr -> int -> (t, n) vec expr

  module Infix: sig
    val (%+): (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr
    val (%-): (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr
    val (%&): (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr
    val (%>>): (t, n) vec expr -> int -> (t, n) vec expr
  end

end

module type SIMD_Mixed_Instr = sig
  type n_half

  include SIMD_Instr with type n = n_half s

  module T_wide: Elem_ty
  type t_wide = T_wide.t

  (* example
    n_half = 8
    n = 16
    t = uint16
    t_wide = uint32
  *)

  val mulhi: (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr

  (* 16 bit x 16 -> 16 bit x 16 -> 32 bit x 8 *)
  val unpacklo: (t, n) vec expr -> (t, n) vec expr -> (t_wide, n_half) vec expr
  val unpackhi: (t, n) vec expr -> (t, n) vec expr -> (t_wide, n_half) vec expr

  (* 32 x 8 -> 32 x 8 -> 16 x 16 *)
  val pack: (t_wide, n_half) vec expr -> (t_wide, n_half) vec expr -> (t, n) vec expr
  val concat: (t_wide, n_half) vec expr -> (t_wide, n_half) vec expr -> (t, n) vec expr

  (* 16 x 16 -> 32 x 8*)
  val convertlo: (t, n) vec expr -> (t_wide, n_half) vec expr
  val converthi: (t, n) vec expr -> (t_wide, n_half) vec expr
end

module Integer_modulo_SIMD
    (Base_lang: C_lang_aux)
    (Instr_wide: SIMD_Instr with type 'a expr = 'a Base_lang.expr)
    (Instr: SIMD_Mixed_Instr
     with type n_half = Instr_wide.n
     with module T_wide = Instr_wide.T
     with type 'a expr = 'a Base_lang.expr)
    (Param: Tagless_fft.Ntt_param) = struct
  open Base_lang
  type t = Instr.t
  type t_wide = Instr.t_wide
  type 'a expr = 'a Base_lang.expr
  (* representation of vector with 2^n elements *)
  type 'a vec = ('a, Instr.n) Tagless_types.vec
  (* representation of vector with 2^(n-1) elements *)
  type 'a vec_wide = ('a, Instr_wide.n) Tagless_types.vec

(* __m256i csub32(__m256i int32_x8) {
 *   __m256i v12289 = _mm256_set1_epi32(12289);
 *   __m256i v_0 = _mm256_sub_epi32(int32_x8, v12289);
 *   return _mm256_add_epi32(v_0, _mm256_and_si256(_mm256_srai_epi32(v_0, 31), v12289));
 * } *)

  let csub: t_wide vec_wide expr -> t_wide vec_wide expr = fun v ->
    let open Instr_wide in
    let open Infix in
    let vec_q = broadcast (T.of_int Param.q) in
    let tmp_sub = v %- vec_q in
    let tmp_sra = shift_right_a tmp_sub 31 in
    let tmp_and = tmp_sra %& vec_q in
    add tmp_sub tmp_and

  let csub =
    let in_ty = CVec (Instr_wide.T.rep_c_type, Instr_wide.vec_len_exponent) in
    func "csub_vec" in_ty (fun v -> return_ (csub v))

(* __m256i barret_reduce_aux(__m256i int32_x8) {
 *   __m256i v5 = _mm256_set1_epi32(5);
 *   __m256i v12289 = _mm256_set1_epi32(12289);
 *   __m256i v_1 = _mm256_srli_epi32(_mm256_mullo_epi32(int32_x8, v5), 16);
 *   __m256i v_2 = _mm256_sub_epi32(int32_x8, _mm256_mullo_epi32(v_1, v12289));
 *   return csub32(v_2);
 * }
 *
 * __m256i barret_reduce(__m256i int16_x16) {
 *   __m256i lo = barret_reduce_aux(_mm256_cvtepu16_epi32(_mm256_extracti128_si256(int16_x16, 0)));
 *   __m256i hi = barret_reduce_aux(_mm256_cvtepu16_epi32(_mm256_extracti128_si256(int16_x16, 1)));
 *   return _mm256_permute4x64_epi64(_mm256_packus_epi32(lo, hi), 0xD8);
 * } *)

  (* (16 x 16) *)
  let barret_reduce: t vec expr -> t vec expr = fun v ->
    (* (32 x 8) *)
    let barret_reduce_aux: t_wide vec_wide expr -> t_wide vec_wide expr = fun v ->
      let open Instr_wide in
      let open Infix in
      let vec_5 = broadcast (T.of_int 5) in
      let vec_q = broadcast (T.of_int Param.q) in
      let tmp_mullo = mullo v vec_5 in
      let tmp_srl = tmp_mullo %>> 16 in
      let tmp_mullo = mullo tmp_srl vec_q in
      let tmp_sub = v %- tmp_mullo in
      (* app csub tmp_sub *)
      tmp_sub
    in
    let barret_reduce_aux =
      let in_ty = CVec (Instr_wide.T.rep_c_type, Instr_wide.vec_len_exponent) in
      func "barret_reduce_aux_vec" in_ty (fun v -> return_ (barret_reduce_aux v)) in
    let open Instr in
    let lo = app barret_reduce_aux (convertlo v) in
    let hi = app barret_reduce_aux (converthi v) in
    concat lo hi

  let barret_reduce =
    let in_ty = CVec (Instr.T.rep_c_type, Instr.vec_len_exponent) in
    func "barret_reduce_vec" in_ty (fun v -> return_ (barret_reduce v))


(* __m256i montgomery_reduce(__m256i uint32_x8) {
 *   __m256i v262143 = _mm256_set1_epi32(262143);
 *   __m256i v12287 = _mm256_set1_epi32(12287);
 *   __m256i v12289 = _mm256_set1_epi32(12289);
 *   __m256i v_3 = _mm256_mullo_epi32(_mm256_and_si256(uint32_x8, v262143), v12287);
 *   __m256i v_4 = _mm256_and_si256(v_3, v262143);
 *   __m256i v_5 = _mm256_mullo_epi32(v_4, v12289);
 *   __m256i v_6 = _mm256_add_epi32(uint32_x8, v_5);
 *   __m256i v_7 = _mm256_srli_epi32(v_6, 18);
 *   return csub32(v_7);
 * } *)

  let montgomery_reduce: t_wide vec_wide expr -> t_wide vec_wide expr = fun v ->
    let open Instr_wide in
    let open Infix in
    let rlog = 16 in
    let r = (Int.shift_left 1 rlog) in
    let vec_r_minus_one = broadcast (T.of_int (r - 1)) in
    let vec_qinv = broadcast (T.of_int Param.qinv) in
    let vec_q = broadcast (T.of_int Param.q) in
    let tmp_and = v %& vec_r_minus_one in
    let tmp_mullo = mullo tmp_and vec_qinv in
    let tmp_and = tmp_mullo %& vec_r_minus_one in
    let tmp_mullo = mullo tmp_and vec_q in
    let tmp_add = v %+ tmp_mullo in
    let tmp_srl = tmp_add %>> rlog in
    (* app csub tmp_srl *)
    tmp_srl


  let montgomery_reduce =
    let in_ty = CVec (Instr_wide.T.rep_c_type, Instr_wide.vec_len_exponent) in
    func "montgomery_reduce_vec" in_ty (fun v -> return_ (montgomery_reduce v))

  let vadd: t vec expr -> t vec expr-> t vec expr = fun v1 v2 ->
    let in_ty = CVec (Instr.T.rep_c_type, Instr.vec_len_exponent) in
    let f =  func2 "vadd" in_ty in_ty (fun v1 v2 ->
        let open Instr in
        return_ (app barret_reduce (add v1 v2)))
    in
    app2 f v1 v2

  let vsub v1 v2 =
    let in_ty = CVec (Instr.T.rep_c_type, Instr.vec_len_exponent) in
    let f = func2 "vsub" in_ty in_ty (fun v1 v2 ->
        let open Instr in
        let open Infix in
        let bias = broadcast (T.of_int (Param.q * 3)) in
        return_ (app barret_reduce ((v1 %+ bias) %- v2)))
    in
    app2 f v1 v2

(* // Input: 16 x 16 vectors
 * // Output: 16 x 16 vectors
 * __m256i vmul16(__m256i lhs, __m256i rhs) {
 *   __m256i lo16 = _mm256_mullo_epi16(lhs, rhs);
 *   __m256i hi16 = _mm256_mulhi_epu16(lhs, rhs);
 *   __m256i lo32_8 = montgomery_reduce(_mm256_unpacklo_epi16(lo16, hi16));
 *   __m256i hi32_8 = montgomery_reduce(_mm256_unpackhi_epi16(lo16, hi16));
 *   return _mm256_packus_epi32(lo32_8, hi32_8);
 * } *)

  let vmul: t vec expr -> t vec expr -> t vec expr = fun v1 v2 ->
    let in_ty = CVec (Instr.T.rep_c_type, Instr.vec_len_exponent) in
    let f = func2 "vmul" in_ty in_ty (fun v1 v2 ->
        let open Instr in
        let lo = mullo v1 v2 in
        let hi = mulhi v1 v2 in
        let lo_reduced = app montgomery_reduce (unpacklo lo hi) in
        let hi_reduced = app montgomery_reduce (unpackhi lo hi) in
        return_ (pack lo_reduced hi_reduced))
    in
    app2 f v1 v2

end

module type SIMD_basic = sig
  val add: string
  val sub: string
  val mullo: string
  val broadcast: string
  val shift_right_l: string
  val shift_right_a: string
  val bitwise_and: string
end

module type SIMD_Mixed = sig
  include SIMD_basic
  val mulhi: string
  val unpacklo: string
  val unpackhi: string
  val pack: string
  val concat: string -> string -> string
  val converthi: string -> string
  val convertlo: string -> string
end

module type Length_spec = sig
  type n
  val vec_len_exponent: n nat
end

module Make_SIMD_Instr(T: Elem_ty)(Instr: SIMD_basic)(L: Length_spec) = struct
  type 'a expr = 'a c_type * string
  type n = L.n
  type t = T.t

  module T = T

  let vec_len_exponent = L.vec_len_exponent

  let broadcast: t -> (t, n) vec expr = fun s ->
    CVec (T.rep_c_type, L.vec_len_exponent), sprintf "%s(%d)" Instr.broadcast (T.to_int s)

  let binop: string -> (t, n) vec expr -> (t, n) vec expr -> (t, n) vec expr = fun op (ty1, v1) (ty2, v2) ->
    match ty1, ty2 with
    | CVec(_, _), CVec(_, _) ->
      (ty1, sprintf "%s(%s, %s)" op v1 v2)
    | _ -> assert false

  let add = binop Instr.add
  let sub = binop Instr.sub
  let mullo = binop Instr.mullo
  let bitwise_and = binop Instr.bitwise_and

  let shift_right: string -> (t, n) vec expr -> int -> (t, n) vec expr = fun op (ty1, v1) i ->
    match ty1 with
    | CVec(_, _) ->
      (ty1, sprintf "%s(%s, %d)" op v1 i)
    | _ -> assert false

  let shift_right_l = shift_right Instr.shift_right_l
  let shift_right_a = shift_right Instr.shift_right_a

  module Infix = struct
    let (%+) = add
    let (%-) = sub
    let (%&) = bitwise_and
    let (%>>) = shift_right_l
  end
end

module Make_SIMD_Mixed_Instr(T: Elem_ty)(T_wide: Elem_ty)(Instr: SIMD_Mixed)(Instr_wide: SIMD_Instr) = struct
  module L = struct
    type n = Instr_wide.n s
    let vec_len_exponent = S Instr_wide.vec_len_exponent
  end
  include Make_SIMD_Instr(T)(Instr)(L)

  type n_half = Instr_wide.n

  type t_wide = T_wide.t

  module T_wide = T_wide

  let vec_len_half_exponent = Instr_wide.vec_len_exponent

  let mulhi = binop Instr.mulhi

  let unpack: string -> (t, n) vec expr -> (t, n) vec expr -> (t_wide, n_half) vec expr = fun op (ty1, v1) (ty2, v2) ->
    match ty1, ty2 with
    | CVec(_, _), CVec(_, _) ->
      CVec (T_wide.rep_c_type, vec_len_half_exponent), sprintf "%s(%s, %s)" op v1 v2
    | _ -> assert false


  let unpacklo = unpack Instr.unpacklo
  let unpackhi = unpack Instr.unpackhi

  let pack: (t_wide, n_half) vec expr -> (t_wide, n_half) vec expr -> (t, n) vec expr = fun (ty1, v1) (ty2, v2) ->
    match ty1, ty2 with
    | CVec(_, _), CVec(_, _) ->
      CVec (T.rep_c_type, L.vec_len_exponent), sprintf "%s(%s, %s)" Instr.pack v1 v2
    | _ -> assert false

  let concat: (t_wide, n_half) vec expr -> (t_wide, n_half) vec expr -> (t, n) vec expr = fun (ty1, v1) (ty2, v2) ->
    match ty1, ty2 with
    | CVec(_, _), CVec(_, _) ->
      CVec (T.rep_c_type, L.vec_len_exponent), Instr.concat v1 v2
    | _ -> assert false

  let convert: (string -> string) -> (t, n) vec expr -> (t_wide, n_half) vec expr = fun op (ty, v1) ->
    match ty with
    | CVec(_, _) ->
      CVec (T_wide.rep_c_type, vec_len_half_exponent), op v1
    | _ -> assert false

  let convertlo = convert Instr.convertlo
  let converthi = convert Instr.converthi
end

module AVX2_v8_length_spec = struct
  type n = z s s s
  let vec_len_exponent = S (S (S Z))
end

module AVX2_v8_Instr : SIMD_basic = struct
  let add = "_mm256_add_epi32"
  let sub = "_mm256_sub_epi32"
  let mullo = "_mm256_mullo_epi32"
  let broadcast = "_mm256_set1_epi32"
  let shift_right_l = "_mm256_srli_epi32"
  let shift_right_a = "_mm256_srai_epi32"
  let bitwise_and = "_mm256_and_si256"
end

module AVX2_v16_Instr : SIMD_basic = struct
  let add = "_mm256_add_epi16"
  let sub = "_mm256_sub_epi16"
  let mullo = "_mm256_mullo_epi16"
  let broadcast = "_mm256_set1_epi16"
  let shift_right_l = "_mm256_srli_epi16"
  let shift_right_a = "_mm256_srai_epi16"
  let bitwise_and = "_mm256_and_si256"
end

module AVX2_v16_Mixed : SIMD_Mixed = struct
  include AVX2_v16_Instr
  let mulhi = "_mm256_mulhi_epu16"
  let unpacklo = "_mm256_unpacklo_epi16"
  let unpackhi = "_mm256_unpackhi_epi16"
  let pack = "_mm256_packus_epi32"
  let concat lo hi =
    sprintf "_mm256_permute4x64_epi64(_mm256_packus_epi32(%s, %s), 0xD8)" lo hi
  let convertlo v =
    sprintf "_mm256_cvtepu16_epi32(_mm256_extracti128_si256(%s, 0))" v
  let converthi v =
    sprintf "_mm256_cvtepu16_epi32(_mm256_extracti128_si256(%s, 1))" v
end

module U16 = struct
  type t = Unsigned.UInt16.t
  let of_int = Unsigned.UInt16.of_int
  let to_int = Unsigned.UInt16.to_int
  let rep_c_type = CUInt16
end

module U32 = struct
  type t = Unsigned.UInt32.t
  let of_int = Unsigned.UInt32.of_int
  let to_int = Unsigned.UInt32.to_int
  let rep_c_type = CUInt32
end

module AVX2_UInt16_int_modulo(Scalar_domain: Domain)(Param: Tagless_fft.Ntt_param) = struct
  module SIMD_v8_u32 = Make_SIMD_Instr(U32)(AVX2_v8_Instr)(AVX2_v8_length_spec)
  module SIMD_v16_u16 = Make_SIMD_Mixed_Instr(U16)(U32)(AVX2_v16_Mixed)(SIMD_v8_u32)

  module Vector_domain = Integer_modulo_SIMD(C_codegen)(SIMD_v8_u32)(SIMD_v16_u16)(Param)
  type 'a vec = 'a Vector_domain.vec

  include C_codegen

  type t = Scalar_domain.t

  let vec_len = 16

  module Mem = struct
    type t = Scalar_domain.t
    type n = SIMD_v16_u16.n

    let vec_len_exponent = SIMD_v16_u16.vec_len_exponent

    let vload = "_mm256_loadu_si256"
    let vstore = "_mm256_storeu_si256"
  end

  module M = SIMD_Mem(Mem)

  let vload = M.vload
  let vstore = M.vstore

end
