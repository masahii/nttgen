#include <stdio.h>
#include <stdint.h>
#include <immintrin.h>
#include <limits.h>

__m256i csub32(__m256i int32_x8) {
  __m256i v12289 = _mm256_set1_epi32(12289);
  __m256i v_0 = _mm256_sub_epi32(int32_x8, v12289);
  return _mm256_add_epi32(v_0, _mm256_and_si256(_mm256_srai_epi32(v_0, 31), v12289));
}

__m256i barret_reduce_aux(__m256i int32_x8) {
  __m256i v5 = _mm256_set1_epi32(5);
  __m256i v12289 = _mm256_set1_epi32(12289);
  __m256i v_1 = _mm256_srli_epi32(_mm256_mullo_epi32(int32_x8, v5), 16);
  __m256i v_2 = _mm256_sub_epi32(int32_x8, _mm256_mullo_epi32(v_1, v12289));
  return csub32(v_2);
}

__m256i barret_reduce(__m256i int16_x16) {
  __m256i v5 = _mm256_set1_epi16(5);
  __m256i v12289 = _mm256_set1_epi16(12289);
  __m256i v_1 = _mm256_mulhi_epi16(int16_x16, v5);
  __m256i v_2 = _mm256_sub_epi16(int16_x16, _mm256_mullo_epi16(v_1, v12289));
  return v_2;
}

// Input: 32 x 8 vector
// Output: 32 x 8 vector
__m256i montgomery_reduce(__m256i uint32_x8) {
  __m256i v262143 = _mm256_set1_epi32(262143);
  __m256i v12287 = _mm256_set1_epi32(12287);
  __m256i v12289 = _mm256_set1_epi32(12289);
  __m256i v_3 = _mm256_mullo_epi32(_mm256_and_si256(uint32_x8, v262143), v12287);
  __m256i v_4 = _mm256_and_si256(v_3, v262143);
  __m256i v_5 = _mm256_mullo_epi32(v_4, v12289);
  __m256i v_6 = _mm256_add_epi32(uint32_x8, v_5);
  __m256i v_7 = _mm256_srli_epi32(v_6, 18);
  return csub32(v_7);
}

__m256i csub_vec_16(__m256i arg0) {
  __m256i q = _mm256_set1_epi16(12289);
  return _mm256_add_epi16(
      _mm256_sub_epi16(arg0, q),
      _mm256_and_si256(_mm256_srai_epi16(_mm256_sub_epi16(arg0, q), 15), q));
}

__m256i montgomery_reduce_vec(__m256i arg0, __m256i mullo_qinv) {
  return _mm256_srli_epi32(
      _mm256_add_epi32(
          arg0, _mm256_mullo_epi32(mullo_qinv, _mm256_set1_epi32(12289))),
      16);
}

__m256i vmul16_old(__m256i arg0, __m256i arg1) {
  __m256i mullo = _mm256_mullo_epi16(arg0, arg1);
  __m256i mulhi = _mm256_mulhi_epu16(arg0, arg1);
  __m256i mullo_qinv = _mm256_mullo_epi16(mullo, _mm256_set1_epi16(12287));
  __m256i zero = _mm256_set1_epi16(0);
  return csub_vec_16(_mm256_packus_epi32(
      montgomery_reduce_vec(_mm256_unpacklo_epi16(mullo, mulhi),
                            _mm256_unpacklo_epi16(mullo_qinv, zero)),
      montgomery_reduce_vec(_mm256_unpackhi_epi16(mullo, mulhi),
                            _mm256_unpackhi_epi16(mullo_qinv, zero))));
}

__m256i vmul16(__m256i arg0, __m256i arg1) {
  __m256i mullo = _mm256_mullo_epi16(arg0, arg1);
  __m256i mulhi = _mm256_mulhi_epu16(arg0, arg1);
  __m256i mullo_qinv = _mm256_mullo_epi16(mullo, _mm256_set1_epi16(12287));
  __m256i t = _mm256_mulhi_epu16(mullo_qinv, _mm256_set1_epi16(12289));
  __m256i zero = _mm256_set1_epi16(0);
  __m256i has_carry =
      _mm256_and_si256(_mm256_xor_si256(_mm256_cmpeq_epi16(mullo, zero),
                                        _mm256_set1_epi16(0xFFFF)),
                       _mm256_set1_epi16(1));

  /* uint16_t tmp[16], tmp2[16], tmp3[16]; */
  /* _mm256_storeu_si256((__m256i *)(tmp), mullo); */
  /* _mm256_storeu_si256((__m256i *)(tmp2), has_carry2); */
  /* _mm256_storeu_si256((__m256i *)(tmp3), mask); */
  /* for (int i = 0; i < 16; ++i) { */
  /*   uint16_t old = tmp[i]; */
  /*   if (tmp[i] > 0) tmp[i] = 1; */
  /*   if (tmp[i] != tmp2[i]) { */
  /*     printf("(%d, %d, %d, %d) ", tmp[i], tmp2[i], tmp3[i], old); */
  /*   } */
  /* } */
  /* printf("\n"); */
  /* __m256i has_carry = _mm256_loadu_si256((__m256i*)tmp); */

  return csub_vec_16(_mm256_add_epi16(_mm256_add_epi16(mulhi, t), has_carry));
}

#define N 1024

int main() {
  if (0) {
    uint16_t A[N], B[N], C[N];
    uint16_t q = 12289;
    int rlog = 18;
    uint16_t factor = USHRT_MAX;
    for (int i = 0; i < N; ++i) {
      A[i] = (uint16_t)(((rand() / (double)RAND_MAX) * factor)) % q;
      B[i] = (uint16_t)(((rand() / (double)RAND_MAX) * factor)) % q;
    }
    for (int i = 0; i < N; i += 16) {
      __m256i a = _mm256_loadu_si256((__m256i *)(A + i));
      __m256i b = _mm256_loadu_si256((__m256i *)(B + i));
      __m256i c = barret_reduce(_mm256_add_epi16(a, b));
      _mm256_storeu_si256((__m256i *)(C + i), c);
    }

    for (int i = 0; i < N; ++i) {
      uint16_t ref = (A[i] + B[i]) % q;
      if (C[i] % q != ref) {
        printf("Wrong result: %d, %d, %d, %d\n", A[i], B[i], C[i], ref);
      } else {
        // printf("Correct %d, %d, %d, %d\n", A[i], B[i], C[i], ref);
      }
    }
  }

  if (1) {
    uint16_t A[N], B[N], B_mont[N], C[N];
    uint16_t q = 12289;
    int rlog = 16;
    uint16_t factor = USHRT_MAX;
    for (int i = 0; i < N; ++i) {
      A[i] = (uint16_t)(((rand() / (double)RAND_MAX) * factor)) % q;
      B[i] = (uint16_t)(((rand() / (double)RAND_MAX) * factor)) % q;
      B_mont[i] = (((uint32_t)B[i]) * (1 << rlog)) % q;
    }
    for (int i = 0; i < N; i += 16) {
      __m256i a = _mm256_loadu_si256((__m256i *)(A + i));
      __m256i b = _mm256_loadu_si256((__m256i *)(B_mont + i));
      __m256i c = vmul16(a, b);
      _mm256_storeu_si256((__m256i *)(C + i), c);
    }

    for (int i = 0; i < N; ++i) {
      uint16_t ref = ((uint32_t)A[i] * (uint32_t)B[i]) % q;
      if (C[i] % q != ref) {
        printf("Wrong result: %d, %d, %d, %d\n", A[i], B[i], C[i], ref);
      } else {
        /* printf("Correct %d, %d, %d, %d\n", A[i], B[i], C[i], ref); */
      }
    }
  }
}
