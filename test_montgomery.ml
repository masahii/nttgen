let c_sub a_1 =
  let v_2 = a_1 - 12289 in v_2 + ((Stdlib.Int.shift_right v_2 16) land 12289)

let mont a_8 =
  let v_9 = a_8 * 12287 in
  let v_10 = v_9 land 262143 in
  let v_11 = v_10 * 12289 in
  let v_12 = a_8 + v_11 in
  let v_13 = Stdlib.Int.shift_right v_12 18 in c_sub v_13
let test =
  let q = 12289 in
  let a = 12290 in
  let rlog = 18 in
  let r = (Int.shift_left 1 rlog) in
  let a_mont = (a * r) mod q in
  Printf.printf "ref = %d, staged = %d\n" (a mod q) (mont a_mont)
