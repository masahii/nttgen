import numpy as np
from consts import *

import sys

sys.setrecursionlimit(2000)

Q = 12289
Q_inv = 12287
OMEGA = 49
N = 1024


class Expr():
    pass


class Sym(Expr):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return str(self.name)


class Const(Expr):
    def __init__(self, v):
        self.value = v

    def __repr__(self):
        return str(self.value)


class BinOp(Expr):
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs


class Add(BinOp):
    def __init__(self, lhs, rhs):
        super().__init__(lhs, rhs)

    def __repr__(self):
        return "%s + %s" % (str(self.lhs), str(self.rhs))


class Sub(BinOp):
    def __init__(self, lhs, rhs):
        super().__init__(lhs, rhs)

    def __repr__(self):
        if isinstance(self.rhs, (Add, Sub)):
            return "%s - (%s)" % (str(self.lhs), str(self.rhs))
        else:
            return "%s - %s" % (str(self.lhs), str(self.rhs))


class Mul(BinOp):
    def __init__(self, lhs, rhs):
        super().__init__(lhs, rhs)

    def __repr__(self):
        if isinstance(self.lhs, (Add, Sub)):
            lhs = "(%s)" % str(self.lhs)
        else:
            lhs = "%s" % str(self.lhs)
        if isinstance(self.rhs, (Add, Sub)):
            rhs = "(%s)" % str(self.rhs)
        else:
            rhs = "%s" % str(self.rhs)
        return "%s * %s" % (lhs, rhs)


def bit_reverse(arr):
    for i in range(N):
        if i < bit_rev_table[i]:
            tmp = arr[i]
            arr[i] = arr[bit_rev_table[i]]
            arr[bit_rev_table[i]] = tmp


def add(x, y):
    if isinstance(x, Const) and isinstance(y, Const):
        return Const((x.value + y.value) % Q)
    if isinstance(x, Const) and x.value == 0:
        return y
    if isinstance(y, Const) and y.value == 0:
        return x
    return Add(x, y)


def sub(x, y):
    if isinstance(x, Const) and isinstance(y, Const):
        return Const((x.value - y.value) % Q)
    if isinstance(y, Const) and y.value == 0:
        return x

    return Sub(x, y)


def mul(x, y):
    if isinstance(x, Const) and isinstance(y, Const):
        return Const((x.value * y.value) % Q)
    if isinstance(x, Const) and x.value == 1:
        return y
    if isinstance(y, Const) and y.value == 1:
        return x
    if isinstance(x, Mul) and isinstance(x.rhs, Const) and isinstance(y, Const):
        return Mul(x.lhs, Const((x.rhs.value * y.value) % Q))
    return Mul(x, y)


def fft(arr):
    bit_reverse(arr)

    for s in range(1, 11):
        m = 1 << s
        coeff_begin = (1 << (s - 1)) - 1
        m_half = m // 2
        for k in range(0, N, m):
            for j in range(m_half):
                index = k + j
                omega = omega_powers[coeff_begin + j]
                u = arr[index]
                t = mul(arr[index + m_half], Const(omega))
                sm = add(u, t)

                arr[index] = sm

                arr[index + m_half] = sub(u, t)
    return arr


def dft(arr):
    omega = OMEGA

    def mul_const(x, y):
        return (x * y) % Q

    def primitive_root_power(j):
        ret = 1
        for i in range(j):
            ret = mul_const(ret, omega)
        return ret

    ret = []
    n = len(arr)
    for k in range(n):
        powers_k = primitive_root_power(k)
        powers_kj = 1
        dot = arr[0]
        for j in range(1, n):
            powers_kj = mul_const(powers_kj, powers_k)
            dot = add(dot, mul(arr[j], Const(powers_kj)))
        ret.append(dot)

    return ret


def poly_simplify(v):
    if isinstance(v, Mul):
        if isinstance(v.lhs, Add):
            return add(poly_simplify(mul(poly_simplify(v.lhs.lhs), v.rhs)),
                       poly_simplify(mul(poly_simplify(v.lhs.rhs), v.rhs)))
        if isinstance(v.lhs, Sub):
            return sub(poly_simplify(mul(poly_simplify(v.lhs.lhs), v.rhs)),
                       poly_simplify(mul(poly_simplify(v.lhs.rhs), v.rhs)))
        if isinstance(v.lhs, Mul):
            return mul(mul(poly_simplify(v.lhs.lhs), poly_simplify(v.lhs.rhs)), v.rhs)

        return v

    if isinstance(v, Add):
        return add(poly_simplify(v.lhs),
                   poly_simplify(v.rhs))

    if isinstance(v, Sub):
        return sub(poly_simplify(v.lhs), poly_simplify(v.rhs))

    return v


def visit(p):
    if isinstance(p, Add):
        visit(p.lhs)
        visit(p.rhs)
    if isinstance(p, Sub):
        print("visit sub")
        visit(p.lhs)
        visit(p.rhs)
    if isinstance(p, Mul):
        print("visit mul")
        visit(p.lhs)
        visit(p.rhs)


def get_poly_coeff(p):
    p_simp = poly_simplify(p)
    coeff = [0] * N
    sign = [1] * N

    def get_leftmost_leaf(t):
        if isinstance(t, Sym):
            return t
        return get_leftmost_leaf(t.lhs)

    def visit(term, is_negative):
        if isinstance(term, Mul):
            assert isinstance(term.lhs, Sym)
            assert isinstance(term.rhs, Const)
            var_name = term.lhs.name
            coeff[int(var_name[1:])] = term.rhs.value
            sign[int(var_name[1:])] = -1 if is_negative else 1
        elif isinstance(term, Sub):
            visit(term.lhs, is_negative)
            visit(term.rhs, not is_negative)
        elif isinstance(term, Add):
            visit(term.lhs, is_negative)
            visit(term.rhs, is_negative)
        elif isinstance(term, Sym):
            var_name = term.name
            coeff[int(var_name[1:])] = 1
            sign[int(var_name[1:])] = -1 if is_negative else 1
        else:
            print(type(term))
            assert False

    visit(p_simp, False)

    return np.array(coeff) * np.array(sign)


def test_simplify():
    p = mul(mul(add(Sym("x0"), Sym("x1") ), Const(5)), Const(6))
    print(p)
    print(poly_simplify(p))


def poly_eval(p, inp):
    if isinstance(p, Const):
        return p
    if isinstance(p, Sym):
        var_name = p.name
        return inp[int(var_name[1:])]
    if isinstance(p, Add):
        return add(poly_eval(p.lhs, inp), poly_eval(p.rhs, inp))
    if isinstance(p, Sub):
        return sub(poly_eval(p.lhs, inp), poly_eval(p.rhs, inp))
    if isinstance(p, Mul):
        return mul(poly_eval(p.lhs, inp), poly_eval(p.rhs, inp))
    assert False


def mod_dot(coeff, inp):
    out = 0
    for c, v in zip(coeff, inp):
        out = (out + (c * v) % Q) % Q
    return out


def poly_eval_test(fft_out_sym, dft_out_sym, inp):
    for i in range(N):
        out1 = poly_eval(poly_simplify(fft_out_sym[i]), inp)
        out2 = poly_eval(poly_simplify(dft_out_sym[i]), inp)
        assert out1.value == out2.value


def poly_eval_random(fft_out_sym, dft_out_sym):
    for i in range(100):
        inp_np = np.random.randint(low=0, high=Q, size=(N,))
        inp = [Const(v) for v in inp_np]
        poly_eval_test(fft_out_sym, dft_out_sym, inp)
        print("ok", i)


def test_fft_dft_equiv(fft_out_sym, dft_out_sym):
    for idx in range(N):
        print(idx)
        coeff_fft = get_poly_coeff(fft_out_sym[idx])
        coeff_dft = get_poly_coeff(dft_out_sym[idx])
        coeff_fft2 = np.array([v if v > 0 else v + Q for v in coeff_fft])
        assert np.all(coeff_fft2 == coeff_dft)


arr = [Sym("x%d" % n) for n in range(N)]
dft_out_sym = dft(arr)
fft_out_sym = fft(arr)
# test_fft_dft_equiv(fft_out_sym, dft_out_sym)
# arr = [Const(v) for v in np.loadtxt("input.txt").astype(np.int)]
# dft_out = dft(arr)
# fft_out = fft(arr)


inp_np = np.loadtxt("input.txt").astype(np.int)
inp = [Const(v) for v in inp_np]
# poly_eval(dft_out_sym[idx], inp)

# poly_eval_test(fft_out_sym, dft_out_sym, inp)
# poly_eval_random(fft_out_sym, dft_out_sym)
idx = 1023
with open("out_%d_fft" % idx, "w") as f:
    f.write(str(fft_out_sym[idx]).replace("-", "-\n"))

coeff_fft = get_poly_coeff(fft_out_sym[idx])
coeff_dft = get_poly_coeff(dft_out_sym[idx])

with open("out", "w") as f:
    for i, (c_fft, c_dft) in enumerate(zip(coeff_fft, coeff_dft)):
        if c_fft < 0:
            f.write("negative coeff %d\n" % c_fft)
            c_fft = c_fft + Q
        f.write("%d: %d, %d\n" % (i, c_fft, c_dft))
