def indices4_lo():
    idxs = []
    for i in range(32):
        if i < 8:
            bit6 = 0
            off = i
        elif i < 16:
            bit6 = 1
            off = i % 8
        elif i < 24:
            bit6 = 0
            off = i % 16 + 16
        else:
            bit6 = 1
            off = i % 24 + 16

        idx = (bit6 << 5) | off
        print(idx, format(idx, "06b"))
        idxs.append(idx)

    return idxs


def indices4_hi():
    idxs = []
    for i in range(32):
        if i < 8:
            bit6 = 0
            off = i + 8
        elif i < 16:
            bit6 = 1
            off = (i % 8) + 8
        elif i < 24:
            bit6 = 0
            off = (i % 16) + 24
        else:
            bit6 = 1
            off = (i % 24) + 24

        idx = (bit6 << 5) | off
        print(idx, format(idx, "06b"))
        idxs.append(idx)

    return idxs


def indices5_lo():
    idxs = []
    for i in range(32):
        if i < 16:
            bit6 = 0
        else:
            bit6 = 1

        idx = (bit6 << 5) | (i % 16)
        # print(idx, format(idx, "06b"))
        idxs.append(idx)

    return idxs


def indices5_hi():
    idxs = []
    for i in range(32):
        if i < 16:
            bit6 = 0
        else:
            bit6 = 1

        idx = (bit6 << 5) | ((i % 16) + 16)
        # print(idx, format(idx, "06b"))
        idxs.append(idx)
    return idxs



idxs = indices4_lo()
print(list(reversed(idxs)))

idxs = indices4_hi()
print(list(reversed(idxs)))
