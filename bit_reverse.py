import numpy as np


def reverse_bit(i, num_digits):
    bits = "{0:b}".format(i)
    bit_len = len(bits)
    bits = '0' * (num_digits - bit_len) + bits
    return int(bits[::-1], 2)


def bit_reverse_table(N):
    table = np.zeros((N), dtype=np.int)
    num_digits = int(np.log2(N))
    for i in range(N):
        table[reverse_bit(i, num_digits)] = i
    return table


table = bit_reverse_table(8)
print("[| " + "; ".join([str(i) for i in table]) + " |]")
