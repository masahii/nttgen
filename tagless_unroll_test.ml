open Tagless_unroll
open Tagless_types
open Tagless_vectorize


module Vector_add(Lang: Array_lang)(D: Domain with type 'a expr = 'a Lang.expr) = struct

  module V_lang = Vectorize_lang_emu(Lang)
  module V_domain = Vectorize_domain_emu(Lang)(D)

  let vector_add n =
    let open Lang in
    let open Sugar(Lang) in
    let arg_ty = CArr D.domain_c_type in
    func2 "vector_add" arg_ty arg_ty (fun arr1 arr2 ->
        let_ (arr_init n (fun _ -> D.lift D.one)) (fun res ->
            let module V = Vectorize(V_lang)(V_domain) in
            let open Unroll(V)(struct let factor = 8 end) in
            let open V in
            seq
              (for_unroll zero (int_ n) (int_ 1) (fun i ->
                   arr_set res i (D.add (arr_get arr1 i) (arr_get arr2 i))))
              (return_ res)))
end

let get_prim_root n q =
  assert (n = 1024 && q = 12289);
  49

open Tagless_fft

let get_vector_add (module Param: Ntt_param) =
  let open Tagless_impl_staged in
  let module C_Int_mod = IntegerModulo(C_modulo)(Param)  in
  let module C_vadd = Vector_add(C_Array)(C_Int_mod) in
  let fn_code = C_vadd.vector_add Param.n in
  Codelib.print_code Format.std_formatter fn_code; print_newline ();
  (* Codelib.print_code_as_ast (Codelib.close_code fn_code); *)
  Runnative.run fn_code

let test_vector_add =
  let size = 1024 in
  let q = 12289 in
  let omega = get_prim_root size q in
  let module Param = struct let q = q let qinv = 12287 let omega = omega let n = size end in
  let fn = get_vector_add (module Param) in
  let arr1 = Array.init size (fun i -> i) in
  let arr2 = Array.init size (fun i -> i) in
  let res = fn arr1 arr2 in
  ignore(res)
  (* Array.iter (fun i -> Printf.printf "tagless %d\n" i) res *)
