#include <stdint.h>
#include <stdio.h>

#define OMEGA 49
#define N 1024
#define Q 12289

uint16_t mul(uint16_t x, uint16_t y) {
  uint32_t prod = (uint32_t)x * (uint32_t)y;
  return prod % Q;
}

uint16_t add(uint16_t x, uint16_t y) {
  uint32_t sum = (uint32_t)x + (uint32_t)y;
  return sum % Q;
}

uint16_t primitive_root_power(int j) {
  uint16_t ret = 1;
  for (int i = 0; i < j; ++i) {
    ret = mul(ret, OMEGA);
  }
  return ret;
}

void fft(uint16_t *arg0) {
  uint16_t ret[N];
  for (int k = 0; k < N; ++k) {
    uint16_t power_k = primitive_root_power(k);
    uint16_t powers_kj = 1;
    uint16_t dot = arg0[0];
    for (int j = 1; j < N; ++j) {
      powers_kj = mul(powers_kj,  power_k);
      dot = add(dot, mul(powers_kj, arg0[j]));
    }
    ret[k] = dot;
  }
  for (int k = 0; k < N; ++k) {
    arg0[k] = ret[k];
  }
}
