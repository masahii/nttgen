open Tagless_types

module Unroll(Base_lang: C_lang)(F: sig val factor: int end) = struct
  include Base_lang

  let factor = F.factor
  let for_step = Base_lang.for_step * F.factor

  let for_unroll lo hi step body =
    if factor = 1 then
      Base_lang.for_ lo hi step body
    else
      let num_loop = (hi %- lo) %/ (int_ factor) in
      let rec do_unroll loop_index counter =
        let one_stmt = (body (lo %+ (loop_index %* (int_ factor) %+ (int_ (counter * Base_lang.for_step))))) in
        if counter = factor - 1 then one_stmt
        else
          seq
            one_stmt
            (do_unroll loop_index (counter + 1)) in
      Base_lang.for_ (int_ 0) num_loop (int_ 1) (fun i -> do_unroll i 0)

end
