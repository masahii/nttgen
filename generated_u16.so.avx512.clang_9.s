	.text
	.file	"generated_u16.so.avx512.c"
	.globl	csub                    # -- Begin function csub
	.p2align	4, 0x90
	.type	csub,@function
csub:                                   # @csub
	.cfi_startproc
# %bb.0:
                                        # kill: def $edi killed $edi def $rdi
	leal	-12289(%rdi), %eax
	cwtl
	shrl	$15, %eax
	andl	$12289, %eax            # imm = 0x3001
	leal	(%rax,%rdi), %eax
	addl	$-12289, %eax           # imm = 0xCFFF
                                        # kill: def $ax killed $ax killed $eax
	retq
.Lfunc_end0:
	.size	csub, .Lfunc_end0-csub
	.cfi_endproc
                                        # -- End function
	.globl	barret_reduce           # -- Begin function barret_reduce
	.p2align	4, 0x90
	.type	barret_reduce,@function
barret_reduce:                          # @barret_reduce
	.cfi_startproc
# %bb.0:
                                        # kill: def $edi killed $edi def $rdi
	leal	(%rdi,%rdi,4), %eax
	shrl	$16, %eax
	imull	$-12289, %eax, %eax     # imm = 0xCFFF
	leal	(%rdi,%rax), %ecx
	addl	$-12289, %ecx           # imm = 0xCFFF
                                        # kill: def $edi killed $edi killed $rdi def $rdi
	addl	%eax, %edi
	movswl	%cx, %eax
	shrl	$15, %eax
	andl	$12289, %eax            # imm = 0x3001
	leal	(%rax,%rdi), %eax
	addl	$-12289, %eax           # imm = 0xCFFF
                                        # kill: def $ax killed $ax killed $eax
	retq
.Lfunc_end1:
	.size	barret_reduce, .Lfunc_end1-barret_reduce
	.cfi_endproc
                                        # -- End function
	.globl	montgomery_reduce       # -- Begin function montgomery_reduce
	.p2align	4, 0x90
	.type	montgomery_reduce,@function
montgomery_reduce:                      # @montgomery_reduce
	.cfi_startproc
# %bb.0:
	imull	$12287, %edi, %eax      # imm = 0x2FFF
	movzwl	%ax, %eax
	imull	$12289, %eax, %eax      # imm = 0x3001
	addl	%edi, %eax
	shrl	$16, %eax
	leal	-12289(%rax), %ecx
	movswl	%cx, %ecx
	shrl	$15, %ecx
	andl	$12289, %ecx            # imm = 0x3001
	leal	(%rcx,%rax), %eax
	addl	$-12289, %eax           # imm = 0xCFFF
                                        # kill: def $ax killed $ax killed $eax
	retq
.Lfunc_end2:
	.size	montgomery_reduce, .Lfunc_end2-montgomery_reduce
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function csub_vec
.LCPI3_0:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI3_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.text
	.globl	csub_vec
	.p2align	4, 0x90
	.type	csub_vec,@function
csub_vec:                               # @csub_vec
	.cfi_startproc
# %bb.0:
	vpaddw	.LCPI3_0(%rip), %zmm0, %zmm0
	vpsraw	$15, %zmm0, %zmm1
	vpandq	.LCPI3_1(%rip), %zmm1, %zmm1
	vpaddw	%zmm0, %zmm1, %zmm0
	retq
.Lfunc_end3:
	.size	csub_vec, .Lfunc_end3-csub_vec
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function barret_reduce_vec
.LCPI4_0:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI4_1:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.text
	.globl	barret_reduce_vec
	.p2align	4, 0x90
	.type	barret_reduce_vec,@function
barret_reduce_vec:                      # @barret_reduce_vec
	.cfi_startproc
# %bb.0:
	vpmulhuw	.LCPI4_0(%rip), %zmm0, %zmm1
	vpmullw	.LCPI4_1(%rip), %zmm1, %zmm1
	vpaddw	%zmm0, %zmm1, %zmm0
	retq
.Lfunc_end4:
	.size	barret_reduce_vec, .Lfunc_end4-barret_reduce_vec
	.cfi_endproc
                                        # -- End function
	.globl	bit_reverse             # -- Begin function bit_reverse
	.p2align	4, 0x90
	.type	bit_reverse,@function
bit_reverse:                            # @bit_reverse
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	movslq	v_10(,%rax,4), %rcx
	cmpq	%rcx, %rax
	jge	.LBB5_3
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_5:
	addq	$1, %rcx
	movq	%rcx, %rax
	cmpq	$1024, %rcx             # imm = 0x400
	je	.LBB5_6
# %bb.1:
	movslq	v_10(,%rax,4), %rcx
	cmpq	%rcx, %rax
	jl	.LBB5_2
.LBB5_3:
	movslq	v_10+4(,%rax,4), %rdx
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jge	.LBB5_5
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_2:
	movzwl	(%rdi,%rax,2), %edx
	movzwl	(%rdi,%rcx,2), %esi
	movw	%si, (%rdi,%rax,2)
	movw	%dx, (%rdi,%rcx,2)
	movslq	v_10+4(,%rax,4), %rdx
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jge	.LBB5_5
.LBB5_4:
	movzwl	2(%rdi,%rax,2), %r8d
	movzwl	(%rdi,%rdx,2), %esi
	movw	%si, 2(%rdi,%rax,2)
	movw	%r8w, (%rdi,%rdx,2)
	jmp	.LBB5_5
.LBB5_6:
	retq
.Lfunc_end5:
	.size	bit_reverse, .Lfunc_end5-bit_reverse
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function vmul
.LCPI6_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI6_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI6_2:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI6_3:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.text
	.globl	vmul
	.p2align	4, 0x90
	.type	vmul,@function
vmul:                                   # @vmul
	.cfi_startproc
# %bb.0:
	vpmullw	%zmm0, %zmm1, %zmm2
	vpmulhuw	%zmm1, %zmm0, %zmm0
	vpmullw	.LCPI6_0(%rip), %zmm2, %zmm1
	vmovdqa64	.LCPI6_1(%rip), %zmm3 # zmm3 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vpmulhuw	%zmm3, %zmm1, %zmm1
	vptestnmw	%zmm2, %zmm2, %k1
	vmovdqa64	.LCPI6_2(%rip), %zmm2 # zmm2 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqu16	.LCPI6_3(%rip), %zmm2 {%k1}
	vpaddw	%zmm0, %zmm2, %zmm0
	vpaddw	%zmm1, %zmm0, %zmm0
	vpsraw	$15, %zmm0, %zmm1
	vpandq	%zmm3, %zmm1, %zmm1
	vpaddw	%zmm0, %zmm1, %zmm0
	retq
.Lfunc_end6:
	.size	vmul, .Lfunc_end6-vmul
	.cfi_endproc
                                        # -- End function
	.globl	vadd                    # -- Begin function vadd
	.p2align	4, 0x90
	.type	vadd,@function
vadd:                                   # @vadd
	.cfi_startproc
# %bb.0:
	vpaddw	%zmm0, %zmm1, %zmm0
	retq
.Lfunc_end7:
	.size	vadd, .Lfunc_end7-vadd
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function vsub
.LCPI8_0:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI8_1:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI8_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.text
	.globl	vsub
	.p2align	4, 0x90
	.type	vsub,@function
vsub:                                   # @vsub
	.cfi_startproc
# %bb.0:
	vpsubw	%zmm1, %zmm0, %zmm0
	vpaddw	.LCPI8_0(%rip), %zmm0, %zmm0
	vpmulhuw	.LCPI8_1(%rip), %zmm0, %zmm1
	vpmullw	.LCPI8_2(%rip), %zmm1, %zmm1
	vpaddw	%zmm0, %zmm1, %zmm0
	retq
.Lfunc_end8:
	.size	vsub, .Lfunc_end8-vsub
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft1
.LCPI9_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI9_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI9_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI9_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI9_4:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI9_5:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.text
	.globl	fft1
	.p2align	4, 0x90
	.type	fft1,@function
fft1:                                   # @fft1
	.cfi_startproc
# %bb.0:
	vmovdqa64	v_9(%rip), %zmm0
	xorl	%eax, %eax
	movl	$-1431655766, %ecx      # imm = 0xAAAAAAAA
	vmovdqa64	.LCPI9_0(%rip), %zmm1 # zmm1 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI9_1(%rip), %zmm2 # zmm2 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI9_2(%rip), %zmm3 # zmm3 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI9_3(%rip), %zmm4 # zmm4 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI9_4(%rip), %zmm5 # zmm5 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	vmovdqa64	.LCPI9_5(%rip), %zmm6 # zmm6 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rdi,%rax,2), %zmm7
	vmovdqu64	64(%rdi,%rax,2), %zmm8
	vpslld	$16, %zmm8, %zmm9
	kmovd	%ecx, %k1
	vpsrld	$16, %zmm7, %zmm10
	vmovdqu16	%zmm9, %zmm7 {%k1}
	vmovdqu16	%zmm8, %zmm10 {%k1}
	vpmullw	%zmm0, %zmm10, %zmm8
	vpmulhuw	%zmm0, %zmm10, %zmm9
	vpmullw	%zmm1, %zmm8, %zmm10
	vptestnmw	%zmm8, %zmm8, %k2
	vpblendmw	%zmm3, %zmm4, %zmm8 {%k2}
	vpmulhuw	%zmm2, %zmm10, %zmm10
	vpaddw	%zmm8, %zmm9, %zmm8
	vpaddw	%zmm8, %zmm10, %zmm8
	vpsraw	$15, %zmm8, %zmm9
	vpandq	%zmm2, %zmm9, %zmm9
	vpaddw	%zmm8, %zmm9, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm9
	vpsubw	%zmm8, %zmm7, %zmm7
	vpaddw	%zmm5, %zmm7, %zmm7
	vpmulhuw	%zmm6, %zmm7, %zmm8
	vpmullw	%zmm3, %zmm8, %zmm8
	vpsrld	$16, %zmm9, %zmm10
	vpaddw	%zmm8, %zmm7, %zmm10 {%k1}
	vpaddw	%zmm8, %zmm7, %zmm7
	vpslld	$16, %zmm7, %zmm7
	vmovdqu16	%zmm7, %zmm9 {%k1}
	vmovdqu64	%zmm9, (%rdi,%rax,2)
	vmovdqu64	%zmm10, 64(%rdi,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB9_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end9:
	.size	fft1, .Lfunc_end9-fft1
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft2
.LCPI10_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI10_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI10_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI10_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI10_4:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI10_5:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.text
	.globl	fft2
	.p2align	4, 0x90
	.type	fft2,@function
fft2:                                   # @fft2
	.cfi_startproc
# %bb.0:
	vmovdqa64	v_9+64(%rip), %zmm0
	xorl	%eax, %eax
	movw	$-21846, %cx            # imm = 0xAAAA
	vmovdqa64	.LCPI10_0(%rip), %zmm1 # zmm1 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI10_1(%rip), %zmm2 # zmm2 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI10_2(%rip), %zmm3 # zmm3 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI10_3(%rip), %zmm4 # zmm4 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI10_4(%rip), %zmm5 # zmm5 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	vmovdqa64	.LCPI10_5(%rip), %zmm6 # zmm6 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rdi,%rax,2), %zmm7
	vmovdqu64	64(%rdi,%rax,2), %zmm8
	vpsllq	$32, %zmm8, %zmm9
	kmovd	%ecx, %k1
	vpsrlq	$32, %zmm7, %zmm10
	vmovdqa32	%zmm9, %zmm7 {%k1}
	vmovdqa32	%zmm8, %zmm10 {%k1}
	vpmullw	%zmm10, %zmm0, %zmm8
	vpmulhuw	%zmm0, %zmm10, %zmm9
	vpmullw	%zmm1, %zmm8, %zmm10
	vpmulhuw	%zmm2, %zmm10, %zmm10
	vptestnmw	%zmm8, %zmm8, %k2
	vpblendmw	%zmm3, %zmm4, %zmm8 {%k2}
	vpaddw	%zmm8, %zmm9, %zmm8
	vpaddw	%zmm8, %zmm10, %zmm8
	vpsraw	$15, %zmm8, %zmm9
	vpandq	%zmm2, %zmm9, %zmm9
	vpaddw	%zmm8, %zmm9, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm5, %zmm9, %zmm10
	vpmullw	%zmm3, %zmm10, %zmm10
	vpaddw	%zmm10, %zmm9, %zmm9
	vpsubw	%zmm8, %zmm7, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm7
	vpmulhuw	%zmm5, %zmm7, %zmm8
	vpmullw	%zmm3, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vpsllq	$32, %zmm7, %zmm8
	vpsrlq	$32, %zmm9, %zmm10
	vmovdqa32	%zmm8, %zmm9 {%k1}
	vmovdqa32	%zmm7, %zmm10 {%k1}
	vmovdqu64	%zmm9, (%rdi,%rax,2)
	vmovdqu64	%zmm10, 64(%rdi,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB10_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end10:
	.size	fft2, .Lfunc_end10-fft2
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft3
.LCPI11_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI11_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI11_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI11_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI11_4:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI11_5:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.text
	.globl	fft3
	.p2align	4, 0x90
	.type	fft3,@function
fft3:                                   # @fft3
	.cfi_startproc
# %bb.0:
	vmovdqa64	v_9+128(%rip), %zmm0
	xorl	%eax, %eax
	vmovdqa64	.LCPI11_0(%rip), %zmm1 # zmm1 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI11_1(%rip), %zmm2 # zmm2 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI11_2(%rip), %zmm3 # zmm3 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI11_3(%rip), %zmm4 # zmm4 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI11_4(%rip), %zmm5 # zmm5 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	vmovdqa64	.LCPI11_5(%rip), %zmm6 # zmm6 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rdi,%rax,2), %zmm7
	vmovdqu64	64(%rdi,%rax,2), %zmm8
	vpunpcklqdq	%zmm8, %zmm7, %zmm9 # zmm9 = zmm7[0],zmm8[0],zmm7[2],zmm8[2],zmm7[4],zmm8[4],zmm7[6],zmm8[6]
	vpunpckhqdq	%zmm8, %zmm7, %zmm7 # zmm7 = zmm7[1],zmm8[1],zmm7[3],zmm8[3],zmm7[5],zmm8[5],zmm7[7],zmm8[7]
	vpmullw	%zmm7, %zmm0, %zmm8
	vpmulhuw	%zmm0, %zmm7, %zmm7
	vpmullw	%zmm1, %zmm8, %zmm10
	vpmulhuw	%zmm2, %zmm10, %zmm10
	vptestnmw	%zmm8, %zmm8, %k1
	vpblendmw	%zmm3, %zmm4, %zmm8 {%k1}
	vpaddw	%zmm8, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm10, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm9, %zmm7, %zmm8
	vpsubw	%zmm7, %zmm9, %zmm7
	vpaddw	%zmm5, %zmm7, %zmm7
	vpmulhuw	%zmm6, %zmm7, %zmm9
	vpmullw	%zmm3, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm7, %zmm7
	vpunpcklqdq	%zmm7, %zmm8, %zmm9 # zmm9 = zmm8[0],zmm7[0],zmm8[2],zmm7[2],zmm8[4],zmm7[4],zmm8[6],zmm7[6]
	vpunpckhqdq	%zmm7, %zmm8, %zmm7 # zmm7 = zmm8[1],zmm7[1],zmm8[3],zmm7[3],zmm8[5],zmm7[5],zmm8[7],zmm7[7]
	vmovdqu64	%zmm9, (%rdi,%rax,2)
	vmovdqu64	%zmm7, 64(%rdi,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB11_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end11:
	.size	fft3, .Lfunc_end11-fft3
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft4
.LCPI12_0:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	50                      # 0x32
	.short	51                      # 0x33
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	55                      # 0x37
.LCPI12_1:
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	62                      # 0x3e
	.short	63                      # 0x3f
.LCPI12_2:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI12_3:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI12_4:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI12_5:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI12_6:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI12_7:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.text
	.globl	fft4
	.p2align	4, 0x90
	.type	fft4,@function
fft4:                                   # @fft4
	.cfi_startproc
# %bb.0:
	vmovdqa64	v_9+192(%rip), %zmm0
	xorl	%eax, %eax
	vmovdqa64	.LCPI12_0(%rip), %zmm1 # zmm1 = [0,1,2,3,4,5,6,7,32,33,34,35,36,37,38,39,16,17,18,19,20,21,22,23,48,49,50,51,52,53,54,55]
	vmovdqa64	.LCPI12_1(%rip), %zmm2 # zmm2 = [8,9,10,11,12,13,14,15,40,41,42,43,44,45,46,47,24,25,26,27,28,29,30,31,56,57,58,59,60,61,62,63]
	vmovdqa64	.LCPI12_2(%rip), %zmm3 # zmm3 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI12_3(%rip), %zmm4 # zmm4 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI12_4(%rip), %zmm5 # zmm5 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI12_5(%rip), %zmm6 # zmm6 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI12_6(%rip), %zmm7 # zmm7 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	vmovdqa64	.LCPI12_7(%rip), %zmm8 # zmm8 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rdi,%rax,2), %zmm9
	vmovdqu64	64(%rdi,%rax,2), %zmm10
	vmovdqa64	%zmm9, %zmm11
	vpermt2w	%zmm10, %zmm1, %zmm11
	vpermt2w	%zmm10, %zmm2, %zmm9
	vpmullw	%zmm0, %zmm9, %zmm10
	vpmulhuw	%zmm0, %zmm9, %zmm9
	vpmullw	%zmm3, %zmm10, %zmm12
	vpmulhuw	%zmm4, %zmm12, %zmm12
	vptestnmw	%zmm10, %zmm10, %k1
	vpblendmw	%zmm5, %zmm6, %zmm10 {%k1}
	vpaddw	%zmm10, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm12, %zmm9
	vpsraw	$15, %zmm9, %zmm10
	vpandq	%zmm4, %zmm10, %zmm10
	vpaddw	%zmm9, %zmm10, %zmm9
	vpaddw	%zmm11, %zmm9, %zmm10
	vpmulhuw	%zmm7, %zmm10, %zmm12
	vpmullw	%zmm5, %zmm12, %zmm12
	vpaddw	%zmm12, %zmm10, %zmm10
	vpsubw	%zmm9, %zmm11, %zmm9
	vpaddw	%zmm8, %zmm9, %zmm9
	vpmulhuw	%zmm7, %zmm9, %zmm11
	vpmullw	%zmm5, %zmm11, %zmm11
	vpaddw	%zmm11, %zmm9, %zmm9
	vmovdqa64	%zmm10, %zmm11
	vpermt2w	%zmm9, %zmm1, %zmm11
	vpermt2w	%zmm9, %zmm2, %zmm10
	vmovdqu64	%zmm11, (%rdi,%rax,2)
	vmovdqu64	%zmm10, 64(%rdi,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB12_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end12:
	.size	fft4, .Lfunc_end12-fft4
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft5
.LCPI13_0:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
.LCPI13_1:
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	50                      # 0x32
	.short	51                      # 0x33
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	55                      # 0x37
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	62                      # 0x3e
	.short	63                      # 0x3f
.LCPI13_2:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI13_3:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI13_4:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI13_5:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI13_6:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI13_7:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.text
	.globl	fft5
	.p2align	4, 0x90
	.type	fft5,@function
fft5:                                   # @fft5
	.cfi_startproc
# %bb.0:
	vmovdqa64	v_9+256(%rip), %zmm0
	xorl	%eax, %eax
	vmovdqa64	.LCPI13_0(%rip), %zmm1 # zmm1 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]
	vmovdqa64	.LCPI13_1(%rip), %zmm2 # zmm2 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63]
	vmovdqa64	.LCPI13_2(%rip), %zmm3 # zmm3 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI13_3(%rip), %zmm4 # zmm4 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI13_4(%rip), %zmm5 # zmm5 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI13_5(%rip), %zmm6 # zmm6 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI13_6(%rip), %zmm7 # zmm7 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	vmovdqa64	.LCPI13_7(%rip), %zmm8 # zmm8 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rdi,%rax,2), %zmm9
	vmovdqu64	64(%rdi,%rax,2), %zmm10
	vmovdqa64	%zmm9, %zmm11
	vpermt2w	%zmm10, %zmm1, %zmm11
	vpermt2w	%zmm10, %zmm2, %zmm9
	vpmullw	%zmm0, %zmm9, %zmm10
	vpmulhuw	%zmm0, %zmm9, %zmm9
	vpmullw	%zmm3, %zmm10, %zmm12
	vpmulhuw	%zmm4, %zmm12, %zmm12
	vptestnmw	%zmm10, %zmm10, %k1
	vpblendmw	%zmm5, %zmm6, %zmm10 {%k1}
	vpaddw	%zmm10, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm12, %zmm9
	vpsraw	$15, %zmm9, %zmm10
	vpandq	%zmm4, %zmm10, %zmm10
	vpaddw	%zmm9, %zmm10, %zmm9
	vpaddw	%zmm11, %zmm9, %zmm10
	vpsubw	%zmm9, %zmm11, %zmm9
	vpaddw	%zmm7, %zmm9, %zmm9
	vpmulhuw	%zmm8, %zmm9, %zmm11
	vpmullw	%zmm5, %zmm11, %zmm11
	vpaddw	%zmm11, %zmm9, %zmm9
	vmovdqa64	%zmm10, %zmm11
	vpermt2w	%zmm9, %zmm1, %zmm11
	vpermt2w	%zmm9, %zmm2, %zmm10
	vmovdqu64	%zmm11, (%rdi,%rax,2)
	vmovdqu64	%zmm10, 64(%rdi,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB13_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end13:
	.size	fft5, .Lfunc_end13-fft5
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft6
.LCPI14_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI14_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI14_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI14_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI14_4:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI14_5:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.text
	.globl	fft6
	.p2align	4, 0x90
	.type	fft6,@function
fft6:                                   # @fft6
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	vmovdqa64	.LCPI14_0(%rip), %zmm0 # zmm0 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI14_1(%rip), %zmm1 # zmm1 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI14_2(%rip), %zmm2 # zmm2 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI14_3(%rip), %zmm3 # zmm3 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI14_4(%rip), %zmm4 # zmm4 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	vmovdqa64	.LCPI14_5(%rip), %zmm5 # zmm5 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB14_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rdi,%rax,2), %zmm6
	vmovdqu64	64(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+320(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vpmulhuw	%zmm4, %zmm8, %zmm9
	vpmullw	%zmm2, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm8, %zmm8
	vmovdqu64	%zmm8, (%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm5, %zmm6, %zmm6
	vpmulhuw	%zmm4, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 64(%rdi,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB14_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end14:
	.size	fft6, .Lfunc_end14-fft6
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft7
.LCPI15_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI15_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI15_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI15_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI15_4:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI15_5:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.text
	.globl	fft7
	.p2align	4, 0x90
	.type	fft7,@function
fft7:                                   # @fft7
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	vmovdqa64	.LCPI15_0(%rip), %zmm0 # zmm0 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI15_1(%rip), %zmm1 # zmm1 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI15_2(%rip), %zmm2 # zmm2 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI15_3(%rip), %zmm3 # zmm3 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI15_4(%rip), %zmm4 # zmm4 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	vmovdqa64	.LCPI15_5(%rip), %zmm5 # zmm5 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqa64	v_9+384(%rip), %zmm6
	vmovdqu64	(%rdi,%rax,2), %zmm7
	vmovdqu64	64(%rdi,%rax,2), %zmm8
	vmovdqu64	128(%rdi,%rax,2), %zmm9
	vmovdqu64	192(%rdi,%rax,2), %zmm10
	vpmullw	%zmm9, %zmm6, %zmm11
	vpmulhuw	%zmm6, %zmm9, %zmm6
	vpmullw	%zmm0, %zmm11, %zmm9
	vpmulhuw	%zmm1, %zmm9, %zmm9
	vptestnmw	%zmm11, %zmm11, %k1
	vpblendmw	%zmm2, %zmm3, %zmm11 {%k1}
	vpaddw	%zmm11, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm9, %zmm6
	vpsraw	$15, %zmm6, %zmm9
	vpandq	%zmm1, %zmm9, %zmm9
	vpaddw	%zmm6, %zmm9, %zmm6
	vpaddw	%zmm7, %zmm6, %zmm9
	vmovdqu64	%zmm9, (%rdi,%rax,2)
	vpsubw	%zmm6, %zmm7, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 128(%rdi,%rax,2)
	vmovdqa64	v_9+448(%rip), %zmm6
	vpmullw	%zmm10, %zmm6, %zmm7
	vpmulhuw	%zmm6, %zmm10, %zmm6
	vpmullw	%zmm0, %zmm7, %zmm9
	vptestnmw	%zmm7, %zmm7, %k1
	vpblendmw	%zmm2, %zmm3, %zmm7 {%k1}
	vpmulhuw	%zmm1, %zmm9, %zmm9
	vpaddw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm9, %zmm6
	vpsraw	$15, %zmm6, %zmm7
	vpandq	%zmm1, %zmm7, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm6
	vpaddw	%zmm8, %zmm6, %zmm7
	vmovdqu64	%zmm7, 64(%rdi,%rax,2)
	vpsubw	%zmm6, %zmm8, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 192(%rdi,%rax,2)
	subq	$-128, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB15_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end15:
	.size	fft7, .Lfunc_end15-fft7
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft8
.LCPI16_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI16_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI16_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI16_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI16_4:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI16_5:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.text
	.globl	fft8
	.p2align	4, 0x90
	.type	fft8,@function
fft8:                                   # @fft8
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	vmovdqa64	.LCPI16_0(%rip), %zmm0 # zmm0 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI16_1(%rip), %zmm1 # zmm1 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI16_2(%rip), %zmm2 # zmm2 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI16_3(%rip), %zmm3 # zmm3 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI16_4(%rip), %zmm4 # zmm4 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	vmovdqa64	.LCPI16_5(%rip), %zmm5 # zmm5 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	256(%rdi,%rax,2), %zmm6
	vmovdqa64	v_9+512(%rip), %zmm7
	vpmullw	%zmm6, %zmm7, %zmm8
	vpmulhuw	%zmm7, %zmm6, %zmm6
	vpmullw	%zmm0, %zmm8, %zmm7
	vpmulhuw	%zmm1, %zmm7, %zmm7
	vptestnmw	%zmm8, %zmm8, %k1
	vpblendmw	%zmm2, %zmm3, %zmm8 {%k1}
	vpaddw	%zmm8, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm7, %zmm6
	vpsraw	$15, %zmm6, %zmm7
	vpandq	%zmm1, %zmm7, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm7
	vmovdqu64	(%rdi,%rax,2), %zmm8
	vmovdqu64	64(%rdi,%rax,2), %zmm9
	vmovdqu64	128(%rdi,%rax,2), %zmm10
	vmovdqu64	192(%rdi,%rax,2), %zmm6
	vpaddw	%zmm8, %zmm7, %zmm11
	vpmulhuw	%zmm4, %zmm11, %zmm12
	vpmullw	%zmm2, %zmm12, %zmm12
	vpaddw	%zmm12, %zmm11, %zmm11
	vmovdqu64	%zmm11, (%rdi,%rax,2)
	vpsubw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm5, %zmm7, %zmm7
	vpmulhuw	%zmm4, %zmm7, %zmm8
	vpmullw	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, 256(%rdi,%rax,2)
	vmovdqu64	320(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+576(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm11
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm11, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm11, %zmm11, %k1
	vpblendmw	%zmm2, %zmm3, %zmm11 {%k1}
	vpaddw	%zmm11, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm9, %zmm7, %zmm8
	vpmulhuw	%zmm4, %zmm8, %zmm11
	vpmullw	%zmm2, %zmm11, %zmm11
	vpaddw	%zmm11, %zmm8, %zmm8
	vmovdqu64	%zmm8, 64(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm9, %zmm7
	vpaddw	%zmm5, %zmm7, %zmm7
	vpmulhuw	%zmm4, %zmm7, %zmm8
	vpmullw	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, 320(%rdi,%rax,2)
	vmovdqu64	384(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+640(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm10, %zmm7, %zmm8
	vpmulhuw	%zmm4, %zmm8, %zmm9
	vpmullw	%zmm2, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm8, %zmm8
	vmovdqu64	%zmm8, 128(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm10, %zmm7
	vpaddw	%zmm5, %zmm7, %zmm7
	vpmulhuw	%zmm4, %zmm7, %zmm8
	vpmullw	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, 384(%rdi,%rax,2)
	vmovdqu64	448(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+704(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vpmulhuw	%zmm4, %zmm8, %zmm9
	vpmullw	%zmm2, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm8, %zmm8
	vmovdqu64	%zmm8, 192(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm5, %zmm6, %zmm6
	vpmulhuw	%zmm4, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 448(%rdi,%rax,2)
	addq	$256, %rax              # imm = 0x100
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB16_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end16:
	.size	fft8, .Lfunc_end16-fft8
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft9
.LCPI17_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI17_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI17_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI17_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI17_4:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI17_5:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.text
	.globl	fft9
	.p2align	4, 0x90
	.type	fft9,@function
fft9:                                   # @fft9
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	vmovdqa64	.LCPI17_0(%rip), %zmm0 # zmm0 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI17_1(%rip), %zmm1 # zmm1 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI17_2(%rip), %zmm2 # zmm2 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI17_3(%rip), %zmm3 # zmm3 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI17_4(%rip), %zmm4 # zmm4 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	vmovdqa64	.LCPI17_5(%rip), %zmm5 # zmm5 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB17_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	512(%rdi,%rax,2), %zmm6
	vmovdqa64	v_9+768(%rip), %zmm7
	vpmullw	%zmm6, %zmm7, %zmm8
	vpmulhuw	%zmm7, %zmm6, %zmm6
	vpmullw	%zmm0, %zmm8, %zmm7
	vpmulhuw	%zmm1, %zmm7, %zmm7
	vptestnmw	%zmm8, %zmm8, %k1
	vpblendmw	%zmm2, %zmm3, %zmm8 {%k1}
	vpaddw	%zmm8, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm7, %zmm6
	vpsraw	$15, %zmm6, %zmm7
	vpandq	%zmm1, %zmm7, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm7
	vmovdqu64	(%rdi,%rax,2), %zmm8
	vmovdqu64	64(%rdi,%rax,2), %zmm9
	vmovdqu64	128(%rdi,%rax,2), %zmm10
	vmovdqu64	192(%rdi,%rax,2), %zmm6
	vpaddw	%zmm8, %zmm7, %zmm11
	vmovdqu64	%zmm11, (%rdi,%rax,2)
	vpsubw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm4, %zmm7, %zmm7
	vpmulhuw	%zmm5, %zmm7, %zmm8
	vpmullw	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, 512(%rdi,%rax,2)
	vmovdqu64	576(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+832(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm11
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm11, %zmm8
	vptestnmw	%zmm11, %zmm11, %k1
	vpblendmw	%zmm2, %zmm3, %zmm11 {%k1}
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm11, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm9, %zmm7, %zmm8
	vmovdqu64	%zmm8, 64(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm9, %zmm7
	vpaddw	%zmm4, %zmm7, %zmm7
	vpmulhuw	%zmm5, %zmm7, %zmm8
	vpmullw	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, 576(%rdi,%rax,2)
	vmovdqu64	640(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+896(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm10, %zmm7, %zmm8
	vmovdqu64	%zmm8, 128(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm10, %zmm7
	vpaddw	%zmm4, %zmm7, %zmm7
	vpmulhuw	%zmm5, %zmm7, %zmm8
	vpmullw	%zmm2, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, 640(%rdi,%rax,2)
	vmovdqu64	704(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+960(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vmovdqu64	%zmm8, 192(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 704(%rdi,%rax,2)
	vmovdqu64	256(%rdi,%rax,2), %zmm6
	vmovdqu64	768(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+1024(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vmovdqu64	%zmm8, 256(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 768(%rdi,%rax,2)
	vmovdqu64	320(%rdi,%rax,2), %zmm6
	vmovdqu64	832(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+1088(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vmovdqu64	%zmm8, 320(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 832(%rdi,%rax,2)
	vmovdqu64	384(%rdi,%rax,2), %zmm6
	vmovdqu64	896(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+1152(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vmovdqu64	%zmm8, 384(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 896(%rdi,%rax,2)
	vmovdqu64	448(%rdi,%rax,2), %zmm6
	vmovdqu64	960(%rdi,%rax,2), %zmm7
	vmovdqa64	v_9+1216(%rip), %zmm8
	vpmullw	%zmm7, %zmm8, %zmm9
	vpmulhuw	%zmm8, %zmm7, %zmm7
	vpmullw	%zmm0, %zmm9, %zmm8
	vptestnmw	%zmm9, %zmm9, %k1
	vpblendmw	%zmm2, %zmm3, %zmm9 {%k1}
	vpmulhuw	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm9, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm8, %zmm7
	vpsraw	$15, %zmm7, %zmm8
	vpandq	%zmm1, %zmm8, %zmm8
	vpaddw	%zmm7, %zmm8, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm8
	vmovdqu64	%zmm8, 448(%rdi,%rax,2)
	vpsubw	%zmm7, %zmm6, %zmm6
	vpaddw	%zmm4, %zmm6, %zmm6
	vpmulhuw	%zmm5, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 960(%rdi,%rax,2)
	addq	$512, %rax              # imm = 0x200
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB17_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end17:
	.size	fft9, .Lfunc_end17-fft9
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft10
.LCPI18_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI18_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI18_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI18_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI18_4:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI18_5:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.text
	.globl	fft10
	.p2align	4, 0x90
	.type	fft10,@function
fft10:                                  # @fft10
	.cfi_startproc
# %bb.0:
	movq	$-1024, %rax            # imm = 0xFC00
	vmovdqa64	.LCPI18_0(%rip), %zmm0 # zmm0 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI18_1(%rip), %zmm1 # zmm1 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI18_2(%rip), %zmm2 # zmm2 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI18_3(%rip), %zmm3 # zmm3 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI18_4(%rip), %zmm4 # zmm4 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	vmovdqa64	.LCPI18_5(%rip), %zmm5 # zmm5 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB18_1:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	2048(%rdi,%rax), %zmm6
	vmovdqa64	v_9+2304(%rax), %zmm7
	vpmullw	%zmm6, %zmm7, %zmm8
	vpmulhuw	%zmm7, %zmm6, %zmm6
	vpmullw	%zmm0, %zmm8, %zmm7
	vpmulhuw	%zmm1, %zmm7, %zmm7
	vptestnmw	%zmm8, %zmm8, %k1
	vpblendmw	%zmm2, %zmm3, %zmm8 {%k1}
	vpaddw	%zmm8, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm7, %zmm6
	vpsraw	$15, %zmm6, %zmm7
	vpandq	%zmm1, %zmm7, %zmm7
	vpaddw	%zmm6, %zmm7, %zmm6
	vmovdqu64	1024(%rdi,%rax), %zmm7
	vpaddw	%zmm7, %zmm6, %zmm8
	vpmulhuw	%zmm4, %zmm8, %zmm9
	vpmullw	%zmm2, %zmm9, %zmm9
	vpaddw	%zmm9, %zmm8, %zmm8
	vmovdqu64	%zmm8, 1024(%rdi,%rax)
	vpsubw	%zmm6, %zmm7, %zmm6
	vpaddw	%zmm5, %zmm6, %zmm6
	vpmulhuw	%zmm4, %zmm6, %zmm7
	vpmullw	%zmm2, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm6, %zmm6
	vmovdqu64	%zmm6, 2048(%rdi,%rax)
	addq	$64, %rax
	jne	.LBB18_1
# %bb.2:
	vzeroupper
	retq
.Lfunc_end18:
	.size	fft10, .Lfunc_end18-fft10
	.cfi_endproc
                                        # -- End function
	.section	.rodata,"a",@progbits
	.p2align	6               # -- Begin function fft
.LCPI19_0:
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
	.short	12287                   # 0x2fff
.LCPI19_1:
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
	.short	12289                   # 0x3001
.LCPI19_2:
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
	.short	53247                   # 0xcfff
.LCPI19_3:
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
	.short	53248                   # 0xd000
.LCPI19_4:
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
	.short	24578                   # 0x6002
.LCPI19_5:
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
	.short	5                       # 0x5
.LCPI19_6:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	50                      # 0x32
	.short	51                      # 0x33
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	55                      # 0x37
.LCPI19_7:
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	62                      # 0x3e
	.short	63                      # 0x3f
.LCPI19_8:
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
.LCPI19_9:
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	50                      # 0x32
	.short	51                      # 0x33
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	55                      # 0x37
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	62                      # 0x3e
	.short	63                      # 0x3f
	.text
	.globl	fft
	.p2align	4, 0x90
	.type	fft,@function
fft:                                    # @fft
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%eax, %eax
	movslq	v_10(,%rax,4), %rcx
	cmpq	%rcx, %rax
	jge	.LBB19_3
	jmp	.LBB19_2
	.p2align	4, 0x90
.LBB19_5:
	addq	$1, %rcx
	movq	%rcx, %rax
	cmpq	$1024, %rcx             # imm = 0x400
	je	.LBB19_6
# %bb.1:
	movslq	v_10(,%rax,4), %rcx
	cmpq	%rcx, %rax
	jl	.LBB19_2
.LBB19_3:
	movslq	v_10+4(,%rax,4), %rdx
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jge	.LBB19_5
	jmp	.LBB19_4
	.p2align	4, 0x90
.LBB19_2:
	movzwl	(%rbx,%rax,2), %edx
	movzwl	(%rbx,%rcx,2), %esi
	movw	%si, (%rbx,%rax,2)
	movw	%dx, (%rbx,%rcx,2)
	movslq	v_10+4(,%rax,4), %rdx
	leaq	1(%rax), %rcx
	cmpq	%rdx, %rcx
	jge	.LBB19_5
.LBB19_4:
	movzwl	2(%rbx,%rax,2), %esi
	movzwl	(%rbx,%rdx,2), %edi
	movw	%di, 2(%rbx,%rax,2)
	movw	%si, (%rbx,%rdx,2)
	jmp	.LBB19_5
.LBB19_6:
	vmovdqa64	v_9(%rip), %zmm1
	xorl	%eax, %eax
	movl	$-1431655766, %ecx      # imm = 0xAAAAAAAA
	vmovdqa64	.LCPI19_0(%rip), %zmm8 # zmm8 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI19_1(%rip), %zmm9 # zmm9 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	vmovdqa64	.LCPI19_2(%rip), %zmm10 # zmm10 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI19_3(%rip), %zmm11 # zmm11 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI19_4(%rip), %zmm12 # zmm12 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	vmovdqa64	.LCPI19_5(%rip), %zmm0 # zmm0 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB19_7:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rbx,%rax,2), %zmm2
	vmovdqu64	64(%rbx,%rax,2), %zmm3
	vpslld	$16, %zmm3, %zmm4
	kmovd	%ecx, %k1
	vpsrld	$16, %zmm2, %zmm5
	vmovdqu16	%zmm4, %zmm2 {%k1}
	vmovdqu16	%zmm3, %zmm5 {%k1}
	vpmullw	%zmm1, %zmm5, %zmm3
	vpmulhuw	%zmm1, %zmm5, %zmm4
	vpmullw	%zmm8, %zmm3, %zmm5
	vptestnmw	%zmm3, %zmm3, %k2
	vpblendmw	%zmm10, %zmm11, %zmm3 {%k2}
	vpmulhuw	%zmm9, %zmm5, %zmm5
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm3, %zmm5, %zmm3
	vpsraw	$15, %zmm3, %zmm4
	vpandq	%zmm9, %zmm4, %zmm4
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm2, %zmm3, %zmm4
	vpsubw	%zmm3, %zmm2, %zmm2
	vpaddw	%zmm12, %zmm2, %zmm2
	vpmulhuw	%zmm0, %zmm2, %zmm3
	vpmullw	%zmm10, %zmm3, %zmm3
	vpsrld	$16, %zmm4, %zmm5
	vpaddw	%zmm3, %zmm2, %zmm5 {%k1}
	vpaddw	%zmm3, %zmm2, %zmm2
	vpslld	$16, %zmm2, %zmm2
	vmovdqu16	%zmm2, %zmm4 {%k1}
	vmovdqu64	%zmm4, (%rbx,%rax,2)
	vmovdqu64	%zmm5, 64(%rbx,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_7
# %bb.8:
	vmovdqa64	v_9+64(%rip), %zmm1
	xorl	%eax, %eax
	movw	$-21846, %cx            # imm = 0xAAAA
	vmovdqa64	.LCPI19_5(%rip), %zmm13 # zmm13 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	.p2align	4, 0x90
.LBB19_9:                               # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rbx,%rax,2), %zmm2
	vmovdqu64	64(%rbx,%rax,2), %zmm3
	vpsllq	$32, %zmm3, %zmm4
	kmovd	%ecx, %k1
	vpsrlq	$32, %zmm2, %zmm5
	vmovdqa32	%zmm4, %zmm2 {%k1}
	vmovdqa32	%zmm3, %zmm5 {%k1}
	vpmullw	%zmm5, %zmm1, %zmm3
	vpmulhuw	%zmm1, %zmm5, %zmm4
	vpmullw	%zmm8, %zmm3, %zmm5
	vpmulhuw	%zmm9, %zmm5, %zmm5
	vptestnmw	%zmm3, %zmm3, %k2
	vpblendmw	%zmm10, %zmm11, %zmm3 {%k2}
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm3, %zmm5, %zmm3
	vpsraw	$15, %zmm3, %zmm4
	vpandq	%zmm9, %zmm4, %zmm4
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm2, %zmm3, %zmm4
	vpmulhuw	%zmm13, %zmm4, %zmm5
	vpmullw	%zmm10, %zmm5, %zmm5
	vpaddw	%zmm5, %zmm4, %zmm4
	vpsubw	%zmm3, %zmm2, %zmm2
	vpaddw	%zmm12, %zmm2, %zmm2
	vpmulhuw	%zmm13, %zmm2, %zmm3
	vpmullw	%zmm10, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm2, %zmm2
	vpsllq	$32, %zmm2, %zmm3
	vpsrlq	$32, %zmm4, %zmm5
	vmovdqa32	%zmm3, %zmm4 {%k1}
	vmovdqa32	%zmm2, %zmm5 {%k1}
	vmovdqu64	%zmm4, (%rbx,%rax,2)
	vmovdqu64	%zmm5, 64(%rbx,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_9
# %bb.10:
	vmovdqa64	v_9+128(%rip), %zmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_11:                              # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rbx,%rax,2), %zmm2
	vmovdqu64	64(%rbx,%rax,2), %zmm3
	vpunpcklqdq	%zmm3, %zmm2, %zmm4 # zmm4 = zmm2[0],zmm3[0],zmm2[2],zmm3[2],zmm2[4],zmm3[4],zmm2[6],zmm3[6]
	vpunpckhqdq	%zmm3, %zmm2, %zmm2 # zmm2 = zmm2[1],zmm3[1],zmm2[3],zmm3[3],zmm2[5],zmm3[5],zmm2[7],zmm3[7]
	vpmullw	%zmm2, %zmm1, %zmm3
	vpmulhuw	%zmm1, %zmm2, %zmm2
	vpmullw	%zmm8, %zmm3, %zmm5
	vpmulhuw	%zmm9, %zmm5, %zmm5
	vptestnmw	%zmm3, %zmm3, %k1
	vpblendmw	%zmm10, %zmm11, %zmm3 {%k1}
	vpaddw	%zmm3, %zmm2, %zmm2
	vpaddw	%zmm2, %zmm5, %zmm2
	vpsraw	$15, %zmm2, %zmm3
	vpandq	%zmm9, %zmm3, %zmm3
	vpaddw	%zmm2, %zmm3, %zmm2
	vpaddw	%zmm4, %zmm2, %zmm3
	vpsubw	%zmm2, %zmm4, %zmm2
	vpaddw	%zmm12, %zmm2, %zmm2
	vpmulhuw	%zmm0, %zmm2, %zmm4
	vpmullw	%zmm10, %zmm4, %zmm4
	vpaddw	%zmm4, %zmm2, %zmm2
	vpunpcklqdq	%zmm2, %zmm3, %zmm4 # zmm4 = zmm3[0],zmm2[0],zmm3[2],zmm2[2],zmm3[4],zmm2[4],zmm3[6],zmm2[6]
	vpunpckhqdq	%zmm2, %zmm3, %zmm2 # zmm2 = zmm3[1],zmm2[1],zmm3[3],zmm2[3],zmm3[5],zmm2[5],zmm3[7],zmm2[7]
	vmovdqu64	%zmm4, (%rbx,%rax,2)
	vmovdqu64	%zmm2, 64(%rbx,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_11
# %bb.12:
	vmovdqa64	v_9+192(%rip), %zmm1
	xorl	%eax, %eax
	vmovdqa64	.LCPI19_6(%rip), %zmm2 # zmm2 = [0,1,2,3,4,5,6,7,32,33,34,35,36,37,38,39,16,17,18,19,20,21,22,23,48,49,50,51,52,53,54,55]
	vmovdqa64	.LCPI19_7(%rip), %zmm3 # zmm3 = [8,9,10,11,12,13,14,15,40,41,42,43,44,45,46,47,24,25,26,27,28,29,30,31,56,57,58,59,60,61,62,63]
	.p2align	4, 0x90
.LBB19_13:                              # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rbx,%rax,2), %zmm4
	vmovdqu64	64(%rbx,%rax,2), %zmm5
	vmovdqa64	%zmm4, %zmm6
	vpermt2w	%zmm5, %zmm2, %zmm6
	vpermt2w	%zmm5, %zmm3, %zmm4
	vpmullw	%zmm1, %zmm4, %zmm5
	vpmulhuw	%zmm1, %zmm4, %zmm4
	vpmullw	%zmm8, %zmm5, %zmm7
	vpmulhuw	%zmm9, %zmm7, %zmm7
	vptestnmw	%zmm5, %zmm5, %k1
	vpblendmw	%zmm10, %zmm11, %zmm5 {%k1}
	vpaddw	%zmm5, %zmm4, %zmm4
	vpaddw	%zmm4, %zmm7, %zmm4
	vpsraw	$15, %zmm4, %zmm5
	vpandq	%zmm9, %zmm5, %zmm5
	vpaddw	%zmm4, %zmm5, %zmm4
	vpaddw	%zmm6, %zmm4, %zmm5
	vpmulhuw	%zmm13, %zmm5, %zmm7
	vpmullw	%zmm10, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm5, %zmm5
	vpsubw	%zmm4, %zmm6, %zmm4
	vpaddw	%zmm12, %zmm4, %zmm4
	vpmulhuw	%zmm13, %zmm4, %zmm6
	vpmullw	%zmm10, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm4, %zmm4
	vmovdqa64	%zmm5, %zmm6
	vpermt2w	%zmm4, %zmm2, %zmm6
	vpermt2w	%zmm4, %zmm3, %zmm5
	vmovdqu64	%zmm6, (%rbx,%rax,2)
	vmovdqu64	%zmm5, 64(%rbx,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_13
# %bb.14:
	vmovdqa64	v_9+256(%rip), %zmm1
	xorl	%eax, %eax
	vmovdqa64	.LCPI19_8(%rip), %zmm2 # zmm2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]
	vmovdqa64	.LCPI19_9(%rip), %zmm3 # zmm3 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63]
	.p2align	4, 0x90
.LBB19_15:                              # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rbx,%rax,2), %zmm4
	vmovdqu64	64(%rbx,%rax,2), %zmm5
	vmovdqa64	%zmm4, %zmm6
	vpermt2w	%zmm5, %zmm2, %zmm6
	vpermt2w	%zmm5, %zmm3, %zmm4
	vpmullw	%zmm1, %zmm4, %zmm5
	vpmulhuw	%zmm1, %zmm4, %zmm4
	vpmullw	%zmm8, %zmm5, %zmm7
	vpmulhuw	%zmm9, %zmm7, %zmm7
	vptestnmw	%zmm5, %zmm5, %k1
	vpblendmw	%zmm10, %zmm11, %zmm5 {%k1}
	vpaddw	%zmm5, %zmm4, %zmm4
	vpaddw	%zmm4, %zmm7, %zmm4
	vpsraw	$15, %zmm4, %zmm5
	vpandq	%zmm9, %zmm5, %zmm5
	vpaddw	%zmm4, %zmm5, %zmm4
	vpaddw	%zmm6, %zmm4, %zmm5
	vpsubw	%zmm4, %zmm6, %zmm4
	vpaddw	%zmm12, %zmm4, %zmm4
	vpmulhuw	%zmm0, %zmm4, %zmm6
	vpmullw	%zmm10, %zmm6, %zmm6
	vpaddw	%zmm6, %zmm4, %zmm4
	vmovdqa64	%zmm5, %zmm6
	vpermt2w	%zmm4, %zmm2, %zmm6
	vpermt2w	%zmm4, %zmm3, %zmm5
	vmovdqu64	%zmm6, (%rbx,%rax,2)
	vmovdqu64	%zmm5, 64(%rbx,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_15
# %bb.16:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_17:                              # =>This Inner Loop Header: Depth=1
	vmovdqu64	(%rbx,%rax,2), %zmm0
	vmovdqu64	64(%rbx,%rax,2), %zmm1
	vmovdqa64	v_9+320(%rip), %zmm2
	vpmullw	%zmm1, %zmm2, %zmm3
	vpmulhuw	%zmm2, %zmm1, %zmm1
	vpmullw	%zmm8, %zmm3, %zmm2
	vpmulhuw	%zmm9, %zmm2, %zmm2
	vptestnmw	%zmm3, %zmm3, %k1
	vpblendmw	%zmm10, %zmm11, %zmm3 {%k1}
	vpaddw	%zmm3, %zmm1, %zmm1
	vpaddw	%zmm1, %zmm2, %zmm1
	vpsraw	$15, %zmm1, %zmm2
	vpandq	%zmm9, %zmm2, %zmm2
	vpaddw	%zmm1, %zmm2, %zmm1
	vpaddw	%zmm0, %zmm1, %zmm2
	vpmulhuw	%zmm13, %zmm2, %zmm3
	vpmullw	%zmm10, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm2, %zmm2
	vmovdqu64	%zmm2, (%rbx,%rax,2)
	vpsubw	%zmm1, %zmm0, %zmm0
	vpaddw	%zmm12, %zmm0, %zmm0
	vpmulhuw	%zmm13, %zmm0, %zmm1
	vpmullw	%zmm10, %zmm1, %zmm1
	vpaddw	%zmm1, %zmm0, %zmm0
	vmovdqu64	%zmm0, 64(%rbx,%rax,2)
	addq	$64, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_17
# %bb.18:
	xorl	%eax, %eax
	vmovdqa64	.LCPI19_0(%rip), %zmm0 # zmm0 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI19_4(%rip), %zmm1 # zmm1 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB19_19:                              # =>This Inner Loop Header: Depth=1
	vmovdqa64	v_9+384(%rip), %zmm2
	vmovdqu64	(%rbx,%rax,2), %zmm3
	vmovdqu64	64(%rbx,%rax,2), %zmm4
	vmovdqu64	128(%rbx,%rax,2), %zmm5
	vmovdqu64	192(%rbx,%rax,2), %zmm6
	vpmullw	%zmm5, %zmm2, %zmm7
	vpmulhuw	%zmm2, %zmm5, %zmm2
	vpmullw	%zmm0, %zmm7, %zmm5
	vpmulhuw	%zmm9, %zmm5, %zmm5
	vptestnmw	%zmm7, %zmm7, %k1
	vpblendmw	%zmm10, %zmm11, %zmm7 {%k1}
	vpaddw	%zmm7, %zmm2, %zmm2
	vpaddw	%zmm2, %zmm5, %zmm2
	vpsraw	$15, %zmm2, %zmm5
	vpandq	%zmm9, %zmm5, %zmm5
	vpaddw	%zmm2, %zmm5, %zmm2
	vpaddw	%zmm3, %zmm2, %zmm5
	vmovdqu64	%zmm5, (%rbx,%rax,2)
	vpsubw	%zmm2, %zmm3, %zmm2
	vpaddw	%zmm1, %zmm2, %zmm2
	vpmulhuw	%zmm13, %zmm2, %zmm3
	vpmullw	%zmm10, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm2, %zmm2
	vmovdqu64	%zmm2, 128(%rbx,%rax,2)
	vmovdqa64	v_9+448(%rip), %zmm2
	vpmullw	%zmm6, %zmm2, %zmm3
	vpmulhuw	%zmm2, %zmm6, %zmm2
	vpmullw	%zmm0, %zmm3, %zmm5
	vptestnmw	%zmm3, %zmm3, %k1
	vpblendmw	%zmm10, %zmm11, %zmm3 {%k1}
	vpmulhuw	%zmm9, %zmm5, %zmm5
	vpaddw	%zmm3, %zmm2, %zmm2
	vpaddw	%zmm2, %zmm5, %zmm2
	vpsraw	$15, %zmm2, %zmm3
	vpandq	%zmm9, %zmm3, %zmm3
	vpaddw	%zmm2, %zmm3, %zmm2
	vpaddw	%zmm4, %zmm2, %zmm3
	vmovdqu64	%zmm3, 64(%rbx,%rax,2)
	vpsubw	%zmm2, %zmm4, %zmm2
	vpaddw	%zmm1, %zmm2, %zmm2
	vpmulhuw	%zmm13, %zmm2, %zmm3
	vpmullw	%zmm10, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm2, %zmm2
	vmovdqu64	%zmm2, 192(%rbx,%rax,2)
	subq	$-128, %rax
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_19
# %bb.20:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_21:                              # =>This Inner Loop Header: Depth=1
	vmovdqu64	256(%rbx,%rax,2), %zmm2
	vmovdqa64	v_9+512(%rip), %zmm3
	vpmullw	%zmm2, %zmm3, %zmm4
	vpmulhuw	%zmm3, %zmm2, %zmm2
	vpmullw	%zmm0, %zmm4, %zmm3
	vpmulhuw	%zmm9, %zmm3, %zmm3
	vptestnmw	%zmm4, %zmm4, %k1
	vpblendmw	%zmm10, %zmm11, %zmm4 {%k1}
	vpaddw	%zmm4, %zmm2, %zmm2
	vpaddw	%zmm2, %zmm3, %zmm2
	vpsraw	$15, %zmm2, %zmm3
	vpandq	%zmm9, %zmm3, %zmm3
	vpaddw	%zmm2, %zmm3, %zmm3
	vmovdqu64	(%rbx,%rax,2), %zmm4
	vmovdqu64	64(%rbx,%rax,2), %zmm5
	vmovdqu64	128(%rbx,%rax,2), %zmm6
	vmovdqu64	192(%rbx,%rax,2), %zmm2
	vpaddw	%zmm4, %zmm3, %zmm7
	vpmulhuw	%zmm13, %zmm7, %zmm8
	vpmullw	%zmm10, %zmm8, %zmm8
	vpaddw	%zmm8, %zmm7, %zmm7
	vmovdqu64	%zmm7, (%rbx,%rax,2)
	vpsubw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm1, %zmm3, %zmm3
	vpmulhuw	%zmm13, %zmm3, %zmm4
	vpmullw	%zmm10, %zmm4, %zmm4
	vpaddw	%zmm4, %zmm3, %zmm3
	vmovdqu64	%zmm3, 256(%rbx,%rax,2)
	vmovdqu64	320(%rbx,%rax,2), %zmm3
	vmovdqa64	v_9+576(%rip), %zmm4
	vpmullw	%zmm3, %zmm4, %zmm7
	vpmulhuw	%zmm4, %zmm3, %zmm3
	vpmullw	%zmm0, %zmm7, %zmm4
	vpmulhuw	%zmm9, %zmm4, %zmm4
	vptestnmw	%zmm7, %zmm7, %k1
	vpblendmw	%zmm10, %zmm11, %zmm7 {%k1}
	vpaddw	%zmm7, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm4, %zmm3
	vpsraw	$15, %zmm3, %zmm4
	vpandq	%zmm9, %zmm4, %zmm4
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm5, %zmm3, %zmm4
	vpmulhuw	%zmm13, %zmm4, %zmm7
	vpmullw	%zmm10, %zmm7, %zmm7
	vpaddw	%zmm7, %zmm4, %zmm4
	vmovdqu64	%zmm4, 64(%rbx,%rax,2)
	vpsubw	%zmm3, %zmm5, %zmm3
	vpaddw	%zmm1, %zmm3, %zmm3
	vpmulhuw	%zmm13, %zmm3, %zmm4
	vpmullw	%zmm10, %zmm4, %zmm4
	vpaddw	%zmm4, %zmm3, %zmm3
	vmovdqu64	%zmm3, 320(%rbx,%rax,2)
	vmovdqu64	384(%rbx,%rax,2), %zmm3
	vmovdqa64	v_9+640(%rip), %zmm4
	vpmullw	%zmm3, %zmm4, %zmm5
	vpmulhuw	%zmm4, %zmm3, %zmm3
	vpmullw	%zmm0, %zmm5, %zmm4
	vptestnmw	%zmm5, %zmm5, %k1
	vpblendmw	%zmm10, %zmm11, %zmm5 {%k1}
	vpmulhuw	%zmm9, %zmm4, %zmm4
	vpaddw	%zmm5, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm4, %zmm3
	vpsraw	$15, %zmm3, %zmm4
	vpandq	%zmm9, %zmm4, %zmm4
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm6, %zmm3, %zmm4
	vpmulhuw	%zmm13, %zmm4, %zmm5
	vpmullw	%zmm10, %zmm5, %zmm5
	vpaddw	%zmm5, %zmm4, %zmm4
	vmovdqu64	%zmm4, 128(%rbx,%rax,2)
	vpsubw	%zmm3, %zmm6, %zmm3
	vpaddw	%zmm1, %zmm3, %zmm3
	vpmulhuw	%zmm13, %zmm3, %zmm4
	vpmullw	%zmm10, %zmm4, %zmm4
	vpaddw	%zmm4, %zmm3, %zmm3
	vmovdqu64	%zmm3, 384(%rbx,%rax,2)
	vmovdqu64	448(%rbx,%rax,2), %zmm3
	vmovdqa64	v_9+704(%rip), %zmm4
	vpmullw	%zmm3, %zmm4, %zmm5
	vpmulhuw	%zmm4, %zmm3, %zmm3
	vpmullw	%zmm0, %zmm5, %zmm4
	vpmulhuw	%zmm9, %zmm4, %zmm4
	vptestnmw	%zmm5, %zmm5, %k1
	vpblendmw	%zmm10, %zmm11, %zmm5 {%k1}
	vpaddw	%zmm5, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm4, %zmm3
	vpsraw	$15, %zmm3, %zmm4
	vpandq	%zmm9, %zmm4, %zmm4
	vpaddw	%zmm3, %zmm4, %zmm3
	vpaddw	%zmm2, %zmm3, %zmm4
	vpmulhuw	%zmm13, %zmm4, %zmm5
	vpmullw	%zmm10, %zmm5, %zmm5
	vpaddw	%zmm5, %zmm4, %zmm4
	vmovdqu64	%zmm4, 192(%rbx,%rax,2)
	vpsubw	%zmm3, %zmm2, %zmm2
	vpaddw	%zmm1, %zmm2, %zmm2
	vpmulhuw	%zmm13, %zmm2, %zmm3
	vpmullw	%zmm10, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm2, %zmm2
	vmovdqu64	%zmm2, 448(%rbx,%rax,2)
	addq	$256, %rax              # imm = 0x100
	cmpq	$1024, %rax             # imm = 0x400
	jb	.LBB19_21
# %bb.22:
	movq	%rbx, %rdi
	vzeroupper
	callq	fft9
	vmovdqa64	.LCPI19_5(%rip), %zmm9 # zmm9 = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	vmovdqa64	.LCPI19_3(%rip), %zmm7 # zmm7 = [53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248,53248]
	vmovdqa64	.LCPI19_2(%rip), %zmm6 # zmm6 = [53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247,53247]
	vmovdqa64	.LCPI19_1(%rip), %zmm5 # zmm5 = [12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289,12289]
	movq	$-1024, %rax            # imm = 0xFC00
	vmovdqa64	.LCPI19_0(%rip), %zmm4 # zmm4 = [12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287,12287]
	vmovdqa64	.LCPI19_4(%rip), %zmm8 # zmm8 = [24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578,24578]
	.p2align	4, 0x90
.LBB19_23:                              # =>This Inner Loop Header: Depth=1
	vmovdqu64	2048(%rbx,%rax), %zmm0
	vmovdqa64	v_9+2304(%rax), %zmm1
	vpmullw	%zmm0, %zmm1, %zmm2
	vpmulhuw	%zmm1, %zmm0, %zmm0
	vpmullw	%zmm4, %zmm2, %zmm1
	vpmulhuw	%zmm5, %zmm1, %zmm1
	vptestnmw	%zmm2, %zmm2, %k1
	vpblendmw	%zmm6, %zmm7, %zmm2 {%k1}
	vpaddw	%zmm2, %zmm0, %zmm0
	vpaddw	%zmm0, %zmm1, %zmm0
	vpsraw	$15, %zmm0, %zmm1
	vpandq	%zmm5, %zmm1, %zmm1
	vpaddw	%zmm0, %zmm1, %zmm0
	vmovdqu64	1024(%rbx,%rax), %zmm1
	vpaddw	%zmm1, %zmm0, %zmm2
	vpmulhuw	%zmm9, %zmm2, %zmm3
	vpmullw	%zmm6, %zmm3, %zmm3
	vpaddw	%zmm3, %zmm2, %zmm2
	vmovdqu64	%zmm2, 1024(%rbx,%rax)
	vpsubw	%zmm0, %zmm1, %zmm0
	vpaddw	%zmm8, %zmm0, %zmm0
	vpmulhuw	%zmm9, %zmm0, %zmm1
	vpmullw	%zmm6, %zmm1, %zmm1
	vpaddw	%zmm1, %zmm0, %zmm0
	vmovdqu64	%zmm0, 2048(%rbx,%rax)
	addq	$64, %rax
	jne	.LBB19_23
# %bb.24:
	popq	%rbx
	.cfi_def_cfa_offset 8
	vzeroupper
	retq
.Lfunc_end19:
	.size	fft, .Lfunc_end19-fft
	.cfi_endproc
                                        # -- End function
	.type	v_9,@object             # @v_9
	.data
	.globl	v_9
	.p2align	6
v_9:
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	7888                    # 0x1ed0
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	11060                   # 0x2b34
	.short	7888                    # 0x1ed0
	.short	11208                   # 0x2bc8
	.short	4091                    # 0xffb
	.short	6960                    # 0x1b30
	.short	11060                   # 0x2b34
	.short	6275                    # 0x1883
	.short	7888                    # 0x1ed0
	.short	4342                    # 0x10f6
	.short	11208                   # 0x2bc8
	.short	9759                    # 0x261f
	.short	4091                    # 0xffb
	.short	6960                    # 0x1b30
	.short	11060                   # 0x2b34
	.short	6275                    # 0x1883
	.short	7888                    # 0x1ed0
	.short	4342                    # 0x10f6
	.short	11208                   # 0x2bc8
	.short	9759                    # 0x261f
	.short	4091                    # 0xffb
	.short	6960                    # 0x1b30
	.short	11060                   # 0x2b34
	.short	6275                    # 0x1883
	.short	7888                    # 0x1ed0
	.short	4342                    # 0x10f6
	.short	11208                   # 0x2bc8
	.short	9759                    # 0x261f
	.short	4091                    # 0xffb
	.short	6960                    # 0x1b30
	.short	11060                   # 0x2b34
	.short	6275                    # 0x1883
	.short	7888                    # 0x1ed0
	.short	4342                    # 0x10f6
	.short	11208                   # 0x2bc8
	.short	9759                    # 0x261f
	.short	4091                    # 0xffb
	.short	1591                    # 0x637
	.short	6960                    # 0x1b30
	.short	586                     # 0x24a
	.short	11060                   # 0x2b34
	.short	9477                    # 0x2505
	.short	6275                    # 0x1883
	.short	7538                    # 0x1d72
	.short	7888                    # 0x1ed0
	.short	6399                    # 0x18ff
	.short	4342                    # 0x10f6
	.short	5825                    # 0x16c1
	.short	11208                   # 0x2bc8
	.short	5266                    # 0x1492
	.short	9759                    # 0x261f
	.short	9710                    # 0x25ee
	.short	4091                    # 0xffb
	.short	1591                    # 0x637
	.short	6960                    # 0x1b30
	.short	586                     # 0x24a
	.short	11060                   # 0x2b34
	.short	9477                    # 0x2505
	.short	6275                    # 0x1883
	.short	7538                    # 0x1d72
	.short	7888                    # 0x1ed0
	.short	6399                    # 0x18ff
	.short	4342                    # 0x10f6
	.short	5825                    # 0x16c1
	.short	11208                   # 0x2bc8
	.short	5266                    # 0x1492
	.short	9759                    # 0x261f
	.short	9710                    # 0x25ee
	.short	4091                    # 0xffb
	.short	1134                    # 0x46e
	.short	1591                    # 0x637
	.short	10414                   # 0x28ae
	.short	6960                    # 0x1b30
	.short	7099                    # 0x1bbb
	.short	586                     # 0x24a
	.short	1364                    # 0x554
	.short	11060                   # 0x2b34
	.short	1711                    # 0x6af
	.short	9477                    # 0x2505
	.short	1885                    # 0x75d
	.short	6275                    # 0x1883
	.short	3743                    # 0xe9f
	.short	7538                    # 0x1d72
	.short	10164                   # 0x27b4
	.short	7888                    # 0x1ed0
	.short	6407                    # 0x1907
	.short	6399                    # 0x18ff
	.short	8100                    # 0x1fa4
	.short	4342                    # 0x10f6
	.short	7674                    # 0x1dfa
	.short	5825                    # 0x16c1
	.short	10329                   # 0x2859
	.short	11208                   # 0x2bc8
	.short	965                     # 0x3c5
	.short	5266                    # 0x1492
	.short	1688                    # 0x698
	.short	9759                    # 0x261f
	.short	6442                    # 0x192a
	.short	9710                    # 0x25ee
	.short	9180                    # 0x23dc
	.short	4091                    # 0xffb
	.short	12210                   # 0x2fb2
	.short	1134                    # 0x46e
	.short	12189                   # 0x2f9d
	.short	1591                    # 0x637
	.short	2829                    # 0xb0d
	.short	10414                   # 0x28ae
	.short	2181                    # 0x885
	.short	6960                    # 0x1b30
	.short	4783                    # 0x12af
	.short	7099                    # 0x1bbb
	.short	7610                    # 0x1dba
	.short	586                     # 0x24a
	.short	7144                    # 0x1be8
	.short	1364                    # 0x554
	.short	4843                    # 0x12eb
	.short	11060                   # 0x2b34
	.short	997                     # 0x3e5
	.short	1711                    # 0x6af
	.short	10751                   # 0x29ff
	.short	9477                    # 0x2505
	.short	4431                    # 0x114f
	.short	1885                    # 0x75d
	.short	8720                    # 0x2210
	.short	6275                    # 0x1883
	.short	1549                    # 0x60d
	.short	3743                    # 0xe9f
	.short	3983                    # 0xf8f
	.short	7538                    # 0x1d72
	.short	5664                    # 0x1620
	.short	10164                   # 0x27b4
	.short	14                      # 0xe
	.short	7888                    # 0x1ed0
	.short	6240                    # 0x1860
	.short	6407                    # 0x1907
	.short	432                     # 0x1b0
	.short	6399                    # 0x18ff
	.short	6458                    # 0x193a
	.short	8100                    # 0x1fa4
	.short	6308                    # 0x18a4
	.short	4342                    # 0x10f6
	.short	4407                    # 0x1137
	.short	7674                    # 0x1dfa
	.short	1534                    # 0x5fe
	.short	5825                    # 0x16c1
	.short	2564                    # 0xa04
	.short	10329                   # 0x2859
	.short	1690                    # 0x69a
	.short	11208                   # 0x2bc8
	.short	117                     # 0x75
	.short	965                     # 0x3c5
	.short	1237                    # 0x4d5
	.short	5266                    # 0x1492
	.short	8877                    # 0x22ad
	.short	1688                    # 0x698
	.short	6570                    # 0x19aa
	.short	9759                    # 0x261f
	.short	7072                    # 0x1ba0
	.short	6442                    # 0x192a
	.short	7863                    # 0x1eb7
	.short	9710                    # 0x25ee
	.short	4042                    # 0xfca
	.short	9180                    # 0x23dc
	.short	3872                    # 0xf20
	.short	4091                    # 0xffb
	.short	5569                    # 0x15c1
	.short	12210                   # 0x2fb2
	.short	11061                   # 0x2b35
	.short	1134                    # 0x46e
	.short	3316                    # 0xcf4
	.short	12189                   # 0x2f9d
	.short	9179                    # 0x23db
	.short	1591                    # 0x637
	.short	1553                    # 0x611
	.short	2829                    # 0xb0d
	.short	730                     # 0x2da
	.short	10414                   # 0x28ae
	.short	9277                    # 0x243d
	.short	2181                    # 0x885
	.short	7613                    # 0x1dbd
	.short	6960                    # 0x1b30
	.short	7543                    # 0x1d77
	.short	4783                    # 0x12af
	.short	4970                    # 0x136a
	.short	7099                    # 0x1bbb
	.short	10637                   # 0x298d
	.short	7610                    # 0x1dba
	.short	3180                    # 0xc6c
	.short	586                     # 0x24a
	.short	1020                    # 0x3fc
	.short	7144                    # 0x1be8
	.short	5892                    # 0x1704
	.short	1364                    # 0x554
	.short	10469                   # 0x28e5
	.short	4843                    # 0x12eb
	.short	6836                    # 0x1ab4
	.short	11060                   # 0x2b34
	.short	12163                   # 0x2f83
	.short	997                     # 0x3e5
	.short	2742                    # 0xab6
	.short	1711                    # 0x6af
	.short	5285                    # 0x14a5
	.short	10751                   # 0x29ff
	.short	3782                    # 0xec6
	.short	9477                    # 0x2505
	.short	8401                    # 0x20d1
	.short	4431                    # 0x114f
	.short	3854                    # 0xf0e
	.short	1885                    # 0x75d
	.short	3323                    # 0xcfb
	.short	8720                    # 0x2210
	.short	834                     # 0x342
	.short	6275                    # 0x1883
	.short	4673                    # 0x1241
	.short	1549                    # 0x60d
	.short	10078                   # 0x275e
	.short	3743                    # 0xe9f
	.short	9493                    # 0x2515
	.short	3983                    # 0xf8f
	.short	4668                    # 0x123c
	.short	7538                    # 0x1d72
	.short	10772                   # 0x2a14
	.short	5664                    # 0x1620
	.short	9020                    # 0x233c
	.short	10164                   # 0x27b4
	.short	1502                    # 0x5de
	.short	14                      # 0xe
	.short	5351                    # 0x14e7
	.short	7888                    # 0x1ed0
	.short	9368                    # 0x2498
	.short	6240                    # 0x1860
	.short	9729                    # 0x2601
	.short	6407                    # 0x1907
	.short	11236                   # 0x2be4
	.short	432                     # 0x1b0
	.short	3604                    # 0xe14
	.short	6399                    # 0x18ff
	.short	1156                    # 0x484
	.short	6458                    # 0x193a
	.short	1762                    # 0x6e2
	.short	8100                    # 0x1fa4
	.short	6130                    # 0x17f2
	.short	6308                    # 0x18a4
	.short	9386                    # 0x24aa
	.short	4342                    # 0x10f6
	.short	2315                    # 0x90b
	.short	4407                    # 0x1137
	.short	10481                   # 0x28f1
	.short	7674                    # 0x1dfa
	.short	10086                   # 0x2766
	.short	1534                    # 0x5fe
	.short	3467                    # 0xd8b
	.short	5825                    # 0x16c1
	.short	2967                    # 0xb97
	.short	2564                    # 0xa04
	.short	10922                   # 0x2aaa
	.short	10329                   # 0x2859
	.short	489                     # 0x1e9
	.short	1690                    # 0x69a
	.short	3403                    # 0xd4b
	.short	11208                   # 0x2bc8
	.short	2019                    # 0x7e3
	.short	117                     # 0x75
	.short	12241                   # 0x2fd1
	.short	965                     # 0x3c5
	.short	11578                   # 0x2d3a
	.short	1237                    # 0x4d5
	.short	10206                   # 0x27de
	.short	5266                    # 0x1492
	.short	11389                   # 0x2c7d
	.short	8877                    # 0x22ad
	.short	2030                    # 0x7ee
	.short	1688                    # 0x698
	.short	883                     # 0x373
	.short	6570                    # 0x19aa
	.short	7703                    # 0x1e17
	.short	9759                    # 0x261f
	.short	7340                    # 0x1cac
	.short	7072                    # 0x1ba0
	.short	1195                    # 0x4ab
	.short	6442                    # 0x192a
	.short	6180                    # 0x1824
	.short	7863                    # 0x1eb7
	.short	2446                    # 0x98e
	.short	9710                    # 0x25ee
	.short	7045                    # 0x1b85
	.short	4042                    # 0xfca
	.short	5274                    # 0x149a
	.short	9180                    # 0x23dc
	.short	2851                    # 0xb23
	.short	3872                    # 0xf20
	.short	12276                   # 0x2ff4
	.short	4091                    # 0xffb
	.short	3580                    # 0xdfc
	.short	5569                    # 0x15c1
	.short	737                     # 0x2e1
	.short	12210                   # 0x2fb2
	.short	6945                    # 0x1b21
	.short	11061                   # 0x2b35
	.short	932                     # 0x3a4
	.short	1134                    # 0x46e
	.short	6865                    # 0x1ad1
	.short	3316                    # 0xcf4
	.short	10733                   # 0x29ed
	.short	12189                   # 0x2f9d
	.short	5680                    # 0x1630
	.short	9179                    # 0x23db
	.short	4602                    # 0x11fa
	.short	1591                    # 0x637
	.short	10401                   # 0x28a1
	.short	1553                    # 0x611
	.short	5186                    # 0x1442
	.short	2829                    # 0xb0d
	.short	8901                    # 0x22c5
	.short	730                     # 0x2da
	.short	7692                    # 0x1e0c
	.short	10414                   # 0x28ae
	.short	8188                    # 0x1ffc
	.short	9277                    # 0x243d
	.short	6409                    # 0x1909
	.short	2181                    # 0x885
	.short	1467                    # 0x5bb
	.short	7613                    # 0x1dbd
	.short	5070                    # 0x13ce
	.short	6960                    # 0x1b30
	.short	10209                   # 0x27e1
	.short	7543                    # 0x1d77
	.short	9046                    # 0x2356
	.short	4783                    # 0x12af
	.short	6057                    # 0x17a9
	.short	4970                    # 0x136a
	.short	351                     # 0x15f
	.short	7099                    # 0x1bbb
	.short	12145                   # 0x2f71
	.short	10637                   # 0x298d
	.short	2895                    # 0xb4f
	.short	7610                    # 0x1dba
	.short	10156                   # 0x27ac
	.short	3180                    # 0xc6c
	.short	3711                    # 0xe7f
	.short	586                     # 0x24a
	.short	6040                    # 0x1798
	.short	1020                    # 0x3fc
	.short	3509                    # 0xdb5
	.short	7144                    # 0x1be8
	.short	9589                    # 0x2575
	.short	5892                    # 0x1704
	.short	2053                    # 0x805
	.short	1364                    # 0x554
	.short	6090                    # 0x17ca
	.short	10469                   # 0x28e5
	.short	5064                    # 0x13c8
	.short	4843                    # 0x12eb
	.short	2649                    # 0xa59
	.short	6836                    # 0x1ab4
	.short	7421                    # 0x1cfd
	.short	11060                   # 0x2b34
	.short	10820                   # 0x2a44
	.short	12163                   # 0x2f83
	.short	4699                    # 0x125b
	.short	997                     # 0x3e5
	.short	9731                    # 0x2603
	.short	2742                    # 0xab6
	.short	8927                    # 0x22df
	.short	1711                    # 0x6af
	.short	3585                    # 0xe01
	.short	5285                    # 0x14a5
	.short	7037                    # 0x1b7d
	.short	10751                   # 0x29ff
	.short	6251                    # 0x186b
	.short	3782                    # 0xec6
	.short	11300                   # 0x2c24
	.short	9477                    # 0x2505
	.short	7338                    # 0x1caa
	.short	8401                    # 0x20d1
	.short	4552                    # 0x11c8
	.short	4431                    # 0x114f
	.short	8846                    # 0x228e
	.short	3854                    # 0xf0e
	.short	12126                   # 0x2f5e
	.short	1885                    # 0x75d
	.short	3533                    # 0xdcd
	.short	3323                    # 0xcfb
	.short	2962                    # 0xb92
	.short	8720                    # 0x2210
	.short	8553                    # 0x2169
	.short	834                     # 0x342
	.short	11616                   # 0x2d60
	.short	6275                    # 0x1883
	.short	12250                   # 0x2fda
	.short	4673                    # 0x1241
	.short	16                      # 0x10
	.short	1549                    # 0x60d
	.short	7871                    # 0x1ebf
	.short	10078                   # 0x275e
	.short	237                     # 0xed
	.short	3743                    # 0xe9f
	.short	3684                    # 0xe64
	.short	9493                    # 0x2515
	.short	8887                    # 0x22b7
	.short	3983                    # 0xf8f
	.short	2341                    # 0x925
	.short	4668                    # 0x123c
	.short	300                     # 0x12c
	.short	7538                    # 0x1d72
	.short	9330                    # 0x2472
	.short	10772                   # 0x2a14
	.short	7516                    # 0x1d5c
	.short	5664                    # 0x1620
	.short	7630                    # 0x1dce
	.short	9020                    # 0x233c
	.short	3802                    # 0xeda
	.short	10164                   # 0x27b4
	.short	10099                   # 0x2773
	.short	1502                    # 0x5de
	.short	5625                    # 0x15f9
	.short	14                      # 0xe
	.short	9036                    # 0x234c
	.short	5351                    # 0x14e7
	.short	5746                    # 0x1672
	.short	7888                    # 0x1ed0
	.short	1739                    # 0x6cb
	.short	9368                    # 0x2498
	.short	3698                    # 0xe72
	.short	6240                    # 0x1860
	.short	1949                    # 0x79d
	.short	9729                    # 0x2601
	.short	10229                   # 0x27f5
	.short	6407                    # 0x1907
	.short	9668                    # 0x25c4
	.short	11236                   # 0x2be4
	.short	3281                    # 0xcd1
	.short	432                     # 0x1b0
	.short	4956                    # 0x135c
	.short	3604                    # 0xe14
	.short	1748                    # 0x6d4
	.short	6399                    # 0x18ff
	.short	2749                    # 0xabd
	.short	1156                    # 0x484
	.short	10531                   # 0x2923
	.short	6458                    # 0x193a
	.short	9229                    # 0x240d
	.short	1762                    # 0x6e2
	.short	3146                    # 0xc4a
	.short	8100                    # 0x1fa4
	.short	6902                    # 0x1af6
	.short	6130                    # 0x17f2
	.short	8197                    # 0x2005
	.short	6308                    # 0x18a4
	.short	5460                    # 0x1554
	.short	9386                    # 0x24aa
	.short	10049                   # 0x2741
	.short	4342                    # 0x10f6
	.short	4070                    # 0xfe6
	.short	2315                    # 0x90b
	.short	3687                    # 0xe67
	.short	4407                    # 0x1137
	.short	378                     # 0x17a
	.short	10481                   # 0x28f1
	.short	9298                    # 0x2452
	.short	7674                    # 0x1dfa
	.short	4063                    # 0xfdf
	.short	10086                   # 0x2766
	.short	7156                    # 0x1bf4
	.short	1534                    # 0x5fe
	.short	8723                    # 0x2213
	.short	3467                    # 0xd8b
	.short	4614                    # 0x1206
	.short	5825                    # 0x16c1
	.short	943                     # 0x3af
	.short	2967                    # 0xb97
	.short	8436                    # 0x20f4
	.short	2564                    # 0xa04
	.short	11664                   # 0x2d90
	.short	10922                   # 0x2aaa
	.short	11285                   # 0x2c15
	.short	10329                   # 0x2859
	.short	727                     # 0x2d7
	.short	489                     # 0x1e9
	.short	6634                    # 0x19ea
	.short	1690                    # 0x69a
	.short	2320                    # 0x910
	.short	3403                    # 0xd4b
	.short	10707                   # 0x29d3
	.short	11208                   # 0x2bc8
	.short	9787                    # 0x263b
	.short	2019                    # 0x7e3
	.short	5753                    # 0x1679
	.short	117                     # 0x75
	.short	10559                   # 0x293f
	.short	12241                   # 0x2fd1
	.short	7642                    # 0x1dda
	.short	965                     # 0x3c5
	.short	6633                    # 0x19e9
	.short	11578                   # 0x2d3a
	.short	1060                    # 0x424
	.short	1237                    # 0x4d5
	.short	8388                    # 0x20c4
	.short	10206                   # 0x27de
	.short	340                     # 0x154
	.short	5266                    # 0x1492
	.short	10574                   # 0x294e
	.short	11389                   # 0x2c7d
	.short	1964                    # 0x7ac
	.short	8877                    # 0x22ad
	.short	4551                    # 0x11c7
	.short	2030                    # 0x7ee
	.short	7586                    # 0x1da2
	.short	1688                    # 0x698
	.short	9807                    # 0x264f
	.short	883                     # 0x373
	.short	6375                    # 0x18e7
	.short	6570                    # 0x19aa
	.short	7783                    # 0x1e67
	.short	7703                    # 0x1e17
	.short	12247                   # 0x2fd7
	.short	9759                    # 0x261f
	.short	8525                    # 0x214d
	.short	7340                    # 0x1cac
	.short	914                     # 0x392
	.short	7072                    # 0x1ba0
	.short	8763                    # 0x223b
	.short	1195                    # 0x4ab
	.short	5858                    # 0x16e2
	.short	6442                    # 0x192a
	.short	7680                    # 0x1e00
	.short	6180                    # 0x1824
	.short	5357                    # 0x14ed
	.short	7863                    # 0x1eb7
	.short	3159                    # 0xc57
	.short	2446                    # 0x98e
	.short	10993                   # 0x2af1
	.short	9710                    # 0x25ee
	.short	1477                    # 0x5c5
	.short	7045                    # 0x1b85
	.short	5381                    # 0x1505
	.short	4042                    # 0xfca
	.short	8821                    # 0x2275
	.short	5274                    # 0x149a
	.short	5204                    # 0x1454
	.short	9180                    # 0x23dc
	.short	7003                    # 0x1b5b
	.short	2851                    # 0xb23
	.short	278                     # 0x116
	.short	3872                    # 0xf20
	.short	6188                    # 0x182c
	.short	12276                   # 0x2ff4
	.short	5654                    # 0x1616
	.short	4091                    # 0xffb
	.short	3835                    # 0xefb
	.short	3580                    # 0xdfc
	.short	3374                    # 0xd2e
	.short	5569                    # 0x15c1
	.short	2523                    # 0x9db
	.short	737                     # 0x2e1
	.short	11535                   # 0x2d0f
	.short	12210                   # 0x2fb2
	.short	8418                    # 0x20e2
	.short	6945                    # 0x1b21
	.short	8502                    # 0x2136
	.short	11061                   # 0x2b35
	.short	1273                    # 0x4f9
	.short	932                     # 0x3a4
	.short	8801                    # 0x2261
	.short	1134                    # 0x46e
	.short	6410                    # 0x190a
	.short	6865                    # 0x1ad1
	.short	4582                    # 0x11e6
	.short	3316                    # 0xcf4
	.short	2727                    # 0xaa7
	.short	10733                   # 0x29ed
	.short	9779                    # 0x2633
	.short	12189                   # 0x2f9d
	.short	7389                    # 0x1cdd
	.short	5680                    # 0x1630
	.short	7962                    # 0x1f1a
	.short	9179                    # 0x23db
	.short	7367                    # 0x1cc7
	.short	4602                    # 0x11fa
	.short	4296                    # 0x10c8
	.short	1591                    # 0x637
	.short	4225                    # 0x1081
	.short	10401                   # 0x28a1
	.short	5800                    # 0x16a8
	.short	1553                    # 0x611
	.short	2363                    # 0x93b
	.short	5186                    # 0x1442
	.short	8334                    # 0x208e
	.short	2829                    # 0xb0d
	.short	3442                    # 0xd72
	.short	8901                    # 0x22c5
	.short	6034                    # 0x1792
	.short	730                     # 0x2da
	.short	11192                   # 0x2bb8
	.short	7692                    # 0x1e0c
	.short	8238                    # 0x202e
	.short	10414                   # 0x28ae
	.short	6437                    # 0x1925
	.short	8188                    # 0x1ffc
	.short	7964                    # 0x1f1c
	.short	9277                    # 0x243d
	.short	12169                   # 0x2f89
	.short	6409                    # 0x1909
	.short	6816                    # 0x1aa0
	.short	2181                    # 0x885
	.short	8557                    # 0x216d
	.short	1467                    # 0x5bb
	.short	10438                   # 0x28c6
	.short	7613                    # 0x1dbd
	.short	4367                    # 0x110f
	.short	5070                    # 0x13ce
	.short	2650                    # 0xa5a
	.short	6960                    # 0x1b30
	.short	9237                    # 0x2415
	.short	10209                   # 0x27e1
	.short	8681                    # 0x21e9
	.short	7543                    # 0x1d77
	.short	937                     # 0x3a9
	.short	9046                    # 0x2356
	.short	850                     # 0x352
	.short	4783                    # 0x12af
	.short	876                     # 0x36c
	.short	6057                    # 0x17a9
	.short	1857                    # 0x741
	.short	4970                    # 0x136a
	.short	10039                   # 0x2737
	.short	351                     # 0x15f
	.short	4910                    # 0x132e
	.short	7099                    # 0x1bbb
	.short	3759                    # 0xeaf
	.short	12145                   # 0x2f71
	.short	5233                    # 0x1471
	.short	10637                   # 0x298d
	.short	5075                    # 0x13d3
	.short	2895                    # 0xb4f
	.short	6676                    # 0x1a14
	.short	7610                    # 0x1dba
	.short	4220                    # 0x107c
	.short	10156                   # 0x27ac
	.short	6084                    # 0x17c4
	.short	3180                    # 0xc6c
	.short	8352                    # 0x20a0
	.short	3711                    # 0xe7f
	.short	9793                    # 0x2641
	.short	586                     # 0x24a
	.short	4136                    # 0x1028
	.short	6040                    # 0x1798
	.short	1024                    # 0x400
	.short	1020                    # 0x3fc
	.short	824                     # 0x338
	.short	3509                    # 0xdb5
	.short	12184                   # 0x2f98
	.short	7144                    # 0x1be8
	.short	5964                    # 0x174c
	.short	9589                    # 0x2575
	.short	2879                    # 0xb3f
	.short	5892                    # 0x1704
	.short	6061                    # 0x17ad
	.short	2053                    # 0x805
	.short	2285                    # 0x8ed
	.short	1364                    # 0x554
	.short	5391                    # 0x150f
	.short	6090                    # 0x17ca
	.short	3474                    # 0xd92
	.short	10469                   # 0x28e5
	.short	9132                    # 0x23ac
	.short	5064                    # 0x13c8
	.short	2356                    # 0x934
	.short	4843                    # 0x12eb
	.short	3816                    # 0xee8
	.short	2649                    # 0xa59
	.short	6911                    # 0x1aff
	.short	6836                    # 0x1ab4
	.short	3161                    # 0xc59
	.short	7421                    # 0x1cfd
	.short	7248                    # 0x1c50
	.short	11060                   # 0x2b34
	.short	1224                    # 0x4c8
	.short	10820                   # 0x2a44
	.short	1753                    # 0x6d9
	.short	12163                   # 0x2f83
	.short	6115                    # 0x17e3
	.short	4699                    # 0x125b
	.short	9049                    # 0x2359
	.short	997                     # 0x3e5
	.short	11986                   # 0x2ed2
	.short	9731                    # 0x2603
	.short	9837                    # 0x266d
	.short	2742                    # 0xab6
	.short	11468                   # 0x2ccc
	.short	8927                    # 0x22df
	.short	7308                    # 0x1c8c
	.short	1711                    # 0x6af
	.short	10105                   # 0x2779
	.short	3585                    # 0xe01
	.short	3619                    # 0xe23
	.short	5285                    # 0x14a5
	.short	896                     # 0x380
	.short	7037                    # 0x1b7d
	.short	721                     # 0x2d1
	.short	10751                   # 0x29ff
	.short	10661                   # 0x29a5
	.short	6251                    # 0x186b
	.short	11363                   # 0x2c63
	.short	3782                    # 0xec6
	.short	983                     # 0x3d7
	.short	11300                   # 0x2c24
	.short	695                     # 0x2b7
	.short	9477                    # 0x2505
	.short	9680                    # 0x25d0
	.short	7338                    # 0x1caa
	.short	3181                    # 0xc6d
	.short	8401                    # 0x20d1
	.short	6112                    # 0x17e0
	.short	4552                    # 0x11c8
	.short	1846                    # 0x736
	.short	4431                    # 0x114f
	.short	8206                    # 0x200e
	.short	8846                    # 0x228e
	.short	3339                    # 0xd0b
	.short	3854                    # 0xf0e
	.short	4511                    # 0x119f
	.short	12126                   # 0x2f5e
	.short	4302                    # 0x10ce
	.short	1885                    # 0x75d
	.short	6342                    # 0x18c6
	.short	3533                    # 0xdcd
	.short	1071                    # 0x42f
	.short	3323                    # 0xcfb
	.short	3070                    # 0xbfe
	.short	2962                    # 0xb92
	.short	9959                    # 0x26e7
	.short	8720                    # 0x2210
	.short	9454                    # 0x24ee
	.short	8553                    # 0x2169
	.short	1271                    # 0x4f7
	.short	834                     # 0x342
	.short	3999                    # 0xf9f
	.short	11616                   # 0x2d60
	.short	3890                    # 0xf32
	.short	6275                    # 0x1883
	.short	250                     # 0xfa
	.short	12250                   # 0x2fda
	.short	10378                   # 0x288a
	.short	4673                    # 0x1241
	.short	7775                    # 0x1e5f
	.short	16                      # 0x10
	.short	784                     # 0x310
	.short	1549                    # 0x60d
	.short	2167                    # 0x877
	.short	7871                    # 0x1ebf
	.short	4720                    # 0x1270
	.short	10078                   # 0x275e
	.short	2262                    # 0x8d6
	.short	237                     # 0xed
	.short	11613                   # 0x2d5d
	.short	3743                    # 0xe9f
	.short	11361                   # 0x2c61
	.short	3684                    # 0xe64
	.short	8470                    # 0x2116
	.short	9493                    # 0x2515
	.short	10464                   # 0x28e0
	.short	8887                    # 0x22b7
	.short	5348                    # 0x14e4
	.short	3983                    # 0xf8f
	.short	10832                   # 0x2a50
	.short	2341                    # 0x925
	.short	4108                    # 0x100c
	.short	4668                    # 0x123c
	.short	7530                    # 0x1d6a
	.short	300                     # 0x12c
	.short	2411                    # 0x96b
	.short	7538                    # 0x1d72
	.short	692                     # 0x2b4
	.short	9330                    # 0x2472
	.short	2477                    # 0x9ad
	.short	10772                   # 0x2a14
	.short	11690                   # 0x2daa
	.short	7516                    # 0x1d5c
	.short	11903                   # 0x2e7f
	.short	5664                    # 0x1620
	.short	7178                    # 0x1c0a
	.short	7630                    # 0x1dce
	.short	5200                    # 0x1450
	.short	9020                    # 0x233c
	.short	11865                   # 0x2e59
	.short	3802                    # 0xeda
	.short	1963                    # 0x7ab
	.short	10164                   # 0x27b4
	.short	6476                    # 0x194c
	.short	10099                   # 0x2773
	.short	3291                    # 0xcdb
	.short	1502                    # 0x5de
	.short	12153                   # 0x2f79
	.short	5625                    # 0x15f9
	.short	5267                    # 0x1493
	.short	14                      # 0xe
	.short	686                     # 0x2ae
	.short	9036                    # 0x234c
	.short	360                     # 0x168
	.short	5351                    # 0x14e7
	.short	4130                    # 0x1022
	.short	5746                    # 0x1672
	.short	11196                   # 0x2bbc
	.short	7888                    # 0x1ed0
	.short	5553                    # 0x15b1
	.short	1739                    # 0x6cb
	.short	11477                   # 0x2cd5
	.short	9368                    # 0x2498
	.short	4339                    # 0x10f3
	.short	3698                    # 0xe72
	.short	9156                    # 0x23c4
	.short	6240                    # 0x1860
	.short	10824                   # 0x2a48
	.short	1949                    # 0x79d
	.short	9478                    # 0x2506
	.short	9729                    # 0x2601
	.short	9739                    # 0x260b
	.short	10229                   # 0x27f5
	.short	9661                    # 0x25bd
	.short	6407                    # 0x1907
	.short	6718                    # 0x1a3e
	.short	9668                    # 0x25c4
	.short	6750                    # 0x1a5e
	.short	11236                   # 0x2be4
	.short	9848                    # 0x2678
	.short	3281                    # 0xcd1
	.short	1012                    # 0x3f4
	.short	432                     # 0x1b0
	.short	8879                    # 0x22af
	.short	4956                    # 0x135c
	.short	9353                    # 0x2489
	.short	3604                    # 0xe14
	.short	4550                    # 0x11c6
	.short	1748                    # 0x6d4
	.short	11918                   # 0x2e8e
	.short	6399                    # 0x18ff
	.short	6326                    # 0x18b6
	.short	2749                    # 0xabd
	.short	11811                   # 0x2e23
	.short	1156                    # 0x484
	.short	7488                    # 0x1d40
	.short	10531                   # 0x2923
	.short	12170                   # 0x2f8a
	.short	6458                    # 0x193a
	.short	9217                    # 0x2401
	.short	9229                    # 0x240d
	.short	9817                    # 0x2659
	.short	1762                    # 0x6e2
	.short	315                     # 0x13b
	.short	3146                    # 0xc4a
	.short	6686                    # 0x1a1e
	.short	8100                    # 0x1fa4
	.short	3652                    # 0xe44
	.short	6902                    # 0x1af6
	.short	6395                    # 0x18fb
	.short	6130                    # 0x17f2
	.short	5434                    # 0x153a
	.short	8197                    # 0x2005
	.short	8405                    # 0x20d5
	.short	6308                    # 0x18a4
	.short	1867                    # 0x74b
	.short	5460                    # 0x1554
	.short	9471                    # 0x24ff
	.short	9386                    # 0x24aa
	.short	5221                    # 0x1465
	.short	10049                   # 0x2741
	.short	841                     # 0x349
	.short	4342                    # 0x10f6
	.short	3845                    # 0xf05
	.short	4070                    # 0xfe6
	.short	2806                    # 0xaf6
	.short	2315                    # 0x90b
	.short	2834                    # 0xb12
	.short	3687                    # 0xe67
	.short	8617                    # 0x21a9
	.short	4407                    # 0x1137
	.short	7030                    # 0x1b76
	.short	378                     # 0x17a
	.short	6233                    # 0x1859
	.short	10481                   # 0x28f1
	.short	9720                    # 0x25f8
	.short	9298                    # 0x2452
	.short	909                     # 0x38d
	.short	7674                    # 0x1dfa
	.short	7356                    # 0x1cbc
	.short	4063                    # 0xfdf
	.short	2463                    # 0x99f
	.short	10086                   # 0x2766
	.short	2654                    # 0xa5e
	.short	7156                    # 0x1bf4
	.short	6552                    # 0x1998
	.short	1534                    # 0x5fe
	.short	1432                    # 0x598
	.short	8723                    # 0x2213
	.short	9601                    # 0x2581
	.short	3467                    # 0xd8b
	.short	10126                   # 0x278e
	.short	4614                    # 0x1206
	.short	4884                    # 0x1314
	.short	5825                    # 0x16c1
	.short	2778                    # 0xada
	.short	943                     # 0x3af
	.short	9340                    # 0x247c
	.short	2967                    # 0xb97
	.short	10204                   # 0x27dc
	.short	8436                    # 0x20f4
	.short	7827                    # 0x1e93
	.short	2564                    # 0xa04
	.short	2746                    # 0xaba
	.short	11664                   # 0x2d90
	.short	6242                    # 0x1862
	.short	10922                   # 0x2aaa
	.short	6751                    # 0x1a5f
	.short	11285                   # 0x2c15
	.short	12249                   # 0x2fd9
	.short	10329                   # 0x2859
	.short	2272                    # 0x8e0
	.short	727                     # 0x2d7
	.short	11045                   # 0x2b25
	.short	489                     # 0x1e9
	.short	11672                   # 0x2d98
	.short	6634                    # 0x19ea
	.short	5552                    # 0x15b0
	.short	1690                    # 0x69a
	.short	9076                    # 0x2374
	.short	2320                    # 0x910
	.short	3079                    # 0xc07
	.short	3403                    # 0xd4b
	.short	6990                    # 0x1b4e
	.short	10707                   # 0x29d3
	.short	8505                    # 0x2139
	.short	11208                   # 0x2bc8
	.short	8476                    # 0x211c
	.short	9787                    # 0x263b
	.short	292                     # 0x124
	.short	2019                    # 0x7e3
	.short	619                     # 0x26b
	.short	5753                    # 0x1679
	.short	11539                   # 0x2d13
	.short	117                     # 0x75
	.short	5733                    # 0x1665
	.short	10559                   # 0x293f
	.short	1253                    # 0x4e5
	.short	12241                   # 0x2fd1
	.short	9937                    # 0x26d1
	.short	7642                    # 0x1dda
	.short	5788                    # 0x169c
	.short	965                     # 0x3c5
	.short	10418                   # 0x28b2
	.short	6633                    # 0x19e9
	.short	5503                    # 0x157f
	.short	11578                   # 0x2d3a
	.short	2028                    # 0x7ec
	.short	1060                    # 0x424
	.short	2784                    # 0xae0
	.short	1237                    # 0x4d5
	.short	11457                   # 0x2cc1
	.short	8388                    # 0x20c4
	.short	5475                    # 0x1563
	.short	10206                   # 0x27de
	.short	8534                    # 0x2156
	.short	340                     # 0x154
	.short	4371                    # 0x1113
	.short	5266                    # 0x1492
	.short	12254                   # 0x2fde
	.short	10574                   # 0x294e
	.short	1988                    # 0x7c4
	.short	11389                   # 0x2c7d
	.short	5056                    # 0x13c0
	.short	1964                    # 0x7ac
	.short	10213                   # 0x27e5
	.short	8877                    # 0x22ad
	.short	4858                    # 0x12fa
	.short	4551                    # 0x11c7
	.short	1797                    # 0x705
	.short	2030                    # 0x7ee
	.short	1158                    # 0x486
	.short	7586                    # 0x1da2
	.short	3044                    # 0xbe4
	.short	1688                    # 0x698
	.short	8978                    # 0x2312
	.short	9807                    # 0x264f
	.short	1272                    # 0x4f8
	.short	883                     # 0x373
	.short	6400                    # 0x1900
	.short	6375                    # 0x18e7
	.short	5150                    # 0x141e
	.short	6570                    # 0x19aa
	.short	2416                    # 0x970
	.short	7783                    # 0x1e67
	.short	408                     # 0x198
	.short	7703                    # 0x1e17
	.short	8777                    # 0x2249
	.short	12247                   # 0x2fd7
	.short	10231                   # 0x27f7
	.short	9759                    # 0x261f
	.short	11209                   # 0x2bc9
	.short	8525                    # 0x214d
	.short	12188                   # 0x2f9c
	.short	7340                    # 0x1cac
	.short	3279                    # 0xccf
	.short	914                     # 0x392
	.short	7919                    # 0x1eef
	.short	7072                    # 0x1ba0
	.short	2436                    # 0x984
	.short	8763                    # 0x223b
	.short	11561                   # 0x2d29
	.short	1195                    # 0x4ab
	.short	9399                    # 0x24b7
	.short	5858                    # 0x16e2
	.short	4395                    # 0x112b
	.short	6442                    # 0x192a
	.short	8433                    # 0x20f1
	.short	7680                    # 0x1e00
	.short	7650                    # 0x1de2
	.short	6180                    # 0x1824
	.short	7884                    # 0x1ecc
	.short	5357                    # 0x14ed
	.short	4424                    # 0x1148
	.short	7863                    # 0x1eb7
	.short	4328                    # 0x10e8
	.short	3159                    # 0xc57
	.short	7323                    # 0x1c9b
	.short	2446                    # 0x98e
	.short	9253                    # 0x2425
	.short	10993                   # 0x2af1
	.short	10230                   # 0x27f6
	.short	9710                    # 0x25ee
	.short	8808                    # 0x2268
	.short	1477                    # 0x5c5
	.short	10928                   # 0x2ab0
	.short	7045                    # 0x1b85
	.short	1113                    # 0x459
	.short	5381                    # 0x1505
	.short	5600                    # 0x15e0
	.short	4042                    # 0xfca
	.short	1434                    # 0x59a
	.short	8821                    # 0x2275
	.short	2114                    # 0x842
	.short	5274                    # 0x149a
	.short	357                     # 0x165
	.short	5204                    # 0x1454
	.short	9216                    # 0x2400
	.short	9180                    # 0x23dc
	.short	7416                    # 0x1cf8
	.short	7003                    # 0x1b5b
	.short	11344                   # 0x2c50
	.short	2851                    # 0xb23
	.short	4520                    # 0x11a8
	.short	278                     # 0x116
	.short	1333                    # 0x535
	.short	3872                    # 0xf20
	.short	5393                    # 0x1511
	.short	6188                    # 0x182c
	.short	8276                    # 0x2054
	.short	12276                   # 0x2ff4
	.short	11652                   # 0x2d84
	.short	5654                    # 0x1616
	.short	6688                    # 0x1a20
	.size	v_9, 2304

	.type	v_10,@object            # @v_10
	.globl	v_10
	.p2align	4
v_10:
	.long	0                       # 0x0
	.long	512                     # 0x200
	.long	256                     # 0x100
	.long	768                     # 0x300
	.long	128                     # 0x80
	.long	640                     # 0x280
	.long	384                     # 0x180
	.long	896                     # 0x380
	.long	64                      # 0x40
	.long	576                     # 0x240
	.long	320                     # 0x140
	.long	832                     # 0x340
	.long	192                     # 0xc0
	.long	704                     # 0x2c0
	.long	448                     # 0x1c0
	.long	960                     # 0x3c0
	.long	32                      # 0x20
	.long	544                     # 0x220
	.long	288                     # 0x120
	.long	800                     # 0x320
	.long	160                     # 0xa0
	.long	672                     # 0x2a0
	.long	416                     # 0x1a0
	.long	928                     # 0x3a0
	.long	96                      # 0x60
	.long	608                     # 0x260
	.long	352                     # 0x160
	.long	864                     # 0x360
	.long	224                     # 0xe0
	.long	736                     # 0x2e0
	.long	480                     # 0x1e0
	.long	992                     # 0x3e0
	.long	16                      # 0x10
	.long	528                     # 0x210
	.long	272                     # 0x110
	.long	784                     # 0x310
	.long	144                     # 0x90
	.long	656                     # 0x290
	.long	400                     # 0x190
	.long	912                     # 0x390
	.long	80                      # 0x50
	.long	592                     # 0x250
	.long	336                     # 0x150
	.long	848                     # 0x350
	.long	208                     # 0xd0
	.long	720                     # 0x2d0
	.long	464                     # 0x1d0
	.long	976                     # 0x3d0
	.long	48                      # 0x30
	.long	560                     # 0x230
	.long	304                     # 0x130
	.long	816                     # 0x330
	.long	176                     # 0xb0
	.long	688                     # 0x2b0
	.long	432                     # 0x1b0
	.long	944                     # 0x3b0
	.long	112                     # 0x70
	.long	624                     # 0x270
	.long	368                     # 0x170
	.long	880                     # 0x370
	.long	240                     # 0xf0
	.long	752                     # 0x2f0
	.long	496                     # 0x1f0
	.long	1008                    # 0x3f0
	.long	8                       # 0x8
	.long	520                     # 0x208
	.long	264                     # 0x108
	.long	776                     # 0x308
	.long	136                     # 0x88
	.long	648                     # 0x288
	.long	392                     # 0x188
	.long	904                     # 0x388
	.long	72                      # 0x48
	.long	584                     # 0x248
	.long	328                     # 0x148
	.long	840                     # 0x348
	.long	200                     # 0xc8
	.long	712                     # 0x2c8
	.long	456                     # 0x1c8
	.long	968                     # 0x3c8
	.long	40                      # 0x28
	.long	552                     # 0x228
	.long	296                     # 0x128
	.long	808                     # 0x328
	.long	168                     # 0xa8
	.long	680                     # 0x2a8
	.long	424                     # 0x1a8
	.long	936                     # 0x3a8
	.long	104                     # 0x68
	.long	616                     # 0x268
	.long	360                     # 0x168
	.long	872                     # 0x368
	.long	232                     # 0xe8
	.long	744                     # 0x2e8
	.long	488                     # 0x1e8
	.long	1000                    # 0x3e8
	.long	24                      # 0x18
	.long	536                     # 0x218
	.long	280                     # 0x118
	.long	792                     # 0x318
	.long	152                     # 0x98
	.long	664                     # 0x298
	.long	408                     # 0x198
	.long	920                     # 0x398
	.long	88                      # 0x58
	.long	600                     # 0x258
	.long	344                     # 0x158
	.long	856                     # 0x358
	.long	216                     # 0xd8
	.long	728                     # 0x2d8
	.long	472                     # 0x1d8
	.long	984                     # 0x3d8
	.long	56                      # 0x38
	.long	568                     # 0x238
	.long	312                     # 0x138
	.long	824                     # 0x338
	.long	184                     # 0xb8
	.long	696                     # 0x2b8
	.long	440                     # 0x1b8
	.long	952                     # 0x3b8
	.long	120                     # 0x78
	.long	632                     # 0x278
	.long	376                     # 0x178
	.long	888                     # 0x378
	.long	248                     # 0xf8
	.long	760                     # 0x2f8
	.long	504                     # 0x1f8
	.long	1016                    # 0x3f8
	.long	4                       # 0x4
	.long	516                     # 0x204
	.long	260                     # 0x104
	.long	772                     # 0x304
	.long	132                     # 0x84
	.long	644                     # 0x284
	.long	388                     # 0x184
	.long	900                     # 0x384
	.long	68                      # 0x44
	.long	580                     # 0x244
	.long	324                     # 0x144
	.long	836                     # 0x344
	.long	196                     # 0xc4
	.long	708                     # 0x2c4
	.long	452                     # 0x1c4
	.long	964                     # 0x3c4
	.long	36                      # 0x24
	.long	548                     # 0x224
	.long	292                     # 0x124
	.long	804                     # 0x324
	.long	164                     # 0xa4
	.long	676                     # 0x2a4
	.long	420                     # 0x1a4
	.long	932                     # 0x3a4
	.long	100                     # 0x64
	.long	612                     # 0x264
	.long	356                     # 0x164
	.long	868                     # 0x364
	.long	228                     # 0xe4
	.long	740                     # 0x2e4
	.long	484                     # 0x1e4
	.long	996                     # 0x3e4
	.long	20                      # 0x14
	.long	532                     # 0x214
	.long	276                     # 0x114
	.long	788                     # 0x314
	.long	148                     # 0x94
	.long	660                     # 0x294
	.long	404                     # 0x194
	.long	916                     # 0x394
	.long	84                      # 0x54
	.long	596                     # 0x254
	.long	340                     # 0x154
	.long	852                     # 0x354
	.long	212                     # 0xd4
	.long	724                     # 0x2d4
	.long	468                     # 0x1d4
	.long	980                     # 0x3d4
	.long	52                      # 0x34
	.long	564                     # 0x234
	.long	308                     # 0x134
	.long	820                     # 0x334
	.long	180                     # 0xb4
	.long	692                     # 0x2b4
	.long	436                     # 0x1b4
	.long	948                     # 0x3b4
	.long	116                     # 0x74
	.long	628                     # 0x274
	.long	372                     # 0x174
	.long	884                     # 0x374
	.long	244                     # 0xf4
	.long	756                     # 0x2f4
	.long	500                     # 0x1f4
	.long	1012                    # 0x3f4
	.long	12                      # 0xc
	.long	524                     # 0x20c
	.long	268                     # 0x10c
	.long	780                     # 0x30c
	.long	140                     # 0x8c
	.long	652                     # 0x28c
	.long	396                     # 0x18c
	.long	908                     # 0x38c
	.long	76                      # 0x4c
	.long	588                     # 0x24c
	.long	332                     # 0x14c
	.long	844                     # 0x34c
	.long	204                     # 0xcc
	.long	716                     # 0x2cc
	.long	460                     # 0x1cc
	.long	972                     # 0x3cc
	.long	44                      # 0x2c
	.long	556                     # 0x22c
	.long	300                     # 0x12c
	.long	812                     # 0x32c
	.long	172                     # 0xac
	.long	684                     # 0x2ac
	.long	428                     # 0x1ac
	.long	940                     # 0x3ac
	.long	108                     # 0x6c
	.long	620                     # 0x26c
	.long	364                     # 0x16c
	.long	876                     # 0x36c
	.long	236                     # 0xec
	.long	748                     # 0x2ec
	.long	492                     # 0x1ec
	.long	1004                    # 0x3ec
	.long	28                      # 0x1c
	.long	540                     # 0x21c
	.long	284                     # 0x11c
	.long	796                     # 0x31c
	.long	156                     # 0x9c
	.long	668                     # 0x29c
	.long	412                     # 0x19c
	.long	924                     # 0x39c
	.long	92                      # 0x5c
	.long	604                     # 0x25c
	.long	348                     # 0x15c
	.long	860                     # 0x35c
	.long	220                     # 0xdc
	.long	732                     # 0x2dc
	.long	476                     # 0x1dc
	.long	988                     # 0x3dc
	.long	60                      # 0x3c
	.long	572                     # 0x23c
	.long	316                     # 0x13c
	.long	828                     # 0x33c
	.long	188                     # 0xbc
	.long	700                     # 0x2bc
	.long	444                     # 0x1bc
	.long	956                     # 0x3bc
	.long	124                     # 0x7c
	.long	636                     # 0x27c
	.long	380                     # 0x17c
	.long	892                     # 0x37c
	.long	252                     # 0xfc
	.long	764                     # 0x2fc
	.long	508                     # 0x1fc
	.long	1020                    # 0x3fc
	.long	2                       # 0x2
	.long	514                     # 0x202
	.long	258                     # 0x102
	.long	770                     # 0x302
	.long	130                     # 0x82
	.long	642                     # 0x282
	.long	386                     # 0x182
	.long	898                     # 0x382
	.long	66                      # 0x42
	.long	578                     # 0x242
	.long	322                     # 0x142
	.long	834                     # 0x342
	.long	194                     # 0xc2
	.long	706                     # 0x2c2
	.long	450                     # 0x1c2
	.long	962                     # 0x3c2
	.long	34                      # 0x22
	.long	546                     # 0x222
	.long	290                     # 0x122
	.long	802                     # 0x322
	.long	162                     # 0xa2
	.long	674                     # 0x2a2
	.long	418                     # 0x1a2
	.long	930                     # 0x3a2
	.long	98                      # 0x62
	.long	610                     # 0x262
	.long	354                     # 0x162
	.long	866                     # 0x362
	.long	226                     # 0xe2
	.long	738                     # 0x2e2
	.long	482                     # 0x1e2
	.long	994                     # 0x3e2
	.long	18                      # 0x12
	.long	530                     # 0x212
	.long	274                     # 0x112
	.long	786                     # 0x312
	.long	146                     # 0x92
	.long	658                     # 0x292
	.long	402                     # 0x192
	.long	914                     # 0x392
	.long	82                      # 0x52
	.long	594                     # 0x252
	.long	338                     # 0x152
	.long	850                     # 0x352
	.long	210                     # 0xd2
	.long	722                     # 0x2d2
	.long	466                     # 0x1d2
	.long	978                     # 0x3d2
	.long	50                      # 0x32
	.long	562                     # 0x232
	.long	306                     # 0x132
	.long	818                     # 0x332
	.long	178                     # 0xb2
	.long	690                     # 0x2b2
	.long	434                     # 0x1b2
	.long	946                     # 0x3b2
	.long	114                     # 0x72
	.long	626                     # 0x272
	.long	370                     # 0x172
	.long	882                     # 0x372
	.long	242                     # 0xf2
	.long	754                     # 0x2f2
	.long	498                     # 0x1f2
	.long	1010                    # 0x3f2
	.long	10                      # 0xa
	.long	522                     # 0x20a
	.long	266                     # 0x10a
	.long	778                     # 0x30a
	.long	138                     # 0x8a
	.long	650                     # 0x28a
	.long	394                     # 0x18a
	.long	906                     # 0x38a
	.long	74                      # 0x4a
	.long	586                     # 0x24a
	.long	330                     # 0x14a
	.long	842                     # 0x34a
	.long	202                     # 0xca
	.long	714                     # 0x2ca
	.long	458                     # 0x1ca
	.long	970                     # 0x3ca
	.long	42                      # 0x2a
	.long	554                     # 0x22a
	.long	298                     # 0x12a
	.long	810                     # 0x32a
	.long	170                     # 0xaa
	.long	682                     # 0x2aa
	.long	426                     # 0x1aa
	.long	938                     # 0x3aa
	.long	106                     # 0x6a
	.long	618                     # 0x26a
	.long	362                     # 0x16a
	.long	874                     # 0x36a
	.long	234                     # 0xea
	.long	746                     # 0x2ea
	.long	490                     # 0x1ea
	.long	1002                    # 0x3ea
	.long	26                      # 0x1a
	.long	538                     # 0x21a
	.long	282                     # 0x11a
	.long	794                     # 0x31a
	.long	154                     # 0x9a
	.long	666                     # 0x29a
	.long	410                     # 0x19a
	.long	922                     # 0x39a
	.long	90                      # 0x5a
	.long	602                     # 0x25a
	.long	346                     # 0x15a
	.long	858                     # 0x35a
	.long	218                     # 0xda
	.long	730                     # 0x2da
	.long	474                     # 0x1da
	.long	986                     # 0x3da
	.long	58                      # 0x3a
	.long	570                     # 0x23a
	.long	314                     # 0x13a
	.long	826                     # 0x33a
	.long	186                     # 0xba
	.long	698                     # 0x2ba
	.long	442                     # 0x1ba
	.long	954                     # 0x3ba
	.long	122                     # 0x7a
	.long	634                     # 0x27a
	.long	378                     # 0x17a
	.long	890                     # 0x37a
	.long	250                     # 0xfa
	.long	762                     # 0x2fa
	.long	506                     # 0x1fa
	.long	1018                    # 0x3fa
	.long	6                       # 0x6
	.long	518                     # 0x206
	.long	262                     # 0x106
	.long	774                     # 0x306
	.long	134                     # 0x86
	.long	646                     # 0x286
	.long	390                     # 0x186
	.long	902                     # 0x386
	.long	70                      # 0x46
	.long	582                     # 0x246
	.long	326                     # 0x146
	.long	838                     # 0x346
	.long	198                     # 0xc6
	.long	710                     # 0x2c6
	.long	454                     # 0x1c6
	.long	966                     # 0x3c6
	.long	38                      # 0x26
	.long	550                     # 0x226
	.long	294                     # 0x126
	.long	806                     # 0x326
	.long	166                     # 0xa6
	.long	678                     # 0x2a6
	.long	422                     # 0x1a6
	.long	934                     # 0x3a6
	.long	102                     # 0x66
	.long	614                     # 0x266
	.long	358                     # 0x166
	.long	870                     # 0x366
	.long	230                     # 0xe6
	.long	742                     # 0x2e6
	.long	486                     # 0x1e6
	.long	998                     # 0x3e6
	.long	22                      # 0x16
	.long	534                     # 0x216
	.long	278                     # 0x116
	.long	790                     # 0x316
	.long	150                     # 0x96
	.long	662                     # 0x296
	.long	406                     # 0x196
	.long	918                     # 0x396
	.long	86                      # 0x56
	.long	598                     # 0x256
	.long	342                     # 0x156
	.long	854                     # 0x356
	.long	214                     # 0xd6
	.long	726                     # 0x2d6
	.long	470                     # 0x1d6
	.long	982                     # 0x3d6
	.long	54                      # 0x36
	.long	566                     # 0x236
	.long	310                     # 0x136
	.long	822                     # 0x336
	.long	182                     # 0xb6
	.long	694                     # 0x2b6
	.long	438                     # 0x1b6
	.long	950                     # 0x3b6
	.long	118                     # 0x76
	.long	630                     # 0x276
	.long	374                     # 0x176
	.long	886                     # 0x376
	.long	246                     # 0xf6
	.long	758                     # 0x2f6
	.long	502                     # 0x1f6
	.long	1014                    # 0x3f6
	.long	14                      # 0xe
	.long	526                     # 0x20e
	.long	270                     # 0x10e
	.long	782                     # 0x30e
	.long	142                     # 0x8e
	.long	654                     # 0x28e
	.long	398                     # 0x18e
	.long	910                     # 0x38e
	.long	78                      # 0x4e
	.long	590                     # 0x24e
	.long	334                     # 0x14e
	.long	846                     # 0x34e
	.long	206                     # 0xce
	.long	718                     # 0x2ce
	.long	462                     # 0x1ce
	.long	974                     # 0x3ce
	.long	46                      # 0x2e
	.long	558                     # 0x22e
	.long	302                     # 0x12e
	.long	814                     # 0x32e
	.long	174                     # 0xae
	.long	686                     # 0x2ae
	.long	430                     # 0x1ae
	.long	942                     # 0x3ae
	.long	110                     # 0x6e
	.long	622                     # 0x26e
	.long	366                     # 0x16e
	.long	878                     # 0x36e
	.long	238                     # 0xee
	.long	750                     # 0x2ee
	.long	494                     # 0x1ee
	.long	1006                    # 0x3ee
	.long	30                      # 0x1e
	.long	542                     # 0x21e
	.long	286                     # 0x11e
	.long	798                     # 0x31e
	.long	158                     # 0x9e
	.long	670                     # 0x29e
	.long	414                     # 0x19e
	.long	926                     # 0x39e
	.long	94                      # 0x5e
	.long	606                     # 0x25e
	.long	350                     # 0x15e
	.long	862                     # 0x35e
	.long	222                     # 0xde
	.long	734                     # 0x2de
	.long	478                     # 0x1de
	.long	990                     # 0x3de
	.long	62                      # 0x3e
	.long	574                     # 0x23e
	.long	318                     # 0x13e
	.long	830                     # 0x33e
	.long	190                     # 0xbe
	.long	702                     # 0x2be
	.long	446                     # 0x1be
	.long	958                     # 0x3be
	.long	126                     # 0x7e
	.long	638                     # 0x27e
	.long	382                     # 0x17e
	.long	894                     # 0x37e
	.long	254                     # 0xfe
	.long	766                     # 0x2fe
	.long	510                     # 0x1fe
	.long	1022                    # 0x3fe
	.long	1                       # 0x1
	.long	513                     # 0x201
	.long	257                     # 0x101
	.long	769                     # 0x301
	.long	129                     # 0x81
	.long	641                     # 0x281
	.long	385                     # 0x181
	.long	897                     # 0x381
	.long	65                      # 0x41
	.long	577                     # 0x241
	.long	321                     # 0x141
	.long	833                     # 0x341
	.long	193                     # 0xc1
	.long	705                     # 0x2c1
	.long	449                     # 0x1c1
	.long	961                     # 0x3c1
	.long	33                      # 0x21
	.long	545                     # 0x221
	.long	289                     # 0x121
	.long	801                     # 0x321
	.long	161                     # 0xa1
	.long	673                     # 0x2a1
	.long	417                     # 0x1a1
	.long	929                     # 0x3a1
	.long	97                      # 0x61
	.long	609                     # 0x261
	.long	353                     # 0x161
	.long	865                     # 0x361
	.long	225                     # 0xe1
	.long	737                     # 0x2e1
	.long	481                     # 0x1e1
	.long	993                     # 0x3e1
	.long	17                      # 0x11
	.long	529                     # 0x211
	.long	273                     # 0x111
	.long	785                     # 0x311
	.long	145                     # 0x91
	.long	657                     # 0x291
	.long	401                     # 0x191
	.long	913                     # 0x391
	.long	81                      # 0x51
	.long	593                     # 0x251
	.long	337                     # 0x151
	.long	849                     # 0x351
	.long	209                     # 0xd1
	.long	721                     # 0x2d1
	.long	465                     # 0x1d1
	.long	977                     # 0x3d1
	.long	49                      # 0x31
	.long	561                     # 0x231
	.long	305                     # 0x131
	.long	817                     # 0x331
	.long	177                     # 0xb1
	.long	689                     # 0x2b1
	.long	433                     # 0x1b1
	.long	945                     # 0x3b1
	.long	113                     # 0x71
	.long	625                     # 0x271
	.long	369                     # 0x171
	.long	881                     # 0x371
	.long	241                     # 0xf1
	.long	753                     # 0x2f1
	.long	497                     # 0x1f1
	.long	1009                    # 0x3f1
	.long	9                       # 0x9
	.long	521                     # 0x209
	.long	265                     # 0x109
	.long	777                     # 0x309
	.long	137                     # 0x89
	.long	649                     # 0x289
	.long	393                     # 0x189
	.long	905                     # 0x389
	.long	73                      # 0x49
	.long	585                     # 0x249
	.long	329                     # 0x149
	.long	841                     # 0x349
	.long	201                     # 0xc9
	.long	713                     # 0x2c9
	.long	457                     # 0x1c9
	.long	969                     # 0x3c9
	.long	41                      # 0x29
	.long	553                     # 0x229
	.long	297                     # 0x129
	.long	809                     # 0x329
	.long	169                     # 0xa9
	.long	681                     # 0x2a9
	.long	425                     # 0x1a9
	.long	937                     # 0x3a9
	.long	105                     # 0x69
	.long	617                     # 0x269
	.long	361                     # 0x169
	.long	873                     # 0x369
	.long	233                     # 0xe9
	.long	745                     # 0x2e9
	.long	489                     # 0x1e9
	.long	1001                    # 0x3e9
	.long	25                      # 0x19
	.long	537                     # 0x219
	.long	281                     # 0x119
	.long	793                     # 0x319
	.long	153                     # 0x99
	.long	665                     # 0x299
	.long	409                     # 0x199
	.long	921                     # 0x399
	.long	89                      # 0x59
	.long	601                     # 0x259
	.long	345                     # 0x159
	.long	857                     # 0x359
	.long	217                     # 0xd9
	.long	729                     # 0x2d9
	.long	473                     # 0x1d9
	.long	985                     # 0x3d9
	.long	57                      # 0x39
	.long	569                     # 0x239
	.long	313                     # 0x139
	.long	825                     # 0x339
	.long	185                     # 0xb9
	.long	697                     # 0x2b9
	.long	441                     # 0x1b9
	.long	953                     # 0x3b9
	.long	121                     # 0x79
	.long	633                     # 0x279
	.long	377                     # 0x179
	.long	889                     # 0x379
	.long	249                     # 0xf9
	.long	761                     # 0x2f9
	.long	505                     # 0x1f9
	.long	1017                    # 0x3f9
	.long	5                       # 0x5
	.long	517                     # 0x205
	.long	261                     # 0x105
	.long	773                     # 0x305
	.long	133                     # 0x85
	.long	645                     # 0x285
	.long	389                     # 0x185
	.long	901                     # 0x385
	.long	69                      # 0x45
	.long	581                     # 0x245
	.long	325                     # 0x145
	.long	837                     # 0x345
	.long	197                     # 0xc5
	.long	709                     # 0x2c5
	.long	453                     # 0x1c5
	.long	965                     # 0x3c5
	.long	37                      # 0x25
	.long	549                     # 0x225
	.long	293                     # 0x125
	.long	805                     # 0x325
	.long	165                     # 0xa5
	.long	677                     # 0x2a5
	.long	421                     # 0x1a5
	.long	933                     # 0x3a5
	.long	101                     # 0x65
	.long	613                     # 0x265
	.long	357                     # 0x165
	.long	869                     # 0x365
	.long	229                     # 0xe5
	.long	741                     # 0x2e5
	.long	485                     # 0x1e5
	.long	997                     # 0x3e5
	.long	21                      # 0x15
	.long	533                     # 0x215
	.long	277                     # 0x115
	.long	789                     # 0x315
	.long	149                     # 0x95
	.long	661                     # 0x295
	.long	405                     # 0x195
	.long	917                     # 0x395
	.long	85                      # 0x55
	.long	597                     # 0x255
	.long	341                     # 0x155
	.long	853                     # 0x355
	.long	213                     # 0xd5
	.long	725                     # 0x2d5
	.long	469                     # 0x1d5
	.long	981                     # 0x3d5
	.long	53                      # 0x35
	.long	565                     # 0x235
	.long	309                     # 0x135
	.long	821                     # 0x335
	.long	181                     # 0xb5
	.long	693                     # 0x2b5
	.long	437                     # 0x1b5
	.long	949                     # 0x3b5
	.long	117                     # 0x75
	.long	629                     # 0x275
	.long	373                     # 0x175
	.long	885                     # 0x375
	.long	245                     # 0xf5
	.long	757                     # 0x2f5
	.long	501                     # 0x1f5
	.long	1013                    # 0x3f5
	.long	13                      # 0xd
	.long	525                     # 0x20d
	.long	269                     # 0x10d
	.long	781                     # 0x30d
	.long	141                     # 0x8d
	.long	653                     # 0x28d
	.long	397                     # 0x18d
	.long	909                     # 0x38d
	.long	77                      # 0x4d
	.long	589                     # 0x24d
	.long	333                     # 0x14d
	.long	845                     # 0x34d
	.long	205                     # 0xcd
	.long	717                     # 0x2cd
	.long	461                     # 0x1cd
	.long	973                     # 0x3cd
	.long	45                      # 0x2d
	.long	557                     # 0x22d
	.long	301                     # 0x12d
	.long	813                     # 0x32d
	.long	173                     # 0xad
	.long	685                     # 0x2ad
	.long	429                     # 0x1ad
	.long	941                     # 0x3ad
	.long	109                     # 0x6d
	.long	621                     # 0x26d
	.long	365                     # 0x16d
	.long	877                     # 0x36d
	.long	237                     # 0xed
	.long	749                     # 0x2ed
	.long	493                     # 0x1ed
	.long	1005                    # 0x3ed
	.long	29                      # 0x1d
	.long	541                     # 0x21d
	.long	285                     # 0x11d
	.long	797                     # 0x31d
	.long	157                     # 0x9d
	.long	669                     # 0x29d
	.long	413                     # 0x19d
	.long	925                     # 0x39d
	.long	93                      # 0x5d
	.long	605                     # 0x25d
	.long	349                     # 0x15d
	.long	861                     # 0x35d
	.long	221                     # 0xdd
	.long	733                     # 0x2dd
	.long	477                     # 0x1dd
	.long	989                     # 0x3dd
	.long	61                      # 0x3d
	.long	573                     # 0x23d
	.long	317                     # 0x13d
	.long	829                     # 0x33d
	.long	189                     # 0xbd
	.long	701                     # 0x2bd
	.long	445                     # 0x1bd
	.long	957                     # 0x3bd
	.long	125                     # 0x7d
	.long	637                     # 0x27d
	.long	381                     # 0x17d
	.long	893                     # 0x37d
	.long	253                     # 0xfd
	.long	765                     # 0x2fd
	.long	509                     # 0x1fd
	.long	1021                    # 0x3fd
	.long	3                       # 0x3
	.long	515                     # 0x203
	.long	259                     # 0x103
	.long	771                     # 0x303
	.long	131                     # 0x83
	.long	643                     # 0x283
	.long	387                     # 0x183
	.long	899                     # 0x383
	.long	67                      # 0x43
	.long	579                     # 0x243
	.long	323                     # 0x143
	.long	835                     # 0x343
	.long	195                     # 0xc3
	.long	707                     # 0x2c3
	.long	451                     # 0x1c3
	.long	963                     # 0x3c3
	.long	35                      # 0x23
	.long	547                     # 0x223
	.long	291                     # 0x123
	.long	803                     # 0x323
	.long	163                     # 0xa3
	.long	675                     # 0x2a3
	.long	419                     # 0x1a3
	.long	931                     # 0x3a3
	.long	99                      # 0x63
	.long	611                     # 0x263
	.long	355                     # 0x163
	.long	867                     # 0x363
	.long	227                     # 0xe3
	.long	739                     # 0x2e3
	.long	483                     # 0x1e3
	.long	995                     # 0x3e3
	.long	19                      # 0x13
	.long	531                     # 0x213
	.long	275                     # 0x113
	.long	787                     # 0x313
	.long	147                     # 0x93
	.long	659                     # 0x293
	.long	403                     # 0x193
	.long	915                     # 0x393
	.long	83                      # 0x53
	.long	595                     # 0x253
	.long	339                     # 0x153
	.long	851                     # 0x353
	.long	211                     # 0xd3
	.long	723                     # 0x2d3
	.long	467                     # 0x1d3
	.long	979                     # 0x3d3
	.long	51                      # 0x33
	.long	563                     # 0x233
	.long	307                     # 0x133
	.long	819                     # 0x333
	.long	179                     # 0xb3
	.long	691                     # 0x2b3
	.long	435                     # 0x1b3
	.long	947                     # 0x3b3
	.long	115                     # 0x73
	.long	627                     # 0x273
	.long	371                     # 0x173
	.long	883                     # 0x373
	.long	243                     # 0xf3
	.long	755                     # 0x2f3
	.long	499                     # 0x1f3
	.long	1011                    # 0x3f3
	.long	11                      # 0xb
	.long	523                     # 0x20b
	.long	267                     # 0x10b
	.long	779                     # 0x30b
	.long	139                     # 0x8b
	.long	651                     # 0x28b
	.long	395                     # 0x18b
	.long	907                     # 0x38b
	.long	75                      # 0x4b
	.long	587                     # 0x24b
	.long	331                     # 0x14b
	.long	843                     # 0x34b
	.long	203                     # 0xcb
	.long	715                     # 0x2cb
	.long	459                     # 0x1cb
	.long	971                     # 0x3cb
	.long	43                      # 0x2b
	.long	555                     # 0x22b
	.long	299                     # 0x12b
	.long	811                     # 0x32b
	.long	171                     # 0xab
	.long	683                     # 0x2ab
	.long	427                     # 0x1ab
	.long	939                     # 0x3ab
	.long	107                     # 0x6b
	.long	619                     # 0x26b
	.long	363                     # 0x16b
	.long	875                     # 0x36b
	.long	235                     # 0xeb
	.long	747                     # 0x2eb
	.long	491                     # 0x1eb
	.long	1003                    # 0x3eb
	.long	27                      # 0x1b
	.long	539                     # 0x21b
	.long	283                     # 0x11b
	.long	795                     # 0x31b
	.long	155                     # 0x9b
	.long	667                     # 0x29b
	.long	411                     # 0x19b
	.long	923                     # 0x39b
	.long	91                      # 0x5b
	.long	603                     # 0x25b
	.long	347                     # 0x15b
	.long	859                     # 0x35b
	.long	219                     # 0xdb
	.long	731                     # 0x2db
	.long	475                     # 0x1db
	.long	987                     # 0x3db
	.long	59                      # 0x3b
	.long	571                     # 0x23b
	.long	315                     # 0x13b
	.long	827                     # 0x33b
	.long	187                     # 0xbb
	.long	699                     # 0x2bb
	.long	443                     # 0x1bb
	.long	955                     # 0x3bb
	.long	123                     # 0x7b
	.long	635                     # 0x27b
	.long	379                     # 0x17b
	.long	891                     # 0x37b
	.long	251                     # 0xfb
	.long	763                     # 0x2fb
	.long	507                     # 0x1fb
	.long	1019                    # 0x3fb
	.long	7                       # 0x7
	.long	519                     # 0x207
	.long	263                     # 0x107
	.long	775                     # 0x307
	.long	135                     # 0x87
	.long	647                     # 0x287
	.long	391                     # 0x187
	.long	903                     # 0x387
	.long	71                      # 0x47
	.long	583                     # 0x247
	.long	327                     # 0x147
	.long	839                     # 0x347
	.long	199                     # 0xc7
	.long	711                     # 0x2c7
	.long	455                     # 0x1c7
	.long	967                     # 0x3c7
	.long	39                      # 0x27
	.long	551                     # 0x227
	.long	295                     # 0x127
	.long	807                     # 0x327
	.long	167                     # 0xa7
	.long	679                     # 0x2a7
	.long	423                     # 0x1a7
	.long	935                     # 0x3a7
	.long	103                     # 0x67
	.long	615                     # 0x267
	.long	359                     # 0x167
	.long	871                     # 0x367
	.long	231                     # 0xe7
	.long	743                     # 0x2e7
	.long	487                     # 0x1e7
	.long	999                     # 0x3e7
	.long	23                      # 0x17
	.long	535                     # 0x217
	.long	279                     # 0x117
	.long	791                     # 0x317
	.long	151                     # 0x97
	.long	663                     # 0x297
	.long	407                     # 0x197
	.long	919                     # 0x397
	.long	87                      # 0x57
	.long	599                     # 0x257
	.long	343                     # 0x157
	.long	855                     # 0x357
	.long	215                     # 0xd7
	.long	727                     # 0x2d7
	.long	471                     # 0x1d7
	.long	983                     # 0x3d7
	.long	55                      # 0x37
	.long	567                     # 0x237
	.long	311                     # 0x137
	.long	823                     # 0x337
	.long	183                     # 0xb7
	.long	695                     # 0x2b7
	.long	439                     # 0x1b7
	.long	951                     # 0x3b7
	.long	119                     # 0x77
	.long	631                     # 0x277
	.long	375                     # 0x177
	.long	887                     # 0x377
	.long	247                     # 0xf7
	.long	759                     # 0x2f7
	.long	503                     # 0x1f7
	.long	1015                    # 0x3f7
	.long	15                      # 0xf
	.long	527                     # 0x20f
	.long	271                     # 0x10f
	.long	783                     # 0x30f
	.long	143                     # 0x8f
	.long	655                     # 0x28f
	.long	399                     # 0x18f
	.long	911                     # 0x38f
	.long	79                      # 0x4f
	.long	591                     # 0x24f
	.long	335                     # 0x14f
	.long	847                     # 0x34f
	.long	207                     # 0xcf
	.long	719                     # 0x2cf
	.long	463                     # 0x1cf
	.long	975                     # 0x3cf
	.long	47                      # 0x2f
	.long	559                     # 0x22f
	.long	303                     # 0x12f
	.long	815                     # 0x32f
	.long	175                     # 0xaf
	.long	687                     # 0x2af
	.long	431                     # 0x1af
	.long	943                     # 0x3af
	.long	111                     # 0x6f
	.long	623                     # 0x26f
	.long	367                     # 0x16f
	.long	879                     # 0x36f
	.long	239                     # 0xef
	.long	751                     # 0x2ef
	.long	495                     # 0x1ef
	.long	1007                    # 0x3ef
	.long	31                      # 0x1f
	.long	543                     # 0x21f
	.long	287                     # 0x11f
	.long	799                     # 0x31f
	.long	159                     # 0x9f
	.long	671                     # 0x29f
	.long	415                     # 0x19f
	.long	927                     # 0x39f
	.long	95                      # 0x5f
	.long	607                     # 0x25f
	.long	351                     # 0x15f
	.long	863                     # 0x35f
	.long	223                     # 0xdf
	.long	735                     # 0x2df
	.long	479                     # 0x1df
	.long	991                     # 0x3df
	.long	63                      # 0x3f
	.long	575                     # 0x23f
	.long	319                     # 0x13f
	.long	831                     # 0x33f
	.long	191                     # 0xbf
	.long	703                     # 0x2bf
	.long	447                     # 0x1bf
	.long	959                     # 0x3bf
	.long	127                     # 0x7f
	.long	639                     # 0x27f
	.long	383                     # 0x17f
	.long	895                     # 0x37f
	.long	255                     # 0xff
	.long	767                     # 0x2ff
	.long	511                     # 0x1ff
	.long	1023                    # 0x3ff
	.size	v_10, 4096


	.ident	"clang version 9.0.1-12 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
