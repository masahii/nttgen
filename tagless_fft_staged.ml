open Tagless_impl_staged
open Tagless_examples
open Tagless_fft

module EX_C = EX(C)
module C_Complex = ComplexDomain(C_complex)
module Bit_rev_C = Bit_rev(C_Array)

let test_C () =
  let cde = EX_C.test_fact 10 in
  let open Codelib in
  print_code Format.std_formatter cde; print_newline ();
  let fact = Runcode.run cde in
  Printf.printf "fact, staged %d\n" fact;

  let cde = EX_C.test_nested_loop 4 5 in
  let open Codelib in
  print_code Format.std_formatter cde; print_newline ();
  let ret = Runcode.run cde in
  Printf.printf "nested loop, staged %d\n" ret

let bit_reverse_ref arr =
  let length =  Array.length arr in
  let bit_reverse_table = Bit_reverse.Bit_rev_1024.bit_reverse_table in
  assert (length = (Array.length bit_reverse_table));
  for i = 0 to length - 1 do
    let r = bit_reverse_table.(i) in
    if i < r then begin
      let tmp = arr.(i) in
      arr.(i) <- arr.(r);
      arr.(r) <- tmp
    end
  done

let test_bit_rev_staged () =
  let open C_Array in
  let bit_rev_code = Bit_rev_C.bit_reverse_8 () in
  Codelib.print_code Format.std_formatter bit_rev_code; print_newline ();
  let fn = Runcode.run bit_rev_code in
  let arr = Runcode.run (arr_init 8 (fun i -> .<i>.)) in
  fn arr;
  Array.iter (fun i -> Printf.printf "%d\n" i) arr

let test_fft_C =
  let module C_FFT = FFT_gen(C_Array)(C_Complex) in
  let open Complex in
  let open Dft in
  let open DFT(Fft_types.ComplexDomain) in
  let arr_ref = Array.init 1024 (fun _ -> {re=Random.float 1.0; im=Random.float 1.0}) in
  let ref_res = dft arr_ref in
  let arr = Array.copy arr_ref in
  let fn_code = C_FFT.fft 1024 in
  Codelib.print_code Format.std_formatter fn_code; print_newline ();
  let fn = Runcode.run fn_code in
  fn arr;
  ignore(Array.init 1024 (fun i -> Printf.printf "ref (%f, %f), tagless (%f, %f)\n" (ref_res.(i).re) (ref_res.(i).im) arr.(i).re arr.(i).im))
